<?php

/**
 * @file
 * Defines helpers functions for the .install file of the Harmonize module.
 */

use Drupal\harmonize\Constants\HarmonizeConfig;

/**
 * Removes dependencies on 'preprocessors' and 'crocheteer' modules.
 *
 * @param array $modules
 *   Array of machine names of modules we want to remove dependency on.
 */
function _harmonize_remove_dependencies(array $modules) : void {
  // First though, we want to check if any other modules depend on them.
  $moduleInfos = Drupal::service('extension.list.module')->getAllInstalledInfo();
  foreach ($moduleInfos as $info) {
    foreach ($modules as $module) {
      if (in_array($module, $info['dependencies'], TRUE)) {
        if (($key = array_search($module, $modules)) !== FALSE) {
          unset($modules[$key]);
        }
      }
    }
  }

  // Clean the modules we have to clean.
  if (!empty($modules)) {
    Drupal::database()->delete('key_value')
      ->condition('collection', 'system.schema')
      ->condition('name', $modules, 'IN')
      ->execute();
    $moduleData = Drupal::config('core.extension')->get('module');
    foreach ($modules as $module) {
      // Unset the modules you do not need.
      unset($moduleData[$module]);
    }
    Drupal::configFactory()->getEditable('core.extension')->set('module', $moduleData)->save();
  }
}

/**
 * Clean main settings configurations.
 */
function _harmonize_clean_main_settings_config() : void {
  // Cleanup main settings.
  $config = Drupal::configFactory()->getEditable(HarmonizeConfig::MAIN);

  // We'll mess with the raw data.
  $data = $config->getRawData();
  foreach ($data as $key => $value) {
    // The following configs have been removed in V3. We'll clean them out.
    if (in_array($key, ['auto_preprocess', 'max_recursive_depth'])) {
      unset($data[$key]);
    }

    // Any '0' or '1' values should be converted to bool.
    if ($value === 0 || $value === 1) {
      $data[$key] = (bool) $value;
    }
  }

  // Set our cleaned main settings configurations.
  Drupal::configFactory()->getEditable(HarmonizeConfig::MAIN)->setData($data)->save();
}

/**
 * Rename a configuration, deleting the old one and retaining the same data.
 *
 * @param string $oldName
 *   Name of the old configuration.
 * @param string $newName
 *   New name to be given to the configuration.
 */
function _harmonize_rename_configuration(string $oldName, string $newName) : void {
  // Migrate old cache configurations.
  $config = Drupal::configFactory()->getEditable($oldName);
  $data = $config->getRawData();

  // Overwrite the configuration if data exists.
  if (!empty($data)) {
    Drupal::configFactory()->getEditable($newName)->setData($data)->save();
    $config->delete();
  }
  else {
    Drupal::messenger()->addStatus(t('Could not rename configuration [@name]. It does not exist. Proceeding...', ['@name' => $oldName]));
  }
}

/**
 * Migrate data configurations to entity rules.
 */
function _harmonize_migrate_data_configurations() : void {
  // Array to store rules data.
  $rules = Drupal::configFactory()->getEditable(HarmonizeConfig::ENTITY_RULES)->getRawData() ?? [];

  // Migrate data configurations to entity rules.
  $config = Drupal::configFactory()->getEditable('harmonize.data_configurations');
  $data = $config->getRawData();

  // If entity settings are not set, we can stop here.
  if (empty($data) || !isset($data['harmonize_entity_settings'])) {
    return;
  }

  // Loop into entity configs and set them up.
  foreach ($data['harmonize_entity_settings'] as $entityTypeId => $exclusionsData) {
    // Get entity type ID exclusions.
    $exclusions = $exclusionsData['harmonize_entity_type_excluded_fields'] ?? [];

    // Do our migrations.
    foreach ($exclusions as $fieldName => $fieldExcluded) {
      if ($fieldExcluded !== 0) {
        $rules['rules'][$entityTypeId]['rules'][$fieldName]['excluded'] = TRUE;
      }
    }

    // Loop in bundle configurations if any.
    foreach ($exclusionsData as $bundle => $bundleExclusionsData) {
      // Get bundle exclusions.
      $bundleExclusions = $bundleExclusionsData['harmonize_entity_type_bundle_excluded_fields'] ?? [];

      // Do our migrations.
      foreach ($bundleExclusions as $fieldName => $fieldExcluded) {
        if ($fieldExcluded !== 0) {
          $rules['rules'][$entityTypeId]['bundles'][$bundle]['rules'][$fieldName]['excluded'] = TRUE;
        }
      }
    }
  }

  // Set our config if we must.
  if (!empty($rules)) {
    Drupal::configFactory()->getEditable(HarmonizeConfig::ENTITY_RULES)->setData($rules)->save();
    Drupal::configFactory()->getEditable('harmonize.data_configurations')->delete();
  }
}
