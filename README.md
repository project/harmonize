# Harmonize

**Please note that this module is nearing EOL. Due to many performance concerns and a large amount of legacy code, further development will be abandoned in favor of a spiritual successor: [Glint](https://git.drupalcode.org/project/glint).**

**Version 4.2 will be the final release of this module and will focus purely on performance optimizations. Moving forward, use of this module is NOT recommended, as it strays away from standard Drupal practices and can bring a big performance hit to your site if used excessively.**

**For more information, [go here](https://git.drupalcode.org/project/harmonize/-/blob/4.x/doc/EOL.md?ref_type=heads).** 

- [Overview](#overview)
- [What does it do?](#what-does-it-do)
- [Quick Start](#quick-start)
- [Features](#features)
  - [Module's Main Settings](#modules-main-settings)
  - [Entity Settings](#entity-settings)
  - [Data Alteration](#data-alteration)
  - [Twig Functions & Filters](#twig-functions-filters)
  - [Styles](#styles)
  - [Entity Processing Rules](#entity-processing-rules)
  - [Caching](#caching)
  - [Visualizer](#visualizer)
- [Advanced Features](#advanced-features)
  - [The Harmonize Service](#the-harmonize-service)

## Overview
Harmonize streamlines preprocessing of entities in Drupal before template
rendering. It is a tool for developers and was originally made to facilitate
collaboration between backend and frontend teams.

## What does it do?
Harmonize makes it much easier to access field values in Drupal templates. It
does this by preprocessing entity content and adding a new variable called
`harmony` to your twig templates. This variable contains a simple, cleanly
formatted array of data to work with. The format and output of this data is consistent
across all entity types you work with on your website.

Drupal already exposes the values of your content to your templates, but it is
sometimes difficult or tedious to access said data. This is especially true when
trying to access values in nested entities.

## Quick Start

The below example uses a content type called **Article**.

Start by enabling the Harmonize (`harmonize`) module.

Simply enabling the module will not add any overhead to your website or do any
preprocessing on entities.

When editing entity type bundles, a new section will be available: **Harmonize settings**.

![Entity Bundle Edit](./doc/screenshots/Screenshot--EntityBundleEdit.png)

Enable preprocessing via this new section, and you will then have access to the **Manage preprocessing** tab.

Here, you can control which fields are processed.

![Manage Preprocessing](./doc/screenshots/Screenshot--ManagePreprocessing.png)

And you're done! Your twig template should now have access to your preprocessed data.

In this example, in our `node--article.html.twig`, we can now use a `{{ dump(harmony) }}` to see the data.

![Data Example](./doc/screenshots/Screenshot--DataExample1.png)

It is **highly recommended** to make use of [**Harmony File Discovery**](#harmony-file-discovery) as well.

### Harmony File Discovery

Similar to how your `templates` folder works in your theme, a `harmony` folder can be created with files that fine-tune
the preprocessing further.

Enable the **Harmony File Discovery** (`harmonize_harmony_file_discovery`) module and visit
`/admin/config/harmonize/harmony-file-discovery` to quickly configure the module and generate the folder.

![HFD Settings](./doc/screenshots/Screenshot--HarmonyFileDiscoverySettings.png)

Once you generate the Harmony File Directory, the `harmony` folder will be added to your default theme. This folder
contains a starter kit of files you can use, as well as its own README with additional documentation.

Simply enabling this and generating the folder will add optimizations for certain media types. You can consult the
generated files to get more information on what this entails.

## Features

### Module's Main Settings

The main settings of the module can be configured at
`/admin/config/harmonize/settings`.

Below is a description of each setting.

#### Automatically Processed Entities

To understand this setting, you must first understand how Harmonize handles processing
entity references.

Take the following data from the Quick Start above:

![Data Example](./doc/screenshots/Screenshot--DataExample1.png)

Notice that for the `field_related_articles` field, the array contains values that are classes.

These are called **Harmonizers**. They are part of the backbone of the module and handle preprocessing
for entities.

The reason why the module returns a Harmonizer and not the data directly is for safety measures. If we continuously
process nested entities non-stop, this could very easily lead to infinite loops if entities have cross-references.

So instead, we return a Harmonizer class.

On the configuration page, you can configure which entity types should bypass this default rule. By default, the module
will bypass this rule for taxonomy terms and media items, returning the nested entity data directly instead of returning
a Harmonizer class.

If we were to add `node.article` in this setting for example, the data from above would instead look like this:

![Data Example 2](./doc/screenshots/Screenshot_6.png)

Instead of returning a **Harmonizer**, we now see the data of the referenced
entities in the entity reference fields.

With that in mind, be wary of the entities you add to this setting.

The recommended and safer way to set auto-preprocessing on nested entities is directly within the
[Entity Settings](#entity-settings). This will be covered in detail below.

Alternatively, you can always accomplish this manually by doing something
similar to this:

```php
/**
 * Implements hook_preprocess_HOOK() for node.html.twig.
 */
function YOUR_THEME_theme_preprocess_node(&$variables) {
  // Manually process nested entities.
  $variables['harmony']['field_related_articles'] = array_map(function ($articleHarmonizer) {
    return $articleHarmonizer->harmonize();
  }, $variables['harmony']['field_related_articles']);
}
```

All **Harmonizers** can be used to access nested data with the `->harmonize()`
function.

#### Allowed Admin Routes

Harmonize normally does not execute in admin routes as it is defined to act
before the templating layer of the frontend. With that being said, there are
cases where you do want it to execute in an admin route. Layout builder paths
are a good example of this since to preview blocks while editing content, Drupal
needs to render the entities for you to see.

Note that the layout builder routes in question are currently already hardcoded,
so you don't need to add them yourself. Chances are this setting won't need to
be touched for a while.

### Entity Settings

As seen in the [Quick Start](#quick-start), you have access to entity specific
settings. These will usually be seen when editing an entity type.

![Content Type Settings](./doc/screenshots/Screenshot--EntityBundleEdit.png)

This will grant you access to the **Manage Preprocessing Tab** for your entity type.

#### Manage Preprocessing Tab

Similar to your **Manage Display** tab, you can use this page to control which fields are processed
by the module. This is a very powerful page and is currently the best and most recommended way to use
the module.

![Manage Preprocessing](./doc/screenshots/Screenshot--ManagePreprocessing.png)

You can configure settings per active view mode, just like in your display settings. Depending on field types,
you will have specific settings that are self-explanatory.

As mentioned above, `entity_reference` & `entity_reference_revisions` type fields have access to additional settings
pertaining to [Automatic Preprocessing](#automatically-processed-entities). This form is the recommended way of managing
this.

With the combination of these settings and the ability to customize this **per view mode**, it is the developer's
responsibility to assure that infinite loops are avoided by fine-tuning and optimizing the fields that are shown.
As a general rule of thumb, you should avoid preprocessing fields that have no use in your templates.

In the future, further documentation will be added here to explain all the possibilities and behaviours.

### Data Alteration

Now we're getting to the good stuff!

Everything we've seen so far is what can be done without any additional code.
Harmonize offers ways to fine-tune the data before it is sent to templates.

Now the easiest way of course would be through your traditional Drupal Hooks.

```php
/**
 * Implements hook_preprocess_HOOK() for node.html.twig.
 */
function YOUR_THEME_theme_preprocess_node(&$variables) {
  // Get the data from Harmonize.
  $harmony = &$variables['harmony'];

  // Do data alteration stuff...
  $harmony['related_article_count'] = count($harmony['field_related_articles']);
}
```

This is completely fine to do, but Harmonize offers other ways to do this
without writing hooks. The two main ways to do this are:

* Harmony Files
* Refiners

Henceforth, these will be referred to as **Alteration Objects**.

#### Harmony Files

**Harmony Files** are the main way to alter data before it is sent to templates.
You need the **Harmonize: Harmony File Discovery** module to be enabled for this feature to be available to you.

With the module enabled, Harmonize will look for files in your active theme.
The file crawling is similar to how Drupal's templating system works.

**e.g.** For a `node` of type `page`, you would create a `node--page.html.twig`
in the `templates` directory of your theme.

**cont.** For the same `node` of type `page`, you would create a
`entity.node.page.harmony` file in the `harmony` directory of your theme.

The contents of the `entity.node.page.harmony` file could be as follows:

```php
<?php
// The '$consignment' variable contains the data that will be sent to templates.
// It can be modified as we wish here. In this case, we simply add the number of related articles.
$consignment['related_article_count'] = count($consignment['field_related_articles']);
```

The `$consignment` variable contains all the data so far. The `$harmony`
variable is the data that will be returned to templates. If `$harmony` is empty
by the end of the file, `$consignment` will be returned. So you have two options
in Harmony Files:

* Modify `$consignment` and never set `$harmony`. This is good for small touch-ups of the data.
* Set `$harmony` variable with new data. This is good if you have to **rearrange or completely change** data.

In our example above, we are simply adding new data, so we modify
`$consignment`.

And voila! In your `node--page.html.twig` template, you should now have access
to `harmony.related_article_count`.

Note that Harmony Files run before your traditional hooks, so you can always do
a combination of both.

If the Harmony File above exists, then:

```php
/**
 * Implements hook_preprocess_HOOK() for node.html.twig.
 */
function YOUR_THEME_theme_preprocess_node(&$variables) {
  // Since the harmony file declared above exists, this should evaluate to true.
  if (isset($harmony['related_article_count'])) {
    // Yay! It exists!
  }
}
```

As you may have guessed, the file crawling works similar to template
suggestions. So `entity.node.harmony` would work too, except it would apply
to all nodes instead of just `page` typed nodes. You can also specify a view
mode, like `entity.node.page.default` or `entity.node.page.teaser`. This works
with the module's very own "loading suggestions" algorithm, akin to
template suggestions.

Devs, refer to the `harmonize.api.php` to find information on hooks other modules
or themes can implement to alter loading suggestions and fine-tune things further.

You can make use of the configuration page associated with the Harmony Files module to
give your theme a headstart on proper preprocessing. Visit `/admin/config/harmonize/harmony-files` with
the module enabled to access a straightforward configuration page.

A button on this page will generate the `harmony` folder in your default theme for you,
and this generated folder includes more documentation and files to start you off.

#### Refiners

Refiners are alteration objects that work very similarly to harmony files. They
are quite simply your OOP alternative to them, allowing for Dependency Injection
and more.

Refiners use the
[Plugin API](https://www.drupal.org/docs/drupal-apis/plugin-api).

If you haven't read the [Harmony Files](#harmony-files) section, I suggest you
do so now to get a good comparison!

You must enable the **Harmonize: Refinery** module to gain the ability to use Refiners.

First, you would create a plugin in a module. For the sake of this example, I am
creating a module called `dreamland_refiners`.

Then, I can create a plugin at
`dreamland_refiners/src/Plugin/harmonize/Refiner/PageNodeRefiner.php`.

The contents would be the following:

```php
<?php

namespace Drupal\dreamland_refiners\Plugin\harmonize\Refiner;

use Drupal\harmonize\PluginManager\Refinery\RefinerBase;

/**
 * Provide plugin to alter harmonizer data for an entity.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Refiner(
 *   id = "dreamland_refiners.entity.node.page.refiner",
 *   target = "entity.node.page"
 * )
 *
 * @package Drupal\harmonize\Plugin\Harmonizer\Refiner
 */
final class PageNodeRefiner extends RefinerBase {

  /**
   * Add personal tweaks to harmonized data in this function.
   *
   * {@inheritdoc}
   */
  public function refine(&$consignment, &$harmony, $object): void {
    // Similar alterations to what we did earlier, but in a refiner!
    $consignment['featured_article_count'] = count($consignment['field_featured_articles']);
  }

}
```

Refiners work quite exactly like Harmony Files. Anything found inside the
`$harmony` variable at the end of the `refine()` function's execution will be
sent to templates. If `$harmony` isn't set, `$consignment` will be taken
instead.

The refiner here knows what to target due to the annotation definition. You can
see that the target is set to `entity.node.page`.

This works just like Harmony files. You could set it to `entity.node` to target
all nodes, or `entity.node.page.default` for the `default` view mode, for example.

#### Alteration Object Flow

So now we should have a good understanding of data alteration objects. With
this, we can finally talk about what happens when all of them exist.

Let's say we have everything from the examples above, so:

* A Refiner at `dreamland_refiners/src/Plugin/harmonize/Refiner/PageNodeRefiner.php` with the same content as above.
* A Harmony File at `dreamland_theme/harmony/entity.node.page.harmony` with the same content as above.

Your alterations would be applied from both locations and your resulting data will have all the changes.

But you're probably wondering what goes first? The answer to your question is
right here.

1. Harmonize Preprocesses Entity
2. Refiners Alter Data
3. Harmony Files Alter Data
4. Data is sent to `harmony` key in `$variables`
5. Data is sent to twig templates

This flow promotes the following best practices:

* Use Harmonize for easy base preprocessing of entities.
* Create Refiners for big data alterations that require advanced Drupal service injection, database queries and more.
* Create Harmony Files OR preprocess functions in your theme for final touch-ups of data before it reaches templates.
* Use `harmony` in your templates to access the preprocessed data.

#### Alteration Object Loading Algorithm

Harmonize has two ways to load alteration objects. To showcase these two
methodologies, let's say we have the following:

* A **Harmony File** called `entity.node.harmony`.
* A **Harmony File** called `entity.node.basic_page.harmony`.
* A **Refiner** targeting `entity.node.harmony`.
* A **Refiner** targeting `entity.node.basic_page.harmony`.

With the existence of the above, we decide to view a `node` of type `basic_page`.

Depending on the algorithm below, the objects will be loaded differently.

##### FCFS (First Come First Serve)

**FCFS** will **only load the first relevant object of each type found**. It attempts to
load **the most relevant alteration objects**.

So in the case above, only objects targeting `entity.node.basic_page.harmony` will be loaded,
because you're loading a `node` of type `basic_page`.

Other harmony files and refiners will be ignored once one is loaded.

The resulting flow is:

1. Harmonize performs default preprocessing.
2. Harmonize detects and loads the **Refiner** targeting `entity.node.basic_page.harmony`. Data is altered.
3. Harmonize detects and loads the **Harmony File** called `entity.node.basic_page.harmony`. Data is altered.
4. Harmonize returns the resulting data.

This loading philosophy is akin to the Drupal loading philosophies for templates.

**This is also the recommended setting to use.** It promotes better organization of your code and files truly
dedicated to specific use cases.

##### SCAN (Elevator)

**SCAN** will **load multiple alteration objects**. It loads **ALL relevant
alteration objects**.

So in the case above, objects targeting both `entity.node.harmony` and
`entity.node.basic_page.harmony` will be loaded. It will process them in order of how
specific your alteration object is, from least to most specific. Here,
`entity.node.harmony` will be loaded first, and then
`entity.node.basic_page.harmony` will be loaded after.

The resulting flow is:

1. Harmonize performs default preprocessing.
2. Harmonize detects and loads the **Refiner** targeting `entity.node.harmony`. Data is altered.
3. Harmonize detects and loads the **Refiner** targeting `entity.node.basic_page.harmony`. Data is altered.
4. Harmonize detects and loads the **Harmony File** called `entity.node.harmony`. Data is altered.
5. Harmonize detects and loads the **Harmony File** called `entity.node.basic_page.harmony`. Data is altered.
6. Harmonize returns the resulting data.

As you can probably tell from the flow here, this can be difficult to manage. This is due to the
**dependency mayhem** that can happen here. Due to the flow, some alteration objects will depend on others
indirectly, simply because they'll expect data alterations from a previous object to be present before they can
perform their own alterations.

This, combined with more objects being loaded in general, can lead to performance issues and simply a general lack of
true control over the understanding of your codebase and preprocessing environment.

**SCAN** essentially exists as a backwards compatibility option if you're migrating from V2.
When starting new projects, definitely opt to use **FCFS**.


### Twig Functions & Filters

Harmonize currently offers one twig function and two twig filters.

#### harmonize()

The `harmonize()` function can be used on an entity in a template to get
harmonized data.

For example in `node.html.twig`, we usually have access to the `node` variable,
which contains the node being viewed.

If you want to harmonize the data, you can do
the following.

```html
{% set harmony = harmonize(node) %}
```

This is something that you can do if you don't want Harmonize to do things
automatically. You can also use this function to get harmonized data from
entities in Entity Reference fields.

#### |get('FIELD_NAME')

The `|get('FIELD_NAME')` filter can be used from a **Harmonizer** to get a field
value. It is used in cases where Harmonize processes an `entity_reference` field and returns
a **Harmonizer** to you instead of the nested data.

Here is an example use case:

```html
<div>
    {% for article in harmony.field_related_articles[0:3] %}
        <img
            src="{{ article|get('field_thumbnail').field_media_image.attributes.src }}"
            alt="{{ article|get('field_thumbnail').field_media_image.attributes.alt }}"
            title="{{ article|get('field_thumbnail').field_media_image.attributes.title }}"
        />
    {% endfor %}
</div>
```

#### |harmonize

The `|harmonize` filter can be used from a **Harmonizer** to get all harmonized
data instead of just a chosen field.

```html
<div>
    {% for article in harmony.field_related_articles[0:3] %}
        <img
            src="{{ article|harmonize.field_media_image.attributes.src }}"
            alt="{{ article|harmonize.field_media_image.attributes.alt }}"
            title="{{ article|harmonize.field_media_image.attributes.title }}"
        />
    {% endfor %}
</div>
```

### Styles

Styles are the predecessors to View Modes support. They offer a way to **filter** the fields that are preprocessed when
you harmonize an entity.

Styles can be created and managed at `admin/config/harmonize/styles`.

For example, say I have a `node` of type `article`. This node has the following fields:

* `field_headline`
* `field_summary`
* `field_content`
* `field_featured_image`
* `field_related_articles`
* `field_reading_time`

By default, Harmonize will process all the available fields in an entity. When viewing our article, this is fine, as
all of these fields are relevant for the display of our article.

Let's say, however, we want to preprocess our article to prepare it to be displayed as a card?

In that case, we don't need certain fields. We'd only need, say:

* `field_summary`
* `field_featured_image`
* `field_reading_time`

To do this, we can create a **Style**. When creating a style, you simply give it a **Machine Name** and specify the
**Fields** that should be preprocessed when using said style. In that case, all other fields are skipped.

To use Styles, you can do it programmatically using the Harmonize service like this:

```php
\Drupal::service('harmonize')->harmonize($node, [HarmonizerFlags::STYLE => 'card']);
```

Or you can set up [rules](#entity-processing-rules) to achieve the same result.

### Entity Processing Rules

Entity Processing Rules can be managed at
`admin/config/harmonize/entity-rules`.

The settings here allow you to specify certain behaviors when Harmonize
processes fields. You can:

* Exclude a field from all processing.
* Disable caching on a field.
* Automatically process referenced entities (Entity Reference Fields Only)
* Set a Harmonizer Style (Entity Reference Fields Only)

This page is great for setting global processing rules for entities. For
example, if there is a field that you never want Harmonize to process, this is
where you would set it.

**e.g.** Excluding all Layout Builder fields from processing in nodes.

You can set up rules for Entities and Entity Bundles.

### Caching

With caching enabled, Harmonize will cache preprocessing results. This is
**heavily recommended** to keep on simply for performance reasons.

Harmonize results will share cache tags with the object it processes.

You can configure the cache at `admin/config/harmonize/cache`.

#### Entity Caching

When Harmonizing an Entity, caching works a little differently. Instead of
caching the full data result, caching is managed on a **per field basis**.

All field value caching is tied to the entity they've been entered in, as they
should be.

Additionally, **entity reference fields are not directly cached**. This is
because the referenced entities' field values are cached individually instead.

### Visualizer

The Visualizer is a powerful tool that can be used to test entity preprocessing.

You can try the visualizer at `admin/config/harmonize/visualize`.

Try it out! It's a pretty powerful tool for debugging as the module itself goes through countless steps when
preprocessing entities. If you're having issues, use the powerful **Trace** feature on this page to retrace the steps
the module takes when processing entities.

**More documentation in the future.**

## Advanced Features

So far, everything that was shown can be accomplished even by developers that
are more specialized in Front-End and templating. The stuff covered in this
section is intended for back-end developers that seek to dive deeper into how
Harmonize works and seek to potentially develop solutions of their own based off
the inner-workings of the module.

### The Harmonize Service

The **Harmonize** service is the central point of the module. In other words,
any object being preprocessed by the module is passed through the
`::harmonize()` method available in this service.

To see what I mean, take this example below.

```php
/**
 * Implements hook_preprocess_HOOK() for node.html.twig.
 */
function YOUR_THEME_theme_preprocess_node(&$variables) {
  $variables['harmony'] = Drupal::service('harmonize')->harmonize($variables['node']);
}
```

If you were to enable the module without configuring any of the settings, you
could use this hook to preprocess a node before template rendering. This would
achieve pretty much the same result as if you had enabled your node type to be
preprocessed via [Entity Settings](#entity-settings), minus the view mode specs.

The `::harmonize()` function accepts nearly any entity. I say nearly because
I personally haven't tested all entity types the world has to offer. However,
the module doesn't do anything crazy depending on the entity type being
processed. It simply loops through the fields and gets their values in a clean
format.

To add view mode specs, you would do the following:

```php
/**
 * Implements hook_preprocess_HOOK() for node.html.twig.
 */
function YOUR_THEME_theme_preprocess_node(&$variables) {
  $variables['harmony'] = Drupal::service('harmonize')->harmonize($variables['node'], [HarmonizerFlags::VIEW_MODE => 'default']);
}
```

This tells Harmonize to preprocess your node, all while taking into account any view mode settings set up in the
[Entity Settings](#entity-settings).

Setting the view mode is only one of the available **Harmonizer Flags**.

### Harmonizer Flags

**Flags** are used to modify the core behaviour when you call the `->harmonize()` function on the
[Harmonize Service](#the-harmonize-service).

There are many available flags. You can see them all at `Drupal\harmonize\Constants\HarmonizerFlags`.

```php
/**
 * Implements hook_preprocess_HOOK() for node.html.twig.
 */
function YOUR_THEME_theme_preprocess_node(&$variables) {
  $variables['harmony'] = Drupal::service('harmonize')->harmonize($variables['node'], [
    // Set Tracing. Only used in the Visualizer.
    // Do not use normally!
    HarmonizerFlags::TRACE => TRUE,

    // Set the theme.
    // Harmonize will normally take the current theme by default.
    // In some...Rare cases, you may want to change this!
    // Note that when using a theme, Harmonize will consider everything in said theme.
    // This includes Styles, Rules & Harmony Files.
    HarmonizerFlags::THEME => 'THEME_NAME',

    // Set fields to Harmonize.
    // Only fields specified in this array will be processed.
    HarmonizerFlags::FIELDS => [
      'field_hello',
      'field_world',
    ],

    // Set fields to skip.
    // Fields specified in this array will be skipped.
    HarmonizerFlags::EXCLUDED_FIELDS => [
      'field_skip',
      'field_me',
    ],

    // Set a language.
    // Harmonize will get the current language by default.
    HarmonizerFlags::LANG => 'en',

    // Set a style.
    HarmonizerFlags::STYLE => 'card',

    // Set a parent Harmonizer.
    // Not meant to be used by devs, but can be.
    HarmonizerFlags::PARENT => '',

    // Disable recursive harmonizations.
    // Can be used to completely prohibit automatic preprocessing of nested entities.
    HarmonizerFlags::NO_RECURSIVE => TRUE,

    // Disable caching.
    // Not recommended.
    HarmonizerFlags::SKIP_CACHE => TRUE,

    // Set a view mode.
    HarmonizerFlags::VIEW_MODE => 'default',
  ]);
}
```
