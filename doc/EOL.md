# End of Life

Due to many performance concerns and a large amount of legacy code, further development will be abandoned in favor of a 
spiritual successor: [Glint](https://www.drupal.org/project/glint).

Version 4.2 will be the final release of this module and will focus purely on performance optimizations. 

Moving forward, use of this module is NOT recommended, as it strays away from standard Drupal practices and can bring a 
big performance hit to your site if used excessively.

Consider using [Glint](https://www.drupal.org/project/glint), a much more user-friendly, Drupal-friendly approach to
preprocessing entity values. Just note that Glint is not a 1 to 1 replacement for Harmonize, but instead a cleaner
approach.

Below are a few tips on planning a removal of Harmonize in favor of Drupal standard practices in D10+.

## Main Components

The main use of the Harmony module is to obtain data from entities and send them to templates. This means that when
removing this module from your site, your main concern will be to send the needed data to your templates in another way.

Start by looking at how your site is built from your theme. If your templates reference a `harmony` or `data` variable,
check how that variable is being set. It can either be in a preprocess function, or from the module itself.

Dump whatever data is being built, and start building it manually with standard Drupal [Preprocess Functions](https://www.drupal.org/docs/8/theming-drupal-8/modifying-attributes-in-a-theme-file).

This can be a long process, but bear in mind that it may be worth it simply to improve performance on your site.

### Harmony Files

If your theme has `.harmony.php` files, you can take a look at those to see what kind of data manipulations devs wrote.
These files act as pseudo preprocess functions and preprocess data sent by Harmonize to your templates.

### Refiners

Keep an eye out for Refiners, which are also pseudo preprocess functions. They live in modules as [Plugins](https://www.drupal.org/docs/drupal-apis/plugin-api).

## Removal Assistance

If you require assistance removing the module from your site, try to contact [ksere](https://www.drupal.org/u/ksere)!