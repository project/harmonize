# Upgrading from older versions

## Upgrading from v2.x to later versions

If you happen to be working on a website with v2.x installed and want to upgrade,
plan ahead for a solid amount of work to do to fit with the new standards of the
later versions.

The first thing to note is that there are many differences in v3.x onwards. One of the
main things is that nested preprocessing of entities in entity reference fields
no longer happens automatically. This is because v2.x had a lot of risks with this
behaviour. Infinite loops were much more prevalent.

With this major change, you will have to refactor some of your existing code
pertaining to harmonize.

When `entity_reference` fields are processed, a **Harmonizer** is returned
instead of the expected data of the referenced entities. To reach the nested
data, you simply have to call `->harmonize()` on the Harmonizer object. Keep
this in mind when refactoring your code.

Below is an example of a use case where you need to do this.

```php
    // Use harmonize to preprocess entity values.
    $variables['data'] = \Drupal::service('harmonize')->harmonize($variables['elements']['#node']);

    // The result of the "field_related_nodes" field no longer returns the processed result directly.
    // It returns a 'Harmonizer', which can be used to get the processed result.
    // Here we do a simple array map to get the data from the harmonizers.
    if (!empty($variables['data']['field_related_nodes'])) {
      $variables['data']['field_related_nodes'] = array_map(function($harmonizer) {
        return $harmonizer->harmonize();
      }, $variables['data']['field_related_nodes']);
    }
```

### Upgrade Steps

Follow these steps to ensure the least amount of headache when upgrading.

* Assure site configurations in your codebase are up-to-date: `drush cex -y`.
* Update the module via composer: `composer update drupal/harmonize`.
* **Important** Run database updates: `drush updb -y`.
  * This is an important step as some configuration objects have been greatly changed. `drush updb` will handle all configuration migrations for you.
* **Important** - Export migrated configurations.
* Now you should be good to go...To start refactoring!

### Changelog & Refactoring

Below are other things to consider when upgrading from v2.x.

* Nested processing of referenced entities is no longer automatic. Adjust code accordingly.
  * When processing entity references, you will likely run into errors like `PHP Error: Cannot use object of type \Drupal\harmonize\Harmonizer\HarmonizerInterface as array ...`.
  * This is due to what is mentioned above in this guide. Basing yourself on the snippet above and using `->harmonize()` on the `Harmonizer` classes that are found in your data, you should be able to refactor your code with no issue.
* Harmonizer Signatures have been replaced with Harmonizer Suggestions. When targeting entities in a specific style or view mode, you must adjust your harmony file names and Refiner targets.
  * i.e. `entity.node.article--card` becomes `entity.node.article.card`.
* Theme Harmony configuration YAML files have been adjusted.
  * `THEMENAME.harmony.yml` is no longer loaded. The module will now load two separate files.
  * `THEMENAME.harmony.styles.yml` for Styles.
  * `THEMENAME.harmony.rules.yml` for Entity Rules.
  * See the `themes` folder in the `harmonize_examples` module for examples of the file structures.
* **Refiners** and **Harmony Files** functionality have been separated into two submodules.
  * Enable these modules accordingly if you need to. Some sites don't use these functionalities, hence the separation.
* Refiners may need to alter their use statements if they use `RefinerBase`.
  * The `RefinerBase` class was simply moved to the submodule, so alter your use statements accordingly. Your IDE will flag this for you.
* Theme `.harmony.php` files have been adjusted. Harmonize will now look for `.harmony` files instead.
  * You have the option to change the harmony file extension by accessing `/admin/config/harmonize/harmony-file-discovery`.
* An `$object` variable is now available in `.harmony` files. This variable contains the original object being preprocessed.
* **(^4.1)** - The 'Harmonics' service has been removed as it was redundant and more confusing than anything.
  * A solution for dependency injection within `.harmony` files will be done later on, likely when/if a solution for dependency injection is found for themes.
  * If you made use of Harmonics, refactor your code to access services using `\Drupal::service()` instead.
  * If you decorated the Harmonics service and had a lot of helper functions in your custom Harmonics service, you can either move them into a Refiner class or convert them into regular functions in a module or in `functions.php`.
* [Styles](https://git.drupalcode.org/project/harmonize#styles) can now be seen and created in the admin interface.
  * Visit `/admin/config/harmonize/styles` to create new styles and view existing ones.
* Data configurations have been deprecated. They have been replaced with [Entity Processing Rules](https://git.drupalcode.org/project/harmonize#entity-processing-rules).
  * `drush updb -y` will migrate all of your data configurations into the new rules. You don't have to worry about doing this by hand.
* The [Visualizer](https://git.drupalcode.org/project/harmonize#visualizer) has been added to allow entity processing tests.
* New [Twig](https://git.drupalcode.org/project/harmonize#twig-functions-filters) functionality has been added.
  * This may help with refactoring in Twig templates.
* [Caching](https://git.drupalcode.org/project/harmonize#caching) has been optimized and is no longer experimental. Enable it. Trust me.
* [Entity Settings](https://git.drupalcode.org/project/harmonize#entity-settings) have been enabled along with view mode integration.
  * Coming from V2, this may not be needed. But it may be something to consider during refactoring.
* [Main Module Settings](https://git.drupalcode.org/project/harmonize#modules-main-settings) have been adjusted.
  * Moved Automatic Preprocessing setting to a textarea. You now specify which entity types are automatically processed in entity reference fields.
    * This is one of the ways of bypassing the change mentioned at the top of this guide, but not the recommended way.
    * **WARNING**: This setting can lead to infinite loops in some cases. Be mindful of the entity types you add here.
  * Added new setting for the [Data Alteration Loading Algorithm](https://git.drupalcode.org/project/harmonize#alteration-object-loading-algorithm).
    * If you're coming from v2.x, and you have a lot of data alteration objects, you may want to set this to **SCAN**, as it was the default (and only) algorithm in v2.x.
    * **FCFS** is the preferred way moving forward as it allows for better organization of code and less performance strain.
    * If upgrading causes issues, use **SCAN** to save yourself a headache in the beginning. Plan a switch and reorganization to **FCFS** later on.
