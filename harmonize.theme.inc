<?php

/**
 * @file
 * Defines theme functions for the Harmonize module.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for harmonize_admin_fields_table form templates.
 *
 * @param array $variables
 *   Associative array of template variables, with the following structure:
 *   - element: Associative array with the following keys:
 *     - form: A render element representing the form.
 *     - note: The table note.
 *
 * @see search_api.theme.inc
 *
 * Default template: harmonize-admin-fields-table.html.twig.
 *
 * @noinspection PhpUnused
 */
function template_preprocess_harmonize_admin_fields_table(array &$variables) : void {
  $form = $variables['element'];
  $rows = [];
  if (!empty($form['fields'])) {
    foreach (Element::children($form['fields']) as $name) {
      $row = [];
      foreach (Element::children($form['fields'][$name]) as $field) {
        if ($cell = Drupal::service('renderer')->render($form['fields'][$name][$field])) {
          $row[] = $cell;
        }
      }
      $row = [
        'data' => $row,
        'data-field-row-id' => $name,
      ];
      if (!empty($form['fields'][$name]['description']['#value'])) {
        $row['title'] = strip_tags($form['fields'][$name]['description']['#value']);
      }
      $rows[] = $row;
    }
  }

  $variables['note'] = $form['note'] ?? '';
  unset($form['note'], $form['submit']);

  $variables['table'] = [
    '#theme' => 'table',
    '#header' => $form['#header'] ?? NULL,
    '#rows' => $rows,
    '#empty' => '',
  ];
}

/**
 * Prepares variables for harmonize_admin_fields_table form templates.
 *
 * @param array $variables
 *   Associative array of template variables, with the following structure:
 *   - element: Associative array with the following keys:
 *     - form: A render element representing the form.
 *     - note: The table note.
 *
 * @see search_api.theme.inc
 *
 * Default template: harmonize-admin-fields-table.html.twig.
 *
 * @noinspection PhpUnused
 */
function template_preprocess_harmonize_admin_visualize_history_table(array &$variables) : void {
  $form = $variables['element'];
  $rows = [];
  if (!empty($form['rows'])) {
    foreach (Element::children($form['rows']) as $rowId) {
      $row = [];
      foreach (Element::children($form['rows'][$rowId]) as $cellId) {
        if ($cell = Drupal::service('renderer')->render($form['rows'][$rowId][$cellId])) {
          $row[] = $cell;
        }
      }
      $row = [
        'data' => $row,
        'data-field-row-id' => $rowId,
      ];
      $rows[] = $row;
    }
  }

  $variables['table'] = [
    '#theme' => 'table',
    '#header' => $form['#header'] ?? NULL,
    '#rows' => $rows,
    '#empty' => '',
  ];
}
