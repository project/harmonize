<?php

namespace Drupal\harmonize\Harmonizer;

/**
 * Provides an interface for Harmonizers.
 *
 * Harmonizers are objects assigned to preprocess a Drupal entity/object.
 *
 * We say entity and/or object because other data structures,
 * such as Regions and Menus, can also be preprocessed.
 *
 * @package Drupal\harmonize\Harmonizer
 */
interface HarmonizerInterface {

  /**
   * Preprocess data or the entity/object assigned to this Harmonizer.
   *
   * @return mixed
   *   Return the harmonizer's pre-processed data.
   *
   * @noinspection PhpMissingReturnTypeInspection
   */
  public function harmonize();

  /**
   * Get this harmonizer's suggestions, cached.
   *
   * Alleviate load and saved in class for dev consultation.
   *
   * @return array
   *   Array of loading suggestions.
   */
  public function getCachedSuggestions() : array;

  /**
   * Reload this harmonizer's suggestions.
   *
   * Reload suggestions when flags are changed or added.
   */
  public function reloadSuggestions() : void;

  /**
   * Generate Cache Tags for this harmonizer.
   *
   * Each type of Harmonizer should generate its own cache tags.
   * This affects how caching is managed for the harmonizers.
   */
  public function getCacheTags() : array;

  /**
   * Return whether the result of this harmonizer should be cached.
   *
   * @return bool
   *   Returns TRUE if the result should be cached. FALSE otherwise.
   */
  public function isCacheable() : bool;

  /**
   * Return whether caching for this harmonizer is enabled.
   *
   * @return bool
   *   Returns TRUE if caching is enabled FALSE otherwise.
   */
  public function cacheEnabled() : bool;

  /**
   * Get Data field of the harmonizer.
   *
   * @return mixed
   *   Return the harmonizer's data.
   *
   * @noinspection PhpMissingReturnTypeInspection
   */
  public function getData();

  /**
   * Set Data field of the harmonizer.
   *
   * @param mixed $data
   *   Set the harmonizer's data to a given value.
   *
   * @noinspection PhpMissingParamTypeInspection
   */
  public function setData($data) : void;

  /**
   * Get the object property of the current harmonizer.
   *
   * @return mixed
   *   Return the object of the current harmonizer.
   *
   * @noinspection PhpMissingReturnTypeInspection
   */
  public function getObject();

  /**
   * Get the type of the harmonizer.
   *
   * @return string
   *   Return the type of harmonizer.
   */
  public function getType() : string;

  /**
   * Retrieve signature from this Harmonizer.
   *
   * @return string
   *   Signature in string format.
   */
  public function getSignature() : string;

  /**
   * Set signature for this harmonizer.
   *
   * @param string $signature
   *   String to set the signature to.
   */
  public function setSignature(string $signature) : void;

  /**
   * Get the signature property of the current harmonizer.
   *
   * If incremental parts are requested, an array will be returned containing
   * each part of the harmonizer's signature incrementally appended.
   *
   * Example: A signature signed "entity.node.basic_page" will return an array
   * with the following values:
   * ['entity', 'entity.node', 'entity.node.basic_page'].
   *
   * The incremental signature array form is used in many of the module's
   * features.
   *
   * @param bool $incremental_parts
   *   Determines whether to get the signature in incremental parts. This will
   *   return the signature in an array instead of a string, and it will be
   *   split in the proper parts according to the rule above.
   *
   * @return array|string
   *   Return the signature of the current harmonizer.
   *   Return array if parts are requested.
   */
  public function getSignatureParts(bool $incremental_parts = FALSE) : array|string;

  /**
   * Get flags set to the harmonizer.
   *
   * Flags can be used to customize the actions of a harmonizer.
   *
   * @return array
   *   Returns value of the requested flag.
   */
  public function getFlags() : array;

  /**
   * Get the value of a flag set to the harmonizer.
   *
   * Flags can be used to customize the actions of a harmonizer.
   *
   * @param string $key
   *   Key of the flag value to look fetch.
   *
   * @return mixed
   *   Returns value of the requested flag.
   *
   * @noinspection PhpMissingReturnTypeInspection
   */
  public function getFlag(string $key);

  /**
   * Check if the current harmonizer has a flag set.
   *
   * Flags can be used to customize the actions of a harmonizer.
   *
   * @param string $key
   *   Key of the flag to look for.
   *
   * @return bool
   *   Returns true if the flag is planted. Returns false otherwise.
   */
  public function hasFlag(string $key) : bool;

  /**
   * Set a flag to the current harmonizer.
   *
   * Flags can be used to customize the actions of a harmonizer.
   *
   * @param string $key
   *   Key of the flag to be set.
   * @param mixed $value
   *   Value of the flag to be set.
   *
   * @noinspection PhpMissingParamTypeInspection
   */
  public function setFlag(string $key, $value) : void;

  /**
   * Set flags to the current harmonizer.
   *
   * Flags can be used to customize the actions of a harmonizer.
   *
   * @param array $flags
   *   Array of flags to set on this harmonizer.
   */
  public function setFlags(array $flags) : void;

  /**
   * Get flags that should be inherited by child harmonizers.
   *
   * @return array
   *   Array of flags that should be bequeathed.
   */
  public function getBequeathedFlags() : array;

  /**
   * Get the Trace of the Harmonizer.
   *
   * @return array
   *   Returns the harmonizer trace.
   */
  public function getTrace() : array;

  /**
   * Add a Trace step in the Harmonizer.
   *
   * @param string $trace
   *   The trace step, which is a string detailing what occurred.
   * @param bool $logData
   *   Flag. Set to TRUE if the data should be logged.
   */
  public function trace(string $trace, bool $logData = FALSE) : void;

  /**
   * Add a Trace step in the Harmonizer.
   *
   * @return bool
   *   Returns TRUE if tracing is activated, returns FALSE otherwise.
   */
  public function traceable() : bool;

  /**
   * Get the parent harmonizer for this harmonizer.
   *
   * @return \Drupal\harmonize\Harmonizer\HarmonizerInterface|null
   *   Return the parent harmonizer class linked to this harmonizer, if any.
   */
  public function getParentHarmonizer() : ?HarmonizerInterface;

  /**
   * Set Parent Harmonizer of this Harmonizer.
   *
   * @param HarmonizerInterface $harmonizer
   *   The parent harmonizer object.
   */
  public function setParentHarmonizer(HarmonizerInterface $harmonizer);

  /**
   * Get the depth of this harmonizer.
   *
   * @return int
   *   Return the depth of this harmonizer.
   */
  public function getDepth() : int;

  /**
   * Set Depth of this Harmonizer.
   *
   * @param int $depth
   *   The depth to set to the harmonizer.
   */
  public function setDepth(int $depth);

}
