<?php

namespace Drupal\harmonize\Harmonizer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer\EntityFieldHarmonizerFactory;
use Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityHarmonizerFactory;
use Drupal\harmonize\Harmonizer\FormHarmonizer\FormHarmonizerFactory;
use Drupal\harmonize\Harmonizer\MenuHarmonizer\MenuHarmonizerFactory;
use Drupal\harmonize\Harmonizer\RegionHarmonizer\RegionHarmonizerFactory;
use Drupal\harmonize\Service\Harmonize;

/**
 * Handles the creation of the proper harmonizer for a given Drupal object.
 *
 * There are many types of objects in Drupal. Each object may have their own
 * specific arrangements that will affect the way the data is pre-processed.
 * We will handle these in respective classes.
 *
 * By checking the object type, the Factory builds the appropriate Harmonizer.
 *
 * @package Drupal\harmonize\Harmonizer
 */
final class MasterHarmonizerFactory {

  /**
   * Build the appropriate Harmonizer with the given object.
   *
   * This Master Factory calls appropriate sub-factories.
   *
   * @param mixed $object
   *   Drupal Object that is currently being processed.
   * @param array $flags
   *   Flags that affect the actions taken when harmonizing the object.
   * @param \Drupal\harmonize\Service\Harmonize $harmonizeService
   *   Main service for the Harmonize module.
   *
   * @return \Drupal\harmonize\Harmonizer\HarmonizerInterface|null
   *   Returns the proper Harmonizer. If it fails, returns null.
   *
   * @noinspection PhpMissingParamTypeInspection
   */
  public static function build($object, array $flags, Harmonize $harmonizeService) : ?HarmonizerInterface {
    // If for some reason we try to harmonize NULL, let's get out of here.
    if ($object === NULL) {
      return NULL;
    }

    // First, check if the object is an Entity.
    if (is_object($object) && in_array(ContentEntityInterface::class, class_implements($object), TRUE)) {
      return EntityHarmonizerFactory::build($object, $flags, $harmonizeService);
    }

    // Next, we'll check if it's a field.
    if (is_object($object) && in_array(FieldItemListInterface::class, class_implements($object), TRUE)) {
      return EntityFieldHarmonizerFactory::build($object, $flags, $harmonizeService);
    }

    // Check if the object is a render array.
    if (is_array($object)) {
      // Check if the object is a form render array.
      if (isset($object['#type']) && $object['#type'] === 'form') {
        return FormHarmonizerFactory::build($object, $flags, $harmonizeService);
      }
      // Check if this object is a menu render array.
      if ((isset($object['menu_name']) && !empty($object['items'])) || (isset($object['#menu_name']) && !empty($object['#items']))) {
        return MenuHarmonizerFactory::build($object, $flags, $harmonizeService);
      }
      // Check if this object is a region render array.
      if (isset($object['region'])) {
        return RegionHarmonizerFactory::build($object, $flags, $harmonizeService);
      }
    }

    // Throw a Drupal log error if no Harmonizer was found for the given object.
    // Throw a status message too!
    // Umm...This has the potential to cause memory errors. Keep an eye out.
    if (is_object($object)) {
      $info = get_class($object);
    }
    elseif (is_array($object)) {
      $info = print_r($object, TRUE);
    }
    else {
      $info = 'No data co1uld be obtained at this time.';
      // We COULD just print_r, but that's dangerous as it could lead to memory
      // errors. So let's not!
    }
    $notice = t("Notice: Harmonize tried to preprocess an object that is currently incompatible.<br><br>See the data below.<br><br><code>@object</code>", [
      '@object' => $info,
    ]);
    $harmonizeService->loggerChannel->notice($notice);

    return NULL;
  }

}
