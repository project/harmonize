<?php

namespace Drupal\harmonize\Harmonizer\FormHarmonizer;

use Drupal\harmonize\Constants\HarmonizerTypes;
use Drupal\harmonize\Harmonizer\Harmonizer;
use Drupal\harmonize\Service\Harmonize;

/**
 * Provides a Harmonizer for Drupal Forms.
 *
 * @package Drupal\harmonize\Harmonizer\FormHarmonizer
 */
class FormHarmonizer extends Harmonizer implements FormHarmonizerInterface {

  /**
   * Drupal Variables array of the form being harmonized.
   *
   * @var array
   */
  protected array $form;

  /**
   * FormHarmonizer constructor.
   *
   * This has not been properly tested in a while. Use at your own discretion.
   *
   * @param array $form
   *   The render array of the form object being harmonized.
   * @param \Drupal\harmonize\Service\Harmonize $harmonizeService
   *   Harmonize module's main service containing Drupal services.
   * @param string $type
   *   The type of harmonizer defined by a string key.
   * @param string $signature
   *   The signature of the harmonizer generated by the factory.
   */
  public function __construct(
    array $form,
    Harmonize $harmonizeService,
    string $type,
    string $signature
  ) {
    $this->type = HarmonizerTypes::FORM;
    $this->form = $form;
    parent::__construct($form, $harmonizeService, $type, $signature);
  }

  /**
   * {@inheritDoc}
   */
  public function getForm() : array {
    return $this->form;
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
   */
  protected function getHarmonizedData() : array {
    $action = $this->getForm()['#action'] ?? NULL;
    $method = $this->getForm()['#method'] ?? NULL;
    $elements = $this->getFormElements();

    return [
      'action'   => $action,
      'method'   => $method,
      'elements' => $elements,
    ];
  }

  /**
   * Parse variables array and get all form elements.
   *
   * @return array
   *   Returns all form elements properly separated.
   */
  protected function getFormElements() : array {
    $elements = [];

    foreach ($this->getForm() as $id => $value) {
      if (!is_array($value)) {
        continue;
      }

      if (isset($value['#input']) && $value['#input'] === TRUE) {
        $fieldType = $value['#type'];

        $elements[$id] = match ($fieldType) {
          'input', 'textarea', 'textfield', 'search', 'password' => $this->inputTextField($value),
          'radio', 'select' => $this->inputOptionsField($value),
          default => $value,
        };
      }
    }

    return $elements;
  }

  /**
   * Properly format form text field.
   *
   * @param array $field
   *   An array of the field attributes.
   *
   * @return array
   *   Formatted array of the attributes for the input field element.
   *
   * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
   */
  protected function inputTextField(array $field) : array {
    return [
      'type'       => 'input',
      'label'      => $field['#title'] ?? NULL,
      'attributes' => [
        'type' => $field['#type'] ?? NULL,
        'name' => $field['#name'] ?? NULL,
      ],
    ];
  }

  /**
   * Properly format input options field.
   *
   * @param array $field
   *   An array of the field attributes.
   *
   * @return array
   *   Formatted array of the attributes for the input field element.
   *
   * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
   */
  protected function inputOptionsField(array $field) : array {
    return [
      'type'    => $field['#type'],
      'options' => $this->formatOptions($field['#options']),
    ];
  }

  /**
   * Properly format form field format options.
   *
   * @param array $options
   *   Raw format options of a given field.
   *
   * @return array
   *   Return the format options of the form field.
   */
  protected function formatOptions(array $options) : array {
    $formattedOptions = [];
    foreach ($options as $key => $option) {
      $formattedOptions[] = [
        'label'      => $option,
        'attributes' => [
          'value' => !is_int($key) ? $key : strtolower($option),
        ],
      ];
    }
    return $formattedOptions;
  }

  /**
   * {@inheritDoc}
   */
  protected function getSuggestions() : array {
    // Resolve some variables.
    $harmonizerType = $this->type;
    $formId = $this->getForm()['#id'];

    // Initial Suggestions.
    $suggestions = [
      $harmonizerType,
      $harmonizerType . "." . $formId,
    ];

    // Alterations, then return.
    $this->alterSuggestions($suggestions, $this);
    return $suggestions;
  }

}
