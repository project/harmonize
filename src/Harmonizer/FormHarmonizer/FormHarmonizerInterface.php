<?php

namespace Drupal\harmonize\Harmonizer\FormHarmonizer;

use Drupal\harmonize\Harmonizer\HarmonizerInterface;

/**
 * Provides an interface for Form Harmonizers.
 *
 * @package Drupal\harmonize\Harmonizer
 */
interface FormHarmonizerInterface extends HarmonizerInterface {

  /**
   * Gets the form from the current harmonizer.
   *
   * @return array
   *   Raw render array of the form associated to this harmonizer.
   */
  public function getForm() : array;

}
