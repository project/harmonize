<?php

namespace Drupal\harmonize\Harmonizer\FormHarmonizer;

use Drupal\harmonize\Constants\HarmonizerFlags;
use Drupal\harmonize\Constants\HarmonizerTypes;
use Drupal\harmonize\Harmonizer\HarmonizerFactory;

/**
 * Manages which FormHarmonizer class should be used to preprocess Drupal forms.
 *
 * There is only one type for now, but this factory makes it easy to
 * extend for future form types or different types of form arrays.
 *
 * @package Drupal\harmonize\Harmonizer\FormHarmonizer
 */
class FormHarmonizerFactory extends HarmonizerFactory {

  /**
   * {@inheritdoc}
   */
  protected static function type() : string {
    return HarmonizerTypes::FORM;
  }

  /**
   * {@inheritdoc}
   */
  protected static function resolveSignature($object, array $flags = []) : string {
    // Add language.
    $lang = isset($flags[HarmonizerFlags::LANG]) && !empty($flags[HarmonizerFlags::LANG]) ? $flags[HarmonizerFlags::LANG] : \Drupal::languageManager()->getCurrentLanguage()->getId();

    // Generate a signature for the harmonizer about to be instantiated.
    // @todo ADD FORM ID HERE...VERY IMPORTANT LOL.
    return self::generateHarmonizerSignature(HarmonizerTypes::FORM, self::generateFlagsSignatures($flags), $lang);
  }

  /**
   * {@inheritdoc}
   */
  protected static function resolveClass($object) : string {
    return FormHarmonizer::class;
  }

}
