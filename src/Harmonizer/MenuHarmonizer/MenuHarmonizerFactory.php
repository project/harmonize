<?php

namespace Drupal\harmonize\Harmonizer\MenuHarmonizer;

use Drupal\harmonize\Constants\HarmonizerFlags;
use Drupal\harmonize\Constants\HarmonizerTypes;
use Drupal\harmonize\Harmonizer\HarmonizerFactory;

/**
 * Manages which MenuHarmonizer class should be used to pre-process the menu.
 *
 * There is only one type for now, but this factory makes it
 * easy to extend for future menu types or different types of menu arrays.
 *
 * @package Drupal\harmonize\Harmonizer\MenuHarmonizer
 */
class MenuHarmonizerFactory extends HarmonizerFactory {

  /**
   * {@inheritdoc}
   */
  protected static function type() : string {
    return HarmonizerTypes::MENU;
  }

  /**
   * {@inheritdoc}
   */
  protected static function resolveSignature($object, array $flags = []) : string {
    // Add language.
    $lang = isset($flags[HarmonizerFlags::LANG]) && !empty($flags[HarmonizerFlags::LANG]) ? $flags[HarmonizerFlags::LANG] : \Drupal::languageManager()->getCurrentLanguage()->getId();

    // Generate a signature for the harmonizer about to be instantiated.
    // Example: menu.main_navigation.
    $menuName = $object['menu_name'] ?? $object['#menu_name'];
    return self::generateHarmonizerSignature(HarmonizerTypes::MENU, $menuName, self::generateFlagsSignatures($flags), $lang);
  }

  /**
   * {@inheritdoc}
   */
  protected static function resolveClass($object) : string {
    return MenuHarmonizer::class;
  }

}
