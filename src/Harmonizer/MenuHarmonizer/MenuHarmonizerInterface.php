<?php

namespace Drupal\harmonize\Harmonizer\MenuHarmonizer;

use Drupal\harmonize\Harmonizer\HarmonizerInterface;

/**
 * Provides an interface for Menu Harmonizers.
 *
 * @package Drupal\harmonize\Harmonizer
 */
interface MenuHarmonizerInterface extends HarmonizerInterface {

  /**
   * Get the name of the Menu being harmonized.
   *
   * @return string
   *   Return the menu name of the current harmonizer.
   */
  public function getMenuName() : string;

}
