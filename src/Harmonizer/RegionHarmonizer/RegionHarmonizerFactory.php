<?php

namespace Drupal\harmonize\Harmonizer\RegionHarmonizer;

use Drupal\harmonize\Constants\HarmonizerFlags;
use Drupal\harmonize\Constants\HarmonizerTypes;
use Drupal\harmonize\Harmonizer\HarmonizerFactory;

/**
 * Manages which RegionHarmonizer class should be used to preprocess the region.
 *
 * There is only one type for now, but this factory makes it
 * easy to extend for future region types that might appear in the future.
 *
 * Let's be real though...We won't be seeing "Region Types" anytime soon!
 *
 * @package Drupal\harmonize\Harmonizer\RegionHarmonizer
 */
class RegionHarmonizerFactory extends HarmonizerFactory {

  /**
   * {@inheritdoc}
   */
  protected static function type() : string {
    return HarmonizerTypes::REGION;
  }

  /**
   * {@inheritdoc}
   */
  protected static function resolveSignature($object, array $flags = []) : string {
    // Add language.
    $lang = isset($flags[HarmonizerFlags::LANG]) && !empty($flags[HarmonizerFlags::LANG]) ? $flags[HarmonizerFlags::LANG] : \Drupal::languageManager()->getCurrentLanguage()->getId();

    // Generate a signature for the harmonizer about to be instantiated.
    // Example: region.header.
    return self::generateHarmonizerSignature(HarmonizerTypes::REGION, $object['region'], self::generateFlagsSignatures($flags), $lang);
  }

  /**
   * {@inheritdoc}
   */
  protected static function resolveClass($object) : string {
    return RegionHarmonizer::class;
  }

}
