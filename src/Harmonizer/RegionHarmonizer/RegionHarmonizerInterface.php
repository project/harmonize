<?php

namespace Drupal\harmonize\Harmonizer\RegionHarmonizer;

use Drupal\harmonize\Harmonizer\HarmonizerInterface;

/**
 * Provides an interface for Region Harmonizers.
 *
 * @package Drupal\harmonize\Harmonizer
 */
interface RegionHarmonizerInterface extends HarmonizerInterface {

  /**
   * Get the name of the Region being harmonized.
   *
   * @return string
   *   Return the menu name of the current harmonizer.
   */
  public function getRegionName() : string;

}
