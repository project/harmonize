<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\harmonize\Constants\HarmonizerFlags;
use Drupal\harmonize\Constants\HarmonizerTypes;
use Drupal\harmonize\Harmonizer\HarmonizerFactory;

/**
 * Handles the creation of the proper harmonizer for a given Entity.
 *
 * There are many types of Entities in Drupal. Each Entity may
 * have their own specific arrangements that will affect the way the data is
 * preprocessed. We will handle these in respective classes.
 *
 * By checking the entity type, the Factory builds the appropriate Harmonizer.
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityHarmonizer
 */
final class EntityHarmonizerFactory extends HarmonizerFactory {

  /**
   * {@inheritdoc}
   */
  protected static function type() : string {
    return HarmonizerTypes::ENTITY;
  }

  /**
   * {@inheritdoc}
   */
  public static function resolveSignature($object, array $flags = []) : string {
    // Generate a signature for the harmonizer about to be instantiated.
    // Example: entity.node.basic_page.
    $signature = self::generateHarmonizerSignature(HarmonizerTypes::ENTITY, $object->getEntityTypeId(), $object->bundle(), self::generateFlagsSignatures($flags), $object->id());

    // If we're in a content entity, we append the revision ID to the signature.
    if ($object instanceof ContentEntityInterface) {
      $loadedRevisionId = $object->getLoadedRevisionId();
      if ($loadedRevisionId !== NULL) {
        $signature = self::generateHarmonizerSignature($signature, $object->getLoadedRevisionId());
      }
    }

    // Add language.
    $lang = isset($flags[HarmonizerFlags::LANG]) && !empty($flags[HarmonizerFlags::LANG]) ? $flags[HarmonizerFlags::LANG] : \Drupal::languageManager()->getCurrentLanguage()->getId();
    $signature .= '.' . $lang;

    return $signature;
  }

  /**
   * {@inheritdoc}
   */
  protected static function resolveClass($object) : string {
    // Get the type of entity.
    // i.e. "node", "paragraph", "media", etc.
    $entity_type = $object->getEntityTypeId();

    // Depending on the type, create the appropriate "Harmonizer".
    return match ($entity_type) {
      'node' => NodeEntityHarmonizer::class,
      'file' => FileEntityHarmonizer::class,
      'media' => MediaEntityHarmonizer::class,
      'paragraph' => ParagraphEntityHarmonizer::class,
      'taxonomy_term' => TaxonomyTermEntityHarmonizer::class,
      'menu_link_content' => MenuLinkContentEntityHarmonizer::class,
      'image_style' => ImageStyleEntityHarmonizer::class,
      default => EntityHarmonizer::class,
    };
  }

}
