<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

/**
 * Handles exceptions for 'text_with_summary' type fields.
 *
 * @property \Drupal\harmonize\Service\Harmonize $harmonizeService
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
class TextWithSummaryEntityFieldHarmonizer extends EntityFieldHarmonizer {

  /**
   * {@inheritdoc}
   */
  protected function process(array $value, int $i) : array {
    $value['processed'] = check_markup($value['value'], $value['format'], $this->harmonizeService->languageManager->getCurrentLanguage()->getId());
    unset($value['format'], $value['_attributes']);
    return $value;
  }

}
