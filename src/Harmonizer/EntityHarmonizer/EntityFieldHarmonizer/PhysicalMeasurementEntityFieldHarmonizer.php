<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

/**
 * Handles exceptions for 'physical_measurement' type fields.
 *
 * Note that this harmonizer assumes that the 'physical' contrib module is
 * installed.
 *
 * @link https://www.drupal.org/project/physical
 *
 * @property \Drupal\harmonize\Service\Harmonize $harmonizeService
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
class PhysicalMeasurementEntityFieldHarmonizer extends EntityFieldHarmonizer {

  /**
   * {@inheritdoc}
   */
  protected function process(array $value, int $i) : array {
    // Simply return the base value array, which in turns contains all
    // measurement properties.
    return $value;
  }

}
