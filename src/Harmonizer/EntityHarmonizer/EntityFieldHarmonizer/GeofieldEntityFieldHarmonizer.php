<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

/**
 * Handles exceptions for 'geofield' type fields.
 *
 * Note that this harmonizer assumes that the geofield contrib module
 * is installed.
 *
 * @link https://www.drupal.org/project/geofield
 *
 * @property \Drupal\harmonize\Service\Harmonize $harmonizeService
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
class GeofieldEntityFieldHarmonizer extends EntityFieldHarmonizer {

  /**
   * {@inheritdoc}
   */
  protected function process(array $value, int $i) : array {
    // Simply return the base value array, which in turns contains all geofield
    // properties. Some properties may be empty or null, depending on the field
    // values requirements settings.
    return $value;
  }

}
