<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

/**
 * Handles exceptions for 'image' type fields.
 *
 * @property \Drupal\harmonize\Service\Harmonize $harmonizeService
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
class ImageEntityFieldHarmonizer extends EntityFieldHarmonizer {

  /**
   * {@inheritdoc}
   */
  protected function process(array $value, int $i) : array|string|NULL {
    // Get the entity from the field value.
    $entity = $this->getFieldData()[$i]->entity;

    if ($entity === NULL) {
      return NULL;
    }

    // Image file data, harmonized by yours truly.
    $file = $this->harmonizeService->harmonize($entity);

    // Account for possibility of alteration objects unsetting default data.
    if (!isset($file['href'])) {
      return $file;
    }

    $src = $file['href'];
    $alt = $this->getFieldData()->getValue()[$i]['alt'] ?? '';
    $title = $this->getFieldData()->getValue()[$i]['title'] ?? '';
    $data = [
      'uri' => $file['uri'],
    ];
    $attributes = [
      'src'   => $src,
      'alt'   => $alt,
      'title' => $title,
    ];

    return [
      'uri'        => $data['uri'],
      'attributes' => $attributes,
    ];
  }

}
