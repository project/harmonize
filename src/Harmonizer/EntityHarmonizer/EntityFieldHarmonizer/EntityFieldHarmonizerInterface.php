<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\harmonize\Harmonizer\HarmonizerInterface;

/**
 * Provides an interface for Entity Field Harmonizers.
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer
 */
interface EntityFieldHarmonizerInterface extends HarmonizerInterface {

  /**
   * Get the field type for this harmonizer.
   *
   * @return string
   *   Return the field type of the field for this harmonizer.
   */
  public function getFieldType() : string;

  /**
   * Get the field name for this harmonizer.
   *
   * @return string
   *   Return the field name of the field for this harmonizer.
   */
  public function getFieldName() : string;

  /**
   * Get the field data for this harmonizer.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface
   *   Raw field data returned.
   */
  public function getFieldData() : FieldItemListInterface;

}
