<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

/**
 * Handles exceptions for 'office_hours' type fields.
 *
 * Note that this harmonizer assumes that the 'office_hours'
 * contrib module is installed.
 *
 * @link https://www.drupal.org/project/office_hours
 *
 * @property \Drupal\harmonize\Service\Harmonize $harmonizeService
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
class OfficeHoursEntityFieldHarmonizer extends EntityFieldHarmonizer {

  /**
   * {@inheritdoc}
   */
  protected function process(array $value, int $i) : array {
    // Simply return the base value array, which in turns contains all
    // office hours properties.
    return $value;
  }

}
