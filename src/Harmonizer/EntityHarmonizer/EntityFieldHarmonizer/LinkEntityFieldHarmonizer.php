<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

use Drupal\Core\Url;
use Drupal\node\NodeInterface;

/**
 * Handles harmonization exceptions for 'link' type fields.
 *
 * @property \Drupal\harmonize\Service\Harmonize $harmonizeService
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
class LinkEntityFieldHarmonizer extends EntityFieldHarmonizer {

  /**
   * {@inheritdoc}
   */
  protected function process(array $value, int $i) : array {
    $label = $value['title'] ?? '';

    try {
      $attributes = [
        'href'   => Url::fromUri($value['uri'])->toString(),
        'target' => $value['options']['attributes']['target'] ?? '_self',
      ];
    }
    catch (\InvalidArgumentException) {
      return [];
    }

    // The 'href' key may sometimes have a node object instead of a URL.
    // Use an above function to format the link to this node.
    if (isset($attributes['href']) && $attributes['href'] instanceof NodeInterface) {
      $attributes['href'] = Url::fromUri('internal:/node/' . $attributes['href']->id(), ['absolute' => TRUE])->toString();
    }

    return [
      'label'      => $label,
      'attributes' => $attributes,
    ];
  }

}
