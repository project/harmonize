<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

use Drupal\harmonize\Constants\HarmonizerFlags;
use Drupal\harmonize\Constants\HarmonizerTypes;
use Drupal\harmonize\Harmonizer\HarmonizerFactory;

/**
 * Handles the creation of the proper field harmonizer with a given Field.
 *
 * There are many types of Fields in Drupal. Each
 * Field will have their own specific arrangements that will affect the way the
 * data is pre-processed. We will handle these in respective classes.
 *
 * By checking the field type, the Factory builds the appropriate
 * EntityFieldHarmonizer.
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
final class EntityFieldHarmonizerFactory extends HarmonizerFactory {

  /**
   * {@inheritdoc}
   */
  protected static function type() : string {
    return HarmonizerTypes::ENTITY_FIELD;
  }

  /**
   * {@inheritdoc}
   */
  protected static function resolveSignature($object, array $flags = []) : string {
    // Add language.
    $lang = isset($flags[HarmonizerFlags::LANG]) && !empty($flags[HarmonizerFlags::LANG]) ? $flags[HarmonizerFlags::LANG] : \Drupal::languageManager()->getCurrentLanguage()->getId();

    // Generate a signature for the harmonizer about to be instantiated.
    // Example: field.email.field_contact_email.
    return self::generateHarmonizerSignature(
      HarmonizerTypes::ENTITY_FIELD,
      $object->getFieldDefinition()->getType(),
      $object->getName(),
      $object->getFieldDefinition()->getTargetEntityTypeId(),
      $object->getFieldDefinition()->getTargetBundle(),
      self::generateFlagsSignatures($flags),
      $object->getEntity()->id(),
      $lang
    );
  }

  /**
   * {@inheritdoc}
   */
  protected static function resolveClass($object) : string {
    // Otherwise, we instantiate it and register it in the factory.
    // Get the type of field.
    $fieldType = $object->getFieldDefinition()->getType();

    // Depending on the type, create the appropriate HarmonizerInterface.
    $class = match ($fieldType) {
      'file' => FileEntityFieldHarmonizer::class,
      'boolean' => BooleanEntityFieldHarmonizer::class,
      'daterange' => DateRangeEntityFieldHarmonizer::class,
      'datetime' => DateTimeEntityFieldHarmonizer::class,
      'image' => ImageEntityFieldHarmonizer::class,
      'metatag' => MetaTagEntityFieldHarmonizer::class,
      'link' => LinkEntityFieldHarmonizer::class,
      'entity_reference' => EntityReferenceEntityFieldHarmonizer::class,
      'entity_reference_revisions' => EntityReferenceRevisionsEntityFieldHarmonizer::class,
      'text_with_summary' => TextWithSummaryEntityFieldHarmonizer::class,
      'color_field_type' => ColorEntityFieldHarmonizer::class,
      'address' => AddressEntityFieldHarmonizer::class,
      'geofield' => GeofieldEntityFieldHarmonizer::class,
      'physical_measurement' => PhysicalMeasurementEntityFieldHarmonizer::class,
      'office_hours' => OfficeHoursEntityFieldHarmonizer::class,
      default => EntityFieldHarmonizer::class,
    };

    // If the class is just default, we throw a log.
    if ($class === EntityFieldHarmonizer::class) {
      \Drupal::logger('harmonize')->warning(t('The field type @fieldType is currently not compatible with Harmonize. Consider requesting the creation of a specific EntityFieldHarmonizer to cover this field type.', ['@fieldType' => $fieldType]));
    }

    return $class;
  }

}
