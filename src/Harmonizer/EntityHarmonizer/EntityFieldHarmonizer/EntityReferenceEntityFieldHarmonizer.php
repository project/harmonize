<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\harmonize\Constants\HarmonizeConfig;
use Drupal\harmonize\Constants\HarmonizerFlags;
use Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityHarmonizerFactory;
use Drupal\harmonize\Service\Harmonize;

/**
 * Handles exceptions for 'entity_reference' type fields.
 *
 * @property Harmonize $harmonizeService
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
class EntityReferenceEntityFieldHarmonizer extends EntityFieldHarmonizer {

  /**
   * Flag to store whether we sent a trace on the view mode.
   *
   * @var bool
   */
  protected bool $viewModeTraceSent = FALSE;

  /**
   * Flag to store whether we sent a trace on the view mode's absence.
   *
   * @var bool
   */
  protected bool $viewModeNonexistentTraceSent = FALSE;

  /**
   * Flag to store whether we sent a trace on the view mode's status.
   *
   * @var bool
   */
  protected bool $viewModeStatusTraceSent = FALSE;

  /**
   * Flag to store whether we sent a trace on the view mode's inheritance.
   *
   * @var bool
   */
  protected bool $viewModeInheritanceTraceSent = FALSE;

  /**
   * Flag to store whether we already sent a trace pertaining to the style.
   *
   * @var bool
   */
  protected bool $styleTraceSent = FALSE;

  /**
   * Flag to store whether we sent a trace on the style's absence.
   *
   * @var bool
   */
  protected bool $styleNonexistentTraceSent = FALSE;

  /**
   * Flag to store whether we sent a trace on the auto harmonize configurations.
   *
   * @var bool
   */
  protected bool $autoHarmonizeTraceSent = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function process(array $value, int $i) {
    // Get the entity from the field value.
    $entity = $this->getFieldData()[$i]->entity;

    if ($entity === NULL) {
      return NULL;
    }

    // Get the translated entity.
    // Only do this if the entity is a content entity.
    if ($entity instanceof ContentEntityInterface) {
      $entity = $this->harmonizeService->helpers->ensureTranslated($entity);
    }

    // Build harmonizer for this entity.
    $flags = [
      HarmonizerFlags::PARENT => $this->getParentHarmonizer(),
    ];
    // If we have any flags here, the harmonizer must inherit some of them.
    if (!empty($this->flags)) {
      $flags[HarmonizerFlags::LANG] = $this->flags[HarmonizerFlags::LANG] ?? '';
      $flags[HarmonizerFlags::SKIP_CACHE] = $this->getParentHarmonizer()->getFlag(HarmonizerFlags::SKIP_CACHE) ?? FALSE;
    }

    // If the parent harmonizer has a view mode set,
    // we want to make sure we follow display settings rules.
    if ($this->parentHarmonizer->hasFlag(HarmonizerFlags::VIEW_MODE)) {
      // Attempted to get the configured view mode.
      $viewMode = $this->getConfiguredViewMode();

      if ($viewMode !== NULL) {
        if ($this->harmonizeService->helpers->viewModeExists($entity->getEntityTypeId(), $viewMode)) {
          if ($this->harmonizeService->helpers->viewModeEnabled($entity->getEntityTypeId(), $entity->bundle(), $viewMode)) {
            if (!$this->viewModeStatusTraceSent) {
              $this->parentHarmonizer->trace($this->t("View mode <strong><code>[@mode]</code></strong> is enabled. The referenced entities in the <strong><code>[@field]</code></strong> field will be processed accordingly.", [
                '@mode'  => $viewMode,
                '@field'  => $this->fieldName,
              ]));
            }
            $this->viewModeStatusTraceSent = TRUE;
            $flags[HarmonizerFlags::VIEW_MODE] = $viewMode;
          }
          else {
            if (!$this->viewModeStatusTraceSent) {
              $this->parentHarmonizer->trace($this->t("View mode <strong><code>[@mode]</code></strong> is not enabled for the referenced entities in the <strong><code>[@field]</code></strong> field. They will be processed using the <code>default</code> view mode.", [
                '@mode'  => $viewMode,
                '@field'  => $this->fieldName,
              ]));
            }
            $this->viewModeStatusTraceSent = TRUE;
            $flags[HarmonizerFlags::VIEW_MODE] = 'default';
          }
        }
        else {
          if (!$this->viewModeNonexistentTraceSent) {
            $this->parentHarmonizer->trace($this->t("View mode <strong><code>[@mode]</code></strong> does not exist. The referenced entities in the <strong><code>[@field]</code></strong> field will be processed with the <code>default</code> view mode.", [
              '@mode'  => $viewMode,
              '@field'  => $this->fieldName,
            ]));
          }
          $this->viewModeNonexistentTraceSent = TRUE;
        }
      }
    }

    // Now we want to check if we need to set a style.
    $style = $this->getConfiguredStyle();
    if ($style !== NULL) {
      if ($this->harmonizeService->helpers->styleExists($style)) {
        $flags[HarmonizerFlags::STYLE] = $style;
      }
      else {
        if (!$this->styleNonexistentTraceSent) {
          $this->parentHarmonizer->trace($this->t("Style <strong><code>[@style]</code></strong> does not exist. Style filtering will not be applied for field <strong><code>[@field]</code></strong>", [
            '@style'  => $style,
            '@field'  => $this->fieldName,
          ]));
        }
        $this->styleNonexistentTraceSent = TRUE;
      }
    }

    // Build our harmonizer.
    $harmonizer = EntityHarmonizerFactory::build($entity, $flags, $this->harmonizeService);

    // Depth check.
    // This is an infinite loop failsafe.
    if ($harmonizer->getDepth() > Harmonize::$config[HarmonizeConfig::MAIN__MAX_DEPTH]) {
      return $harmonizer;
    }

    // If we're dealing with items set to AUTOMATICALLY harmonize,
    // we return the harmonization. Otherwise, we return the harmonizer.
    // First, we check in configurations.
    if (in_array($entity->getEntityTypeId(), Harmonize::$config[HarmonizeConfig::MAIN__AUTO_HARMONIZED_ENTITIES])) {
      if (!$this->autoHarmonizeTraceSent) {
        $this->parentHarmonizer->trace($this->t("Nested harmonization of referenced <strong><code>[@entity]</code></strong> @entities performed for field <strong><code>[@field]</code></strong>. This was configured in the main Harmonize settings.", [
          '@field'  => $this->fieldName,
          '@entity' => $entity->getEntityTypeId(),
          '@entities' => $this->cardinality === 1 ? $this->t('entity') : $this->t('entities'),
        ]));
        $this->autoHarmonizeTraceSent = TRUE;
      }
      return $harmonizer->harmonize();
    }

    // Bundle check too for configurations.
    if (!empty($entity->bundle()) && in_array($entity->getEntityTypeId() . '.' . $entity->bundle(), Harmonize::$config[HarmonizeConfig::MAIN__AUTO_HARMONIZED_ENTITIES])) {
      if (!$this->autoHarmonizeTraceSent) {
        $this->parentHarmonizer->trace($this->t("Nested harmonization of referenced <strong><code>[@entity]</code></strong> @entities performed for field <strong><code>[@field]</code></strong>. This was configured in the main Harmonize settings.", [
          '@field' => $this->fieldName,
          '@entity' => $entity->getEntityTypeId(),
          '@entities' => $this->cardinality === 1 ? $this->t('entity') : $this->t('entities'),
        ]));
        $this->autoHarmonizeTraceSent = TRUE;
      }
      return $harmonizer->harmonize();
    }

    // Now we check in rules & view mode settings as well.
    if ($this->isAutoHarmonizable()) {
      return $harmonizer->harmonize();
    }

    // Return harmonizer.
    return $harmonizer;
  }

  /**
   * Get the configured view mode, if any.
   */
  protected function getConfiguredViewMode() : ?string {
    // Get the parent entity.
    $parentEntityTypeId = $this->fieldDefinition->getTargetEntityTypeId();
    $parentBundle = $this->fieldDefinition->getTargetBundle();
    $parentViewMode = $this->parentHarmonizer->getFlag(HarmonizerFlags::VIEW_MODE);

    // Attempt to get preprocess settings first.
    $settings = $this->harmonizeService->helpers->loadEntitySettings($parentEntityTypeId, $parentBundle, $parentViewMode);

    // If preprocess settings are found, we use those.
    if (!empty($settings)) {
      // Get component settings for this field.
      // If we're here, then theoretically there is no reason why the field
      // specific settings wouldn't exist.
      // But an extra check doesn't hurt, right?
      $fieldSettings = $settings->getComponent($this->fieldName);
      if (!empty($fieldSettings)) {
        $viewMode = $fieldSettings['settings']['view_mode'];

        if ($viewMode !== 'inherit') {
          if (!$this->viewModeTraceSent) {
            $this->parentHarmonizer->trace($this->t("Entities in field <strong><code>[@field]</code></strong> are configured to be rendered in View mode <strong><code>[@mode]</code></strong> via preprocess settings. They will be processed accordingly", [
              '@mode' => $fieldSettings['settings']['view_mode'],
              '@field'  => $this->fieldName,
            ]));
            $this->viewModeTraceSent = TRUE;
          }
          return $viewMode;
        }
        else {
          if (!$this->viewModeInheritanceTraceSent) {
            $this->parentHarmonizer->trace($this->t("Entities in field <strong><code>[@field]</code></strong> are configured to inherit the view mode configured in Display settings.", [
              '@mode' => $fieldSettings['settings']['view_mode'],
              '@field'  => $this->fieldName,
            ]));
            $this->viewModeInheritanceTraceSent = TRUE;
          }
        }
      }
    }

    // We fall back to Display Settings if no preprocess settings are found,
    // or if inheritance is detected.
    try {
      $settings = $this->harmonizeService->helpers->getViewModeDisplaySettings($parentEntityTypeId, $parentBundle, $parentViewMode, $this->fieldName);
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException) {
      return NULL;
    }

    // If there are no settings, we can stop here.
    if (empty($settings)) {
      return NULL;
    }

    // If we found a view mode by now, we can return it.
    if (!empty($settings['view_mode'])) {
      if (!$this->viewModeTraceSent) {
        $this->parentHarmonizer->trace($this->t("Entities in field <strong><code>[@field]</code></strong> are configured to be rendered in View mode <strong><code>[@mode]</code></strong> via display settings. They will be processed accordingly", [
          '@mode' => $settings['view_mode'],
          '@field'  => $this->fieldName,
        ]));
        $this->viewModeTraceSent = TRUE;
      }
      return $settings['view_mode'];
    }

    // Otherwise, we have no view mode to use.
    return NULL;
  }

  /**
   * Get the configured style, if any.
   */
  protected function getConfiguredStyle() : ?string {
    // Get our rule configurations.
    $rules = Harmonize::$config[HarmonizeConfig::ENTITY_RULES__RULES];

    // Get harmony configurations from the theme if any.
    $themeRules = $this->harmonizeService->helpers->getHarmonyThemeRulesConfig($this->getFlag(HarmonizerFlags::THEME) ?? NULL);

    // If both configurations are null, we have nothing to do here.
    if (empty($rules) && empty($themeRules)) {
      return NULL;
    }

    // Theme rules take precedence when it comes to styles,
    // so we'll try to get a style from them first.
    $style = $this->getStyleFromRules($themeRules);

    // If we found a style by now, we can return it.
    if (!empty($style)) {
      if (!$this->styleTraceSent) {
        $this->parentHarmonizer->trace($this->t("Style <strong><code>[@style]</code></strong> is set to be used when processing entities in <strong><code>[@field]</code></strong>. This was configured in Entity Rules (Theme)", [
          '@style' => $style,
          '@field'  => $this->fieldName,
        ]));
        $this->styleTraceSent = TRUE;
      }
      return $style;
    }

    // Now we can check in the regular rules.
    $style = $this->getStyleFromRules($rules);

    // If we found a style by now, we can return it.
    if (!empty($style)) {
      if (!$this->styleTraceSent) {
        $this->parentHarmonizer->trace($this->t("Style <strong><code>[@style]</code></strong> is set to be used when processing entities for field <strong><code>[@field]</code></strong>. This was configured in Entity Rules (Config)", [
          '@style' => $style,
          '@field'  => $this->fieldName,
        ]));
        $this->styleTraceSent = TRUE;
      }
      return $style;
    }

    // At this point if we haven't returned yet, we didn't find a style.
    return NULL;
  }

  /**
   * Attempt to get a style that this field is configured to be harmonized with.
   *
   * The style is fetched from an array of rules.
   *
   * @param array|null $rules
   *   The array of rules, populated by config or a theme declaration file.
   *
   * @return string|null
   *   Returns the machine name of a style if found, returns NULL otherwise.
   */
  protected function getStyleFromRules(?array $rules) : ?string {
    // If there are no rules, we can leave.
    if (empty($rules)) {
      return NULL;
    }

    // Convenience.
    $fieldName = $this->fieldName;

    // Get the parent entity.
    $parentEntity = $this->fieldDefinition->getTargetEntityTypeId();
    $parentBundle = $this->fieldDefinition->getTargetBundle();

    // Get bundle rules.
    $bundleRules = $rules[$parentEntity]['bundles'][$parentBundle] ?? NULL;

    // Check if the field is set in the rules.
    if ($bundleRules && array_key_exists($fieldName, $bundleRules['rules'] ?? [])) {
      if (isset($bundleRules['rules'][$fieldName]['style'])) {
        return $bundleRules['rules'][$fieldName]['style'];
      }
    }

    // Check entity rules.
    $entityRules = $rules[$parentEntity] ?? NULL;

    // Check if the field is set in the rules.
    if ($entityRules && array_key_exists($fieldName, $entityRules['rules'] ?? [])) {
      if (isset($entityRules['rules'][$fieldName]['style'])) {
        return $entityRules['rules'][$fieldName]['style'];
      }
    }

    // At this point if we haven't returned yet, we didn't find a style.
    return NULL;
  }

  /**
   * Check if this field is set to be automatically harmonized.
   */
  protected function isAutoHarmonizable() : ?bool {
    // Get our rule configurations.
    $rules = Harmonize::$config[HarmonizeConfig::ENTITY_RULES__RULES];

    // Get harmony configurations from the theme if any.
    $themeRules = $this->harmonizeService->helpers->getHarmonyThemeRulesConfig($this->getFlag(HarmonizerFlags::THEME) ?? NULL);

    // Theme rules check.
    if (!empty($themeRules) && $this->isAutoHarmonizeInRules($themeRules)) {
      if (!$this->autoHarmonizeTraceSent) {
        $this->parentHarmonizer->trace($this->t("Nested harmonization performed for field <strong><code>[@field]</code></strong>. This was configured via Entity Rules (Theme)", [
          '@field' => $this->fieldName,
        ]));
        $this->autoHarmonizeTraceSent = TRUE;
      }
      return TRUE;
    }

    // Config rules check.
    if (!empty($rules) && $this->isAutoHarmonizeInRules($rules)) {
      if (!$this->autoHarmonizeTraceSent) {
        $this->parentHarmonizer->trace($this->t("Nested harmonization performed for field <strong><code>[@field]</code></strong>. This was configured via Entity Rules (Config)", [
          '@field' => $this->fieldName,
        ]));
        $this->autoHarmonizeTraceSent = TRUE;
      }
      return TRUE;
    }

    // Now we can check in entity settings, if we have to.
    if ($this->parentHarmonizer->hasFlag(HarmonizerFlags::VIEW_MODE)) {
      $parentEntityTypeId = $this->fieldDefinition->getTargetEntityTypeId();
      $parentBundle = $this->fieldDefinition->getTargetBundle();
      $parentViewMode = $this->parentHarmonizer->getFlag(HarmonizerFlags::VIEW_MODE);
      $settings = $this->harmonizeService->helpers->loadEntitySettings($parentEntityTypeId, $parentBundle, $parentViewMode);
      if (empty($settings)) {
        return FALSE;
      }
      $fieldSettings = $settings->getComponent($this->fieldName);
      if (empty($fieldSettings)) {
        return FALSE;
      }
      return $fieldSettings['settings']['auto_preprocess'];
    }

    return FALSE;
  }

  /**
   * Attempt to check if this field is set to be automatically processed.
   *
   * @param array|null $rules
   *   The array of rules, populated by config or a theme declaration file.
   *
   * @return string|null
   *   Returns the machine name of a style if found, returns NULL otherwise.
   */
  protected function isAutoHarmonizeInRules(?array $rules) : ?string {
    // If there are no rules, we can leave.
    if (empty($rules)) {
      return FALSE;
    }

    // Convenience.
    $fieldName = $this->fieldName;

    // Get the parent entity.
    $parentEntity = $this->fieldDefinition->getTargetEntityTypeId();
    $parentBundle = $this->fieldDefinition->getTargetBundle();

    // Get bundle rules if any.
    $bundleRules = $rules[$parentEntity]['bundles'][$parentBundle] ?? NULL;

    // Check if the field is set in the rules.
    if ($bundleRules && array_key_exists($fieldName, $bundleRules['rules'] ?? [])) {
      if (isset($bundleRules['rules'][$fieldName]['auto_harmonize'])) {
        return $bundleRules['rules'][$fieldName]['auto_harmonize'];
      }
    }

    // Check entity rules.
    $entityRules = $rules[$parentEntity] ?? NULL;

    // Check if the field is set in the rules.
    if ($entityRules && array_key_exists($fieldName, $entityRules['rules'] ?? [])) {
      if (isset($entityRules['rules'][$fieldName]['auto_harmonize'])) {
        return $entityRules['rules'][$fieldName]['auto_harmonize'];
      }
    }

    // At this point the field is not auto harmonized.
    return FALSE;
  }

}
