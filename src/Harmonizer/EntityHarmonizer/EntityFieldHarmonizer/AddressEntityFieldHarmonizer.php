<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer;

/**
 * Handles exceptions for 'address' type fields.
 *
 * Note that this harmonizer assumes that the 'address' contrib module is
 * installed.
 *
 * @link https://www.drupal.org/project/address
 *
 * @property \Drupal\harmonize\Service\Harmonize $harmonizeService
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer
 */
class AddressEntityFieldHarmonizer extends EntityFieldHarmonizer {

  /**
   * {@inheritdoc}
   */
  protected function process(array $value, int $i) {
    // Simply return the base value array, which in turns contains all address
    // properties. Some properties may be empty or null, depending on the field
    // values requirements settings.
    return $value;
  }

}
