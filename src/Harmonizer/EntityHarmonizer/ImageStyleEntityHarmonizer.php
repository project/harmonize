<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer;

/**
 * Handles harmonization exceptions for Node entities.
 *
 * @property \Drupal\image\ImageStyleInterface $entity
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer
 */
final class ImageStyleEntityHarmonizer extends EntityHarmonizer {

  /**
   * {@inheritdoc}
   */
  protected function getHarmonizedData() : array {
    $data['machine_name'] = $this->entity->getName();
    return $data;
  }

}
