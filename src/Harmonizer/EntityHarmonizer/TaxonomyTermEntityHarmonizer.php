<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer;

use Drupal\Core\Entity\EntityMalformedException;
use Drupal\taxonomy\Entity\Term;

/**
 * Handles harmonization exceptions for TaxonomyTerm entities.
 *
 * @property Term $entity
 *
 * @package Drupal\harmonize\Harmonizer\TaxonomyTermEntityHarmonizer
 */
final class TaxonomyTermEntityHarmonizer extends EntityHarmonizer {

  /**
   * {@inheritdoc}
   */
  protected function getHarmonizedData() {
    // Run the EntityHarmonizerBase function. No exceptions here.
    $data = parent::getHarmonizedData();

    // Add term title to the processed data.
    $data['name'] = $this->entity->getName();

    // Add term description to the processed data.
    $data['description'] = $this->entity->getDescription();

    // Add term weight to the processed data.
    $data['weight'] = $this->entity->getWeight();

    // Add URL information if term is not new.
    if (!$this->entity->isNew()) {
      try {
        $url = $this->entity->toUrl()->toString();
        $data['href'] = $url;
      }
      catch (EntityMalformedException $exception) {
        $this->harmonizeService->loggerChannel->error($exception->getMessage());
      }
    }

    // Add parent information, if any, to the processed data.
    if ($this->entity->hasField('parent') && !empty($this->entity->get('parent')->getString())) {
      $parentId = $this->entity->get('parent')->getString();
      $parent = Term::load($parentId);
      $data['parent'] = EntityHarmonizerFactory::build($parent, [], $this->harmonizeService);
    }

    return $data;
  }

}
