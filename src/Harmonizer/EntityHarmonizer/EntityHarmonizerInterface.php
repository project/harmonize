<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer;

use Drupal\Core\Entity\EntityInterface;
use Drupal\harmonize\Harmonizer\HarmonizerInterface;

/**
 * Provides an interface for Entity Harmonizers.
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer
 */
interface EntityHarmonizerInterface extends HarmonizerInterface {

  /**
   * Get Entity Type ID for this Harmonizer.
   *
   * @return string
   *   Return Entity Type ID string.
   */
  public function getEntityTypeId() : string;

  /**
   * Get Entity Bundle for this Harmonizer.
   *
   * @return string|null
   *   Return Entity Bundle string.
   */
  public function getEntityBundle() : ?string;

  /**
   * Get Entity for this Harmonizer.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Return Entity.
   */
  public function getEntity() : EntityInterface;

  /**
   * Get the harmonized value of a single field.
   *
   * @return \Drupal\harmonize\Harmonizer\HarmonizerInterface|string|array|null
   *   The data harmonized for a single field.
   */
  public function get(string $fieldName) : HarmonizerInterface|string|array|NULL;

}
