<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer;

use Drupal\Core\Entity\EntityMalformedException;

/**
 * Handles harmonization exceptions for Node entities.
 *
 * @property \Drupal\node\Entity\Node $entity
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer
 */
final class NodeEntityHarmonizer extends EntityHarmonizer {

  /**
   * {@inheritdoc}
   */
  protected function getHarmonizedData() {
    // Perform default EntityHarmonizer tasks to get any field values.
    $data = parent::getHarmonizedData();

    // If...For some reason...Wait...
    // What was this here for again?
    // @todo Investigate cases where the harmonized data can be a string and not an array...
    if (!is_array($data)) {
      $data = ['content' => $data];
    }

    // Get Entity Title if it exists.
    if ($this->entity->getTitle() !== NULL) {
      $data['title'] = $this->entity->getTitle();
    }

    // Add URL information if node is not new.
    if (!$this->entity->isNew()) {
      try {
        $url = $this->entity->toUrl('canonical', ['absolute' => FALSE])->toString();
        $data['href'] = $url;
      }
      catch (EntityMalformedException $exception) {
        $this->harmonizeService->loggerChannel->error($exception->getMessage());
      }
    }

    // Add other relevant information for nodes.
    $data['creation_date'] = $this->entity->getCreatedTime();

    // Add the author object.
    // We add it as a harmonizer.
    $data['drupal_author'] = EntityHarmonizerFactory::build($this->entity->getOwner(), $this->getBequeathedFlags(), $this->harmonizeService);

    return $data;
  }

}
