<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer;

/**
 * Handles harmonizer exceptions for File entities.
 *
 * @property \Drupal\file\Entity\File $entity
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer
 */
final class FileEntityHarmonizer extends EntityHarmonizer {

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
   */
  protected function getHarmonizedData() : array {
    // Return only the relevant information.
    return [
      'uri'      => $this->entity->getFileUri(),
      'filename' => $this->entity->getFilename(),
      'filesize' => $this->formatFilesize($this->entity->getSize()),
      'href'     => $this->harmonizeService->fileUrlGenerator->generate($this->entity->getFileUri())->toString(),
    ];
  }

  /**
   * Unique function to FileHarmonizer. Formats the filesize accordingly.
   *
   * @param int $size
   *   Filesize provided in bytes.
   *
   * @return string
   *   Formatted filesize in text readable.
   */
  public function formatFilesize(int $size) : string {
    // Format the filesize given the bytes returned.
    if ($size >= 1073741824) {
      $size /= 1073741824;
      $unit = 'GB';
    }
    elseif ($size >= 1048576) {
      $size /= 1048576;
      $unit = 'MB';
    }
    elseif ($size >= 1024) {
      $size /= 1024;
      $unit = 'KB';
    }
    else {
      $unit = 'B';
    }

    // Format the number.
    $size = $this->numberFormat($size);

    return $size . ' ' . $unit;
  }

  /**
   * Format numbers considering the current language.
   *
   * Numbers have different formats per language.
   *
   * @param mixed $number
   *   Provided number in text format.
   *
   * @return string
   *   Formatted number.
   *
   * @noinspection PhpMissingParamTypeInspection
   */
  public function numberFormat($number) : string {
    // Get the current language.
    $lang = $this->harmonizeService->languageManager->getCurrentLanguage()->getId();

    // Defaults.
    $formatDecimalCount = 2;
    $formatDecimalDelimiter = '.';
    $formatThousandsSeparator = ',';

    // Change format given the language.
    // @todo Can maybe have a config for this since EN & FR aren't the only languages in the world buddy!
    switch ($lang) {
      case 'en':
        // No changes to be done to the defaults.
        break;

      case 'fr':
        $formatDecimalCount = 3;
        $formatDecimalDelimiter = ',';
        $formatThousandsSeparator = ' ';
        break;

      default:
        // Do nothing.
        break;

    }

    return number_format($number, $formatDecimalCount, $formatDecimalDelimiter, $formatThousandsSeparator);
  }

}
