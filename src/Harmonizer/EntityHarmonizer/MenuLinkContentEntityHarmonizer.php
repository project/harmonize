<?php

namespace Drupal\harmonize\Harmonizer\EntityHarmonizer;

/**
 * Handles harmonization exceptions for MenuLinkContent entities.
 *
 * @property \Drupal\menu_link_content\Entity\MenuLinkContent $entity
 *
 * @package Drupal\harmonize\Harmonizer\EntityHarmonizer
 */
final class MenuLinkContentEntityHarmonizer extends EntityHarmonizer {

  /**
   * {@inheritdoc}
   */
  protected function getHarmonizedData() {
    // Perform default EntityHarmonizer tasks to get any field values.
    $data = parent::getHarmonizedData();

    // UUID.
    $data['uuid'] = $this->entity->uuid();

    return $data;
  }

}
