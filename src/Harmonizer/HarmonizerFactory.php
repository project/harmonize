<?php

namespace Drupal\harmonize\Harmonizer;

use Drupal\Component\Serialization\Json;
use Drupal\harmonize\Constants\HarmonizerFlags;
use Drupal\harmonize\Service\Harmonize;

/**
 * Provides a base class for all Harmonizer Sub-Factories.
 *
 * @package Drupal\harmonize\Harmonizer
 */
abstract class HarmonizerFactory {

  /**
   * Storage used to keep harmonizers that have already been generated.
   *
   * Works wonders for pages that harmonize the same object multiple times. This
   * will allow harmonizers to be stored using their signature. If they're
   * called again on the same page, they will be fetched instead of
   * re-instantiated.
   *
   * @var array
   */
  protected static array $registry = [];

  /**
   * Build the appropriate Harmonizer given the provided object.
   *
   * @param mixed $object
   *   Drupal object that is currently being processed.
   * @param array $flags
   *   Flags that affect the actions taken when harmonization the object.
   * @param \Drupal\harmonize\Service\Harmonize $harmonizeService
   *   The Harmonize service.
   *
   * @return \Drupal\harmonize\Harmonizer\HarmonizerInterface
   *   Returns the proper Harmonizer.
   *
   * @noinspection PhpMissingParamTypeInspection
   */
  public static function build($object, array $flags, Harmonize $harmonizeService) : HarmonizerInterface {
    // Generate a signature for the harmonizer about to be instantiated.
    // Example: entity.node.basic_page.
    $signature = static::resolveSignature($object, $flags);

    // Check if the harmonizer is already created/registered in this factory.
    // If it is, return it.
    // Note: We do not load registered harmonizers if a parent is set in the
    // flags. This is because data might differ when a harmonizer is processed
    // as a child versus processing data standalone.
    // We also skip the registry if the flag is set.
    $registered = static::requestHarmonizer($signature);
    if ($registered !== NULL && !isset($flags[HarmonizerFlags::PARENT])) {
      return $registered;
    }

    // Get the class we need to use for harmonization.
    $class = static::resolveClass($object);

    // Obtain harmonizer.
    $harmonizer = static::obtainHarmonizer($signature, $class, $object, $harmonizeService, static::type());

    // Set flags.
    $harmonizer->setFlags($flags);

    // If the parent parameter is not null, we'll set it to the harmonizer.
    if (array_key_exists(HarmonizerFlags::PARENT, $flags) && $flags[HarmonizerFlags::PARENT] !== NULL) {
      $harmonizer->setParentHarmonizer($flags[HarmonizerFlags::PARENT]);
      // Also set the depth.
      $harmonizer->setDepth($harmonizer->getParentHarmonizer()->getDepth() + 1);
    }

    // Reload suggestions.
    $harmonizer->reloadSuggestions();

    // Return the harmonizer that is now ready.
    return $harmonizer;
  }

  /**
   * Configuration function to set type of harmonizer for a given factory.
   *
   * @return string
   *   The unique string identifying a harmonizer's type.
   */
  abstract protected static function type() : string;

  /**
   * Using the provided object, resolve the appropriate signature to set.
   *
   * @param mixed $object
   *   Object to be harmonized.
   * @param array $flags
   *   Harmonizer flags if set.
   *
   * @return string
   *   Signature string to use.
   *
   * @noinspection PhpMissingParamTypeInspection
   */
  abstract protected static function resolveSignature($object, array $flags = []) : string;

  /**
   * Using the provided object, resolve the appropriate class to use.
   *
   * @param mixed $object
   *   Object to be harmonized.
   *
   * @return string
   *   Name of the class to use for harmonization.
   *
   * @noinspection PhpMissingParamTypeInspection
   */
  abstract protected static function resolveClass($object) : string;

  /**
   * Create and obtain a harmonizer using the provided data.
   *
   * This function will instantiate a harmonizer using the class determined to
   * be the most appropriate. It will then register it in the factory.
   *
   * @param string $signature
   *   Harmonizer signature generated in the respective factory.
   * @param string $class
   *   Optimal class for harmonization determined by the factory.
   * @param mixed $object
   *   Object being harmonized.
   * @param \Drupal\harmonize\Service\Harmonize $harmonizeService
   *   Harmonize module's main service injected.
   * @param string $type
   *   The type of harmonizer being created.
   *
   * @return \Drupal\harmonize\Harmonizer\HarmonizerInterface
   *   Return the harmonizer that was retrieved/created.
   *
   * @noinspection PhpMissingParamTypeInspection
   */
  final public static function obtainHarmonizer(string $signature, string $class, $object, Harmonize $harmonizeService, string $type) : HarmonizerInterface {
    // Instantiate the harmonizer.
    $harmonizer = new $class($object, $harmonizeService, $type, $signature);

    // Register the harmonizer.
    self::registerHarmonizer($signature, $harmonizer);

    // Return the requested harmonizer.
    return $harmonizer;
  }

  /**
   * Request a harmonizer from this factory's registry using the signature.
   *
   * @param string $signature
   *   Signature to search for in the registry.
   *
   * @return \Drupal\harmonize\Harmonizer\HarmonizerInterface|null
   *   Returns a harmonizer if it's found. Otherwise, return null.
   */
  final public static function requestHarmonizer(string $signature) : ?HarmonizerInterface {
    if (isset(self::$registry[$signature])) {
      return self::$registry[$signature];
    }

    return NULL;
  }

  /**
   * Saves an instantiated harmonizer in the factory class.
   *
   * This reduces execution load as we will always obtain a harmonizer that has
   * already been instantiated instead of making duplicates.
   *
   * @param string $signature
   *   Signature key to use as an identity to save this harmonizer under.
   * @param \Drupal\harmonize\Harmonizer\HarmonizerInterface $harmonizer
   *   Harmonizer class to save in the factory's registry.
   */
  final public static function registerHarmonizer(string $signature, HarmonizerInterface $harmonizer) : void {
    self::$registry[$signature] = $harmonizer;
  }

  /**
   * Create a Harmonizer Signature.
   *
   * Texts provided will be merged together.
   *
   * Example: If "entity" and "node" are provided, the generated signature will
   * be "entity.node".
   *
   * @param mixed $strings
   *   Texts that should be merged, separated by '.' characters.
   *
   * @return string
   *   The generated signature string.
   */
  final public static function generateHarmonizerSignature(...$strings) : string {
    // Initialize the string.
    $signature = '';

    // Append all the strings, separated by ".".
    end($strings);
    $lastKey = key($strings);
    foreach ($strings as $key => $text) {
      if (empty($text)) {
        continue;
      }
      $signature .= $text;
      if ($key !== $lastKey) {
        $signature .= '.';
      }
    }

    // Return the signature from the function call.
    return $signature;
  }

  /**
   * Generate flag signatures.
   *
   * @return string
   *   The generated signature string.
   */
  final public static function generateFlagsSignatures(array $flags) : string {
    $signature = '';

    // If the View Mode flag is set, we want to add the view mode to the
    // signature.
    if (array_key_exists(HarmonizerFlags::VIEW_MODE, $flags) && !empty($flags[HarmonizerFlags::VIEW_MODE])) {
      $signature = $signature . '.' . $flags[HarmonizerFlags::VIEW_MODE];
    }

    // If the Style flag is set, we want to add the style to the signature.
    if (array_key_exists(HarmonizerFlags::STYLE, $flags) && !empty($flags[HarmonizerFlags::STYLE])) {
      $signature = $signature . '.' . $flags[HarmonizerFlags::STYLE];
    }

    asort($flags);
    foreach ($flags as $key => $value) {
      switch ($key) {
        case HarmonizerFlags::FIELDS:
          $signature .= '.' . HarmonizerFlags::FIELDS . '.' . Json::encode($value);
          break;

        case HarmonizerFlags::EXCLUDED_FIELDS:
          $signature .= '.' . HarmonizerFlags::EXCLUDED_FIELDS . '.' . Json::encode($value);
          break;

        case HarmonizerFlags::NO_RECURSIVE:
          $signature .= '.' . HarmonizerFlags::NO_RECURSIVE;
          break;
      }
    }

    return ltrim($signature, '.');
  }

}
