<?php

namespace Drupal\harmonize\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\harmonize\Constants\HarmonizeConfig;
use Symfony\Component\Routing\Route;

/**
 * Defines a route controller for the autocomplete in Style edit forms.
 */
class ManagePreprocessAccessCheck implements AccessInterface {

  /**
   * Check access to the route.
   */
  public function access(Route $route, RouteMatch $route_match) : AccessResult {
    $currentRouteMatch = \Drupal::routeMatch();
    $rawParameters = $currentRouteMatch->getRawParameters();
    $accessResults = [];

    // @todo Optimize this in the future. We got lazy here!
    foreach (\Drupal::entityTypeManager()->getDefinitions() as $entityTypeId => $entityType) {
      if ($entityType->get('field_ui_base_route')) {
        if ($entityTypeId === 'user') {
          $config = \Drupal::config(HarmonizeConfig::MAIN)->get(HarmonizeConfig::MAIN__PREPROCESSING) ?? [];
          $allowed = $config["{$entityType->id()}__$entityTypeId"] ?? FALSE;
          if ($allowed) {
            $accessResults["user"] = TRUE;
          }
        }

        $bundleEntityType = $entityType->getBundleEntityType() ?? NULL;

        if ($bundleEntityType === NULL) {
          continue;
        }

        if ($bundle = $rawParameters->get($bundleEntityType)) {
          $config = \Drupal::config(HarmonizeConfig::MAIN)->get(HarmonizeConfig::MAIN__PREPROCESSING) ?? [];
          $allowed = $config["{$entityType->id()}__$bundle"] ?? FALSE;
          if ($allowed) {
            $accessResults[$bundleEntityType] = TRUE;
          }
        }
      }
    }

    foreach ($rawParameters->all() as $key => $value) {
      if (isset($accessResults[$key]) && $accessResults[$key] === TRUE) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::forbidden();
  }

}
