<?php

namespace Drupal\harmonize\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\harmonize\Harmonizer\HarmonizerInterface;

/**
 * Provides an Event for when Entities are harmonized.
 *
 * @package Drupal\harmonize\Event
 */
class HarmonizationEvent extends Event {
  /**
   * The harmonizer that fired the event.
   *
   * @var \Drupal\harmonize\Harmonizer\HarmonizerInterface
   */
  public HarmonizerInterface $harmonizer;

  /**
   * Constructs the object.
   *
   * @param \Drupal\harmonize\Harmonizer\HarmonizerInterface $harmonizer
   *   Harmonizer attached to the event that was triggered.
   */
  public function __construct(HarmonizerInterface $harmonizer) {
    $this->harmonizer = $harmonizer;
  }

}
