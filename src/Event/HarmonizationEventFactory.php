<?php

namespace Drupal\harmonize\Event;

use Drupal\harmonize\Constants\HarmonizationEvents;
use Drupal\harmonize\Constants\HarmonizerTypes;
use Drupal\harmonize\Harmonizer\HarmonizerInterface;

/**
 * Handles the creation of the proper harmonizer event with a given type.
 *
 * @package Drupal\harmonize\Event
 */
class HarmonizationEventFactory {

  /**
   * Builds and returns the appropriate event class.
   *
   * @param \Drupal\harmonize\Harmonizer\HarmonizerInterface $harmonizer
   *   Harmonizer that launched this event.
   *
   * @return array
   *   Return array of data with event information.
   *
   * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
   */
  public static function build(HarmonizerInterface $harmonizer) : array {
    // Initializations.
    $name = '';

    // Depending on the type, create the appropriate HarmonizerInterface.
    switch ($harmonizer->getType()) {

      case HarmonizerTypes::ENTITY:
        $class = EntityHarmonizationEvent::class;
        $name = HarmonizationEvents::ENTITY;
        break;

      case HarmonizerTypes::ENTITY_FIELD:
        $class = EntityFieldHarmonizationEvent::class;
        $name = HarmonizationEvents::ENTITY_FIELD;
        break;

      case HarmonizerTypes::MENU:
        $class = MenuHarmonizationEvent::class;
        $name = HarmonizationEvents::MENU;
        break;

      case HarmonizerTypes::FORM:
        $class = FormHarmonizationEvent::class;
        $name = HarmonizationEvents::FORM;
        break;

      default:
        $class = HarmonizationEvent::class;
        break;
    }

    // Create our event.
    $event = new $class($harmonizer);

    // Return our event data.
    return [
      'event_name'    => $name,
      'event_object'  => $event,
    ];
  }

}
