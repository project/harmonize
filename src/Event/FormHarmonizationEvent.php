<?php

namespace Drupal\harmonize\Event;

/**
 * Harmonization Event for Forms.
 *
 * @property \Drupal\harmonize\Harmonizer\FormHarmonizerInterface $harmonizer
 *
 * @package Drupal\harmonize\Event
 */
class FormHarmonizationEvent extends HarmonizationEvent {}
