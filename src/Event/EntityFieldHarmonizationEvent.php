<?php

namespace Drupal\harmonize\Event;

/**
 * Harmonization Event for Entity Fields.
 *
 * @property \Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityFieldHarmonizer\EntityFieldHarmonizer $harmonizer
 *
 * @package Drupal\harmonize\Event
 */
class EntityFieldHarmonizationEvent extends HarmonizationEvent {}
