<?php

namespace Drupal\harmonize\Constants;

/**
 * Defines events for data harmonization.
 */
final class HarmonizationEvents {

  /**
   * Name of the event fired when harmonizing any object.
   *
   * This event allows modules to perform an action whenever an entity
   * object is harmonized. The event listener method receives a
   * \Drupal\harmonize\Event\HarmonizationEvent instance.
   *
   * @Event
   *
   * @var string
   */
  public const CORE = 'harmonize.core';

  /**
   * Name of the event fired when harmonizing an entity object.
   *
   * @Event
   *
   * @var string
   */
  public const ENTITY = 'harmonize.entity';

  /**
   * Name of the event fired when harmonizing an entity object's field.
   *
   * @Event
   *
   * @var string
   */
  public const ENTITY_FIELD = 'harmonize.entity.field';

  /**
   * Name of the event fired when harmonizing a form object.
   *
   * @Event
   *
   * @var string
   */
  public const FORM = 'harmonize.form';

  /**
   * Name of the event fired when harmonizing a menu object.
   *
   * @Event
   *
   * @var string
   */
  public const MENU = 'harmonize.menu';

}
