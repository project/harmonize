<?php

namespace Drupal\harmonize\Constants;

/**
 * Defines constants for the harmonizer flags system.
 */
final class HarmonizerFlags {

  /**
   * Global string defining the name of a specific harmonizer flag.
   *
   * Enabling TRACE will track the preprocessing steps all the way to the
   * final result of the data.
   *
   * This allows for more serious debugging when using the Visualization
   * feature.
   */
  public const TRACE = 'trace';

  /**
   * Global string defining the name of a specific harmonizer flag.
   *
   * THEME allows you to set which theme to use when harmonizing data.
   *
   * The active theme will be taken by default. There are cases however where
   * we'll want to force the theme choice.
   *
   * When the theme is changed, .harmony.yml file discovery is done on the
   * appropriate theme.
   */
  public const THEME = 'theme';

  /**
   * Global string defining the name of a specific harmonizer flag.
   *
   * FIELDS allows you to specify which fields to specifically harmonize during
   * Entity Harmonizations.
   */
  public const FIELDS = 'fields';

  /**
   * Global string defining the name of a specific harmonizer flag.
   *
   * FIELDS allows you to specify which fields to specifically exclude during
   * Entity Harmonizations.
   */
  public const EXCLUDED_FIELDS = 'excluded_fields';

  /**
   * Global string defining the name of a specific harmonizer flag.
   *
   * LANG can be used to request returning data in a specified language.
   */
  public const LANG = 'lang';

  /**
   * Global string defining the name of a specific harmonizer flag.
   *
   * STYLE can be used to request returning data in a specified style.
   */
  public const STYLE = 'style';

  /**
   * Global string defining the name of a specific harmonizer flag.
   *
   * VIEW_MODE can be used to request returning data of an entity with a
   * specified view mode.
   *
   * View modes can be defined to only use specific fields. In this case, we
   * only want to preprocess the fields that
   * are enabled.
   */
  public const VIEW_MODE = 'view_mode';

  /**
   * Global string defining the name of a specific harmonizer flag.
   *
   * PARENT can be used to set a parent to a harmonizer.
   */
  public const PARENT = 'parent';

  /**
   * Global string defining the name of a specific harmonizer flag.
   *
   * NO_RECURSIVE can be used to prohibit recursive harmonization.
   * If this flag is used, instead of harmonizing any nested entities, the data
   * will instead include the harmonizer itself.
   */
  public const NO_RECURSIVE = 'no_recursive';

  /**
   * Global string defining the name of a specific harmonizer flag.
   *
   * SKIP_CACHE can be used to...Well, can you guess?
   */
  public const SKIP_CACHE = 'skip_cache';

  /**
   * Global string defining the name of a specific harmonizer flag.
   *
   * SKIP_REGISTRY can be used to skip the registry.
   * The registry is used so that Harmonizers never process more than
   * once for a single page load.
   *
   * The only use case for this currently is the 'Visualize' form.
   * Otherwise, not really useful.
   */
  public const SKIP_REGISTRY = 'skip_registry';

}
