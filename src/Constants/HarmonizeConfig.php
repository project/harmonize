<?php

namespace Drupal\harmonize\Constants;

/**
 * Defines constants for the harmonization system.
 */
final class HarmonizeConfig {

  /**
   * Global string defining the name for the main configuration of the module.
   *
   * @var string
   */
  public const MAIN = 'harmonize.settings';

  /**
   * The variable name for the data in templates.
   *
   * @var string
   */
  public const MAIN__VARIABLE_NAME = 'variable_name';

  /**
   * The default variable name for the data in templates.
   *
   * @var string
   */
  public const MAIN__VARIABLE_NAME__DEFAULT = 'harmony';

  /**
   * Remove field underscore prefix config string.
   *
   * @var string
   */
  public const MAIN__REMOVE_FIELD_UNDERSCORE_PREFIX = 'remove_field_underscore_prefix';

  /**
   * Loading Algorithm config string.
   *
   * @var string
   */
  public const MAIN__LOADING_ALGORITHM = 'loading_algorithm';

  /**
   * Allowed admin routes config string.
   *
   * @var string
   */
  public const MAIN__ALLOWED_ADMIN_ROUTES = 'allowed_admin_routes';

  /**
   * Auto Harmonized entities config string.
   *
   * @var string
   */
  public const MAIN__AUTO_HARMONIZED_ENTITIES = 'auto_harmonized_entities';


  /**
   * Max depth config string.
   *
   * @var string
   */
  public const MAIN__MAX_DEPTH = 'max_depth';

  /**
   * Preprocessing config string.
   *
   * Controls configuration realm that houses which
   * entities have preprocessing enabled.
   *
   * @var string
   */
  public const MAIN__PREPROCESSING = 'preprocessing';

  /**
   * Global string defining the name for the exclusions configurations.
   *
   * @var string
   */
  public const ENTITY_RULES = 'harmonize.entity_processing_rules';

  /**
   * Global string defining the name for the exclusions configurations.
   *
   * @var string
   */
  public const ENTITY_RULES__RULES = 'rules';

  /**
   * Global string defining the name for the cache configurations.
   *
   * @var string
   */
  public const CACHE = 'harmonize.cache';

  /**
   * Cache enabled config string.
   *
   * @var string
   */
  public const CACHE__ENABLED = 'cache_enabled';

  /**
   * Cache exclusions config string.
   *
   * @var string
   */
  public const CACHE__EXCLUSIONS = 'cache_exclusions';

}
