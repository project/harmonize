<?php

namespace Drupal\harmonize\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\harmonize\Constants\HarmonizeConfig;

/**
 * Provides a configuration form for the Harmonizer module.
 *
 * @package Drupal\harmonize\Form
 *
 * @noinspection PhpUnused
 */
class HarmonizeSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'harmonize_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() : array {
    return [
      HarmonizeConfig::MAIN,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) : array {
    // Set this for better organization of form values later.
    $form['#tree'] = TRUE;

    // Get the current configurations.
    $variableName = $this->config(HarmonizeConfig::MAIN)->get(HarmonizeConfig::MAIN__VARIABLE_NAME);
    $removeFieldUnderscorePrefix = $this->config(HarmonizeConfig::MAIN)->get(HarmonizeConfig::MAIN__REMOVE_FIELD_UNDERSCORE_PREFIX);
    $extensionLoadingAlgorithm = $this->config(HarmonizeConfig::MAIN)->get(HarmonizeConfig::MAIN__LOADING_ALGORITHM);
    $allowedAdminRoutes = $this->config(HarmonizeConfig::MAIN)->get(HarmonizeConfig::MAIN__ALLOWED_ADMIN_ROUTES);
    $allowedAdminRoutes = empty($allowedAdminRoutes) ? [] : implode("\n", $allowedAdminRoutes);
    $automaticallyProcessedEntities = $this->config(HarmonizeConfig::MAIN)->get(HarmonizeConfig::MAIN__AUTO_HARMONIZED_ENTITIES);
    $automaticallyProcessedEntities = empty($automaticallyProcessedEntities) ? [] : implode("\n", $automaticallyProcessedEntities);
    $maxDepth = $this->config(HarmonizeConfig::MAIN)->get(HarmonizeConfig::MAIN__MAX_DEPTH);

    if ($extensionLoadingAlgorithm === 'scan') {
      $this->messenger()->addWarning(
        $this->t('The extension loading algorithm is currently set to <strong>SCAN</strong>.<br>
                        Be advised that this can add more strain to your site performance if code is not properly organised, maintained and/or understood.<br>
                        Refer to the <a href="https://git.drupalcode.org/project/harmonize#alteration-object-loading-algorithm">documentation</a> for more information on this.
                        '));
    }

    // Add a field for the variable to store harmonize data in templates.
    $form[HarmonizeConfig::MAIN__VARIABLE_NAME] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Variable name'),
      '#description'   => $this->t('Customize the variable name for preprocessed data sent to templates.'),
      '#default_value' => !empty($variableName) ? $variableName : HarmonizeConfig::MAIN__VARIABLE_NAME__DEFAULT,
    ];

    // Enable the automatic removal "field_" prefix when harmonization fields.
    $form[HarmonizeConfig::MAIN__REMOVE_FIELD_UNDERSCORE_PREFIX] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Omit <strong>"field_"</strong> prefix in processed data'),
      '#description'   => $this->t('Check this box to remove the prefix from field names in data processing results.'),
      '#default_value' => !empty($removeFieldUnderscorePrefix) ? $removeFieldUnderscorePrefix : FALSE,
    ];

    // Adjust the loading algorithm.
    $form[HarmonizeConfig::MAIN__LOADING_ALGORITHM] = [
      '#type'          => 'select',
      '#title'         => $this->t('Extension Loading Algorithm'),
      '#description'   => $this->t(
        'This setting touches upon the behaviour when loading objects that alter the preprocessed data. Therefore, <strong>Refiners</strong> & <strong>Harmony Files</strong><br>
              When creating these alteration objects, you have an option to target the data you want to alter via suggestions. <em>e.g. <strong>entity.node.basic_page</strong></em><br>
              In the case where multiple alteration objects apply to a single set of data, this algorithm will determine what happens.<br>
              <ul>
                <li><strong>FCFS (First Come First Serve)</strong> : Will only take the most relevant alteration object. <em>e.g. Will take <strong>entity.node.basic_page</strong> over <strong>entity.node</strong> if both exist. Only one alteration object is processed.</em></li>
                <li><strong>SCAN (Elevator)</strong> : Will take all relevant alteration objects. <em>e.g. Will take both <strong>entity.node.basic_page</strong> and <strong>entity.node</strong> if both exist. The smaller signature is processed first, and so forth, so <strong>entity.node</strong> -> <strong>entity.node.basic_page</strong></em></li>
              </ul>
              The recommended setting is <strong>FCFS</strong>.<br>
              <strong>SCAN</strong> is mainly available for projects upgrading from lower versions of the module.'
      ),
      '#options'       => [
        'fcfs' => $this->t('FCFS'),
        'scan' => $this->t('SCAN'),
      ],
      '#default_value' => !empty($extensionLoadingAlgorithm) ? $extensionLoadingAlgorithm : 'fcfs',
    ];

    // Configure allowed admin routes.
    $form[HarmonizeConfig::MAIN__AUTO_HARMONIZED_ENTITIES] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Automatically Processed Referenced Entities'),
      '#description'   => $this->t(
        'On each line, enter entity types that should automatically be processed when found as nested entities.<br>
              This pertains to <strong>entity_reference</strong> and <strong>entity_reference_revisions</strong> type fields.<br>
              <em>e.g. <strong>media</strong></em><br>
              <em>e.g. <strong>taxonomy_term</strong></em><br>
              <em>e.g. <strong>paragraph.carousel_slide</strong></em><br>'
      ),
      '#default_value' => !empty($automaticallyProcessedEntities) ? $automaticallyProcessedEntities : '',
    ];

    // Configure max depth.
    $form[HarmonizeConfig::MAIN__MAX_DEPTH] = [
      '#type'          => 'number',
      '#title'         => $this->t('Max Depth'),
      '#description'   => $this->t(
        'Configure the max depth when processing nested entities in entity reference fields.<br>
              This pertains to <strong>entity_reference</strong> and <strong>entity_reference_revisions</strong> type fields.<br>'
      ),
      '#max'           => 12,
      '#default_value' => !empty($maxDepth) ? $maxDepth : 6,
    ];

    // Configure allowed admin routes.
    $form[HarmonizeConfig::MAIN__ALLOWED_ADMIN_ROUTES] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Allowed Admin Routes'),
      '#description'   => $this->t(
        'On each line, enter routes where harmonization should execute when rendering entities.<br>
              Harmonize does not run on admin routes by default since it is intended for the frontend layer.<br>
              That being said, there are cases where you want it to run on the admin side.<br>
              The best example is in the case of the layout builder routes. Those routes are on the admin side, but preview content as it would look on the front-end side, therefore needing preprocessing and theming.<br><br>
              <em>e.g. <strong>layout_builder.add_block</strong></em><br>
              <em>e.g. <strong>layout_builder.move_block</strong></em><br>
              <em>e.g. <strong>layout_builder.remove_block</strong></em><br>'
      ),
      '#default_value' => !empty($allowedAdminRoutes) ? $allowedAdminRoutes : '',
    ];

    // Return the form with all necessary fields.
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) : void {
    // Retrieve the configuration.
    $values = $form_state->getValues();

    // Format values.
    $adminRoutes = empty($values[HarmonizeConfig::MAIN__ALLOWED_ADMIN_ROUTES]) ? [] : array_map('trim', explode("\n", $values[HarmonizeConfig::MAIN__ALLOWED_ADMIN_ROUTES]));
    $automaticallyProcessedEntities = empty($values[HarmonizeConfig::MAIN__AUTO_HARMONIZED_ENTITIES]) ? [] : array_map('trim', explode("\n", $values[HarmonizeConfig::MAIN__AUTO_HARMONIZED_ENTITIES]));

    $this->configFactory->getEditable(HarmonizeConfig::MAIN)
      ->set(HarmonizeConfig::MAIN__VARIABLE_NAME, $values[HarmonizeConfig::MAIN__VARIABLE_NAME])
      ->set(HarmonizeConfig::MAIN__REMOVE_FIELD_UNDERSCORE_PREFIX, (bool) $values[HarmonizeConfig::MAIN__REMOVE_FIELD_UNDERSCORE_PREFIX])
      ->set(HarmonizeConfig::MAIN__LOADING_ALGORITHM, $values[HarmonizeConfig::MAIN__LOADING_ALGORITHM])
      ->set(HarmonizeConfig::MAIN__ALLOWED_ADMIN_ROUTES, $adminRoutes)
      ->set(HarmonizeConfig::MAIN__AUTO_HARMONIZED_ENTITIES, $automaticallyProcessedEntities)
      ->set(HarmonizeConfig::MAIN__MAX_DEPTH, $values[HarmonizeConfig::MAIN__MAX_DEPTH])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
