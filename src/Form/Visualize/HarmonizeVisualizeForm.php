<?php

namespace Drupal\harmonize\Form\Visualize;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\harmonize\Constants\HarmonizeConfig;
use Drupal\harmonize\Constants\HarmonizerFlags;
use Drupal\harmonize\Service\Harmonize;
use Drupal\harmonize\Service\Helpers;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a configuration form for the Harmonizer module.
 *
 * @package Drupal\harmonize\Form
 *
 * @noinspection PhpUnused
 */
class HarmonizeVisualizeForm extends ConfigFormBase {

  /**
   * The Harmonize service injected through DI.
   *
   * @var \Drupal\harmonize\Service\Harmonize
   */
  protected Harmonize $harmonize;

  /**
   * The Helpers service injected through DI.
   *
   * @var \Drupal\harmonize\Service\Helpers
   */
  protected Helpers $helpers;

  /**
   * The State service injected through DI.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   ConfigFactory service.
   * @param \Drupal\harmonize\Service\Harmonize $harmonize
   *   Harmonize service.
   * @param \Drupal\harmonize\Service\Helpers $helpers
   *   Harmonize Helpers service.
   * @param \Drupal\Core\State\StateInterface $state
   *   State service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, Harmonize $harmonize, Helpers $helpers, StateInterface $state) {
    parent::__construct($configFactory);
    $this->harmonize = $harmonize;
    $this->helpers = $helpers;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container) : HarmonizeVisualizeForm {
    return new static(
      $container->get('config.factory'),
      $container->get('harmonize'),
      $container->get('harmonize.helpers'),
      $container->get('state'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'harmonize_admin_visualize_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() : array {
    return [
      HarmonizeConfig::MAIN,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) : array {
    // Set this for better organization of form values later.
    $form['#tree'] = TRUE;

    // Some setup.
    $form['#prefix'] = '<div id="harmonize_visualize_form">';
    $form['#suffix'] = '</div>';
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $form['#attached']['library'][] = 'harmonize/visualize_admin_form';

    // The status messages that will contain any form errors.
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    // Add controls section.
    $form['controls'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Controls'),
      '#description' => $this->t('Make your selections above.'),
      '#tree' => TRUE,
      '#wrapper_attributes' => [
        'class' => ['visualize-controls-wrapper'],
      ],
    ];

    // Build history section.
    $this->addHistorySection($form, $form_state);

    // Choose the object type we want to visualize.
    $form['controls']['theme'] = [
      '#title' => $this->t('Theme'),
      '#type' => 'select',
      '#options' => array_map(function ($ext) {
        return $ext->getName();
      }, $this->helpers->themeExtensionList->getList()),
      '#default_value' => $this->helpers->getDefaultTheme() ?? $this->helpers->themeManager->getActiveTheme()->getName(),
      '#wrapper_attributes' => [
        'class' => ['visualize-controls-field'],
      ],
    ];

    // Choose the object type we want to visualize.
    $form['controls']['lang'] = [
      '#title' => $this->t('Language'),
      '#type' => 'select',
      '#options' => array_map(function ($lang) {
        return $lang->getName();
      }, $this->helpers->languageManager->getLanguages()),
      '#default_value' => $this->helpers->languageManager->getCurrentLanguage()->getName(),
      '#wrapper_attributes' => [
        'class' => ['visualize-controls-field'],
      ],
    ];

    // Load styles from config & default theme.
    $styles = $this->helpers->getStyles();
    $defaultThemeStyles = $this->helpers->getHarmonyThemeStylesConfig($this->helpers->getDefaultTheme())['styles'] ?? [];

    // Merge our styles.
    // As mentioned elsewhere in the module, theme styles take precedence if we
    // have duplicates.
    $styles = array_merge($styles, $defaultThemeStyles);

    // Build the selector if we have styles.
    if (!empty($styles)) {
      $styleOptions = [
        '' => '-- Select --',
      ];
      foreach ($styles as $machineName => $values) {
        $styleOptions[$machineName] = $machineName;
      }
      // Entity type selector.
      $form['controls']['style'] = [
        '#title' => $this->t('Style'),
        '#type' => 'select',
        '#options' => $styleOptions,
        '#wrapper_attributes' => [
          'class' => ['visualize-controls-field'],
        ],
      ];
    }

    // Choose the object type we want to visualize.
    $form['controls']['object_type'] = [
      '#title' => $this->t('Object Type'),
      '#type' => 'select',
      '#options' => [
        'entity' => $this->t('Entity'),
        'region' => $this->t('Region'),
        'menu'   => $this->t('Menu'),
        'form'   => $this->t('Form'),
      ],
      '#wrapper_attributes' => [
        'class' => ['visualize-controls-field'],
      ],
      '#attributes' => [
        'class' => [
          'use-ajax',
        ],
      ],
    ];

    // Add elements for the 'entity' option.
    $this->addEntitySelectionFormElements($form, $form_state);

    $form['visualization'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Result'),
      '#description' => $this->t('The results will show up here!'),
    ];

    if ($form_state->get('processingTime')) {
      $time = $form_state->get('processingTime');
      $form['visualization']['time'] = ['#children' => $time];
    }

    if ($form_state->get('visualization')) {
      $data = $form_state->get('visualization');
      $form['visualization']['data'] = ['#children' => $data];
    }

    if ($form_state->get('trace')) {
      $trace = $form_state->get('trace');
      $form['trace'] = [
        '#type' => 'details',
        '#title' => $this->t('Trace'),
        '#description' => $this->t('A backtrace of the process.'),
        '#open' => $form_state->get("trace_open") ?? FALSE,
      ];

      foreach ($trace as $delta => $info) {
        $form['trace'][$delta] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['container-inline'],
          ],
        ];
        $form['trace'][$delta]['info'] = [
          '#type' => 'item',
          '#description' => $this->t("@number. @step", [
            '@number' => $delta + 1,
            '@step' => new FormattableMarkup($info['trace'], []),
          ]),
          '#suffix' => "<br>",
        ];
        if (!empty($info['data'])) {
          $form['trace'][$delta]['data'] = [
            '#type' => 'details',
            '#title' => $this->t('View Data'),
          ];
          $form['trace'][$delta]['data']['dump'] = [
            '#type' => 'item',
            '#description' => new FormattableMarkup($this->helpers->getDumpOutput($info['data']), []),
          ];
        }
      }
    }

    // Return the form with all necessary fields.
    return $form;
  }

  /**
   * Add History section.
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function addHistorySection(array &$form, FormStateInterface $form_state) {
    // Add history section.
    if ($this->state->get('harmonize__visualizer_history')) {
      // Fieldset.
      $form['history'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('History'),
      ];

      // Build our history table.
      $form['history']['table'] = [
        '#type' => 'container',
        '#title' => $this->t('History'),
        '#theme' => 'harmonize_admin_visualize_history_table',
        '#tree' => TRUE,
        '#header' => [
          $this->t('Theme'),
          $this->t('Language'),
          $this->t('View Mode'),
          $this->t('Style'),
          $this->t('Object Type'),
          $this->t('ID'),
          $this->t('Operations'),
        ],
        '#rows' => [],
        '#wrapper_attributes' => [
          'class' => ['visualize-history-wrapper'],
        ],
      ];
      foreach ($this->state->get('harmonize__visualizer_history') as $key => $info) {
        // Build row.
        $form['history']['table']['rows'][$key]['theme']['#markup'] = new FormattableMarkup("<code>@theme</code>", ['@theme' => $info['theme']]);
        $form['history']['table']['rows'][$key]['lang']['#markup'] = new FormattableMarkup("<code>@lang</code>", ['@lang' => $info['lang'] ?? '']);
        $form['history']['table']['rows'][$key]['view_mode']['#markup'] = new FormattableMarkup("<code>@viewMode</code>", ['@viewMode' => $info['view_mode']]);
        $form['history']['table']['rows'][$key]['style']['#markup'] = new FormattableMarkup("<code>@style</code>", ['@style' => $info['style'] ?? '']);
        $form['history']['table']['rows'][$key]['objectType']['#markup'] = new FormattableMarkup("<code>@type</code>", ['@type' => $info['objectType']]);
        $form['history']['table']['rows'][$key]['id']['#markup'] = new FormattableMarkup("<code>@id</code>", ['@id' => $info['id']]);

        // Resolve callback & markup.
        switch ($info['objectType']) {
          case 'entity':
            $callback = '::visualizeEntity';
            $form['history']['table']['rows'][$key]['id']['#markup'] = new FormattableMarkup("<code>@entityType:@id</code>", [
              '@entityType' => $info['entityTypeId'],
              '@id' => $info['id'],
            ]);
            break;

          case 'region':
            $callback = '::visualizeRegion';
            break;

          case 'menu':
            $callback = '::visualizeMenu';
            break;

          case 'form':
            $callback = '::visualizeForm';
            break;

          default:
            $callback = '';
            break;
        }

        // Show the visualize button.
        $form['history']['table']['rows'][$key]['operations']['data'] = [
          '#type' => 'submit',
          '#name' => "visualize_history_$key",
          '#value' => $this->t('Visualize'),
          '#submit' => [$callback],
          '#info' => $info,
          '#attributes' => [
            'class' => [
              'use-ajax',
            ],
          ],
          '#ajax' => [
            'callback' => '::ajax',
            'wrapper' => 'harmonize_visualize_form',
            'disable-refocus' => TRUE,
          ],
        ];
      }

      // Clear button.
      $form['history']['clear'] = [
        '#type' => 'submit',
        '#name' => 'clear_history',
        '#value' => $this->t('Clear History'),
        '#submit' => ['::clearHistory'],
        '#attributes' => [
          'class' => [
            'use-ajax',
          ],
        ],
        '#ajax' => [
          'callback' => '::ajax',
          'wrapper' => 'harmonize_visualize_form',
          'disable-refocus' => TRUE,
        ],
      ];
    }
  }

  /**
   * Function to add form elements when the 'entity' object type is selection.
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function addEntitySelectionFormElements(array &$form, FormStateInterface $form_state) {
    // Now we want to get ALL the entity types installed on the site and list
    // them.
    $entityTypeDefinitions = $this->helpers->entityTypeManager->getDefinitions();

    // Get only content entity types.
    $contentEntityTypeDefinitions = array_filter($entityTypeDefinitions, function ($definition) {
      return $definition instanceof ContentEntityType;
    });

    // Create and populate our options array.
    $contentEntityTypeOptions = [];
    foreach ($contentEntityTypeDefinitions as $definition) {
      $id = $definition->id();
      $label = $definition->getLabel()->getUntranslatedString();
      $contentEntityTypeOptions[$id] = $label;
    }
    asort($contentEntityTypeOptions);

    // Entity type selector.
    $form['controls']['entity']['entity_type'] = [
      '#title' => $this->t('Entity Type'),
      '#type' => 'select',
      '#options' => $contentEntityTypeOptions,
      '#default_value' => 'node',
      '#wrapper_attributes' => [
        'class' => ['visualize-controls-field'],
      ],
      '#attributes' => [
        'class' => [
          'use-ajax',
        ],
      ],
      '#states' => [
        'visible' => [
          'select[name="controls[object_type]"]' => ['value' => 'entity'],
        ],
      ],
    ];

    // ID text field.
    $form['controls']['entity']['id'] = [
      '#title' => $this->t('ID'),
      '#type' => 'textfield',
      '#validated' => TRUE,
      '#wrapper_attributes' => [
        'class' => ['visualize-controls-field'],
      ],
      '#states' => [
        'visible' => [
          'select[name="controls[object_type]"]' => ['value' => 'entity'],
        ],
      ],
    ];

    // View Mode selector.
    // @todo Convert to selector that updates based on entity type choice.
    $form['controls']['entity']['view_mode'] = [
      '#title' => $this->t('View Mode'),
      '#type' => 'textfield',
      '#validated' => TRUE,
      '#wrapper_attributes' => [
        'class' => ['visualize-controls-field'],
      ],
      '#states' => [
        'visible' => [
          'select[name="controls[object_type]"]' => ['value' => 'entity'],
        ],
      ],
    ];

    // Show the visualize button.
    $form['controls']['entity']['visualize'] = [
      '#type' => 'submit',
      '#name' => 'visualize',
      '#value' => $this->t('Visualize'),
      '#submit' => ['::visualizeEntity'],
      '#attributes' => [
        'class' => [
          'use-ajax',
        ],
      ],
      '#ajax' => [
        'callback' => '::ajax',
        'wrapper' => 'harmonize_visualize_form',
        'disable-refocus' => TRUE,
      ],
      '#states' => [
        'visible' => [
          'select[name="controls[object_type]"]' => ['value' => 'entity'],
        ],
      ],
    ];
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function ajax(array $form, FormStateInterface $form_state) : array {
    return $form;
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   *
   * @noinspection PhpUnused
   */
  public function clearHistory(array $form, FormStateInterface $form_state) : array {
    // Set our form rebuild early.
    $form_state->setRebuild();
    $this->state->set('harmonize__visualizer_history', []);
    return $form;
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   *
   * @noinspection PhpUnused
   */
  public function visualizeEntity(array $form, FormStateInterface $form_state) : array {
    // Variable to store whether we want to log this visualization in the
    // history.
    $skipHistoryLog = FALSE;

    // Set our form rebuild early.
    $form_state->setRebuild();

    // Get the button that triggered this call.
    $button = $form_state->getTriggeringElement();
    if (!$button) {
      return $form;
    }

    // If info is set on the button, it means we clicked a history button.
    if (isset($button['#info'])) {
      $theme = $button['#info']['theme'];
      $lang = $button['#info']['lang'];
      $style = $button['#info']['style'];
      $entityTypeId = $button['#info']['entityTypeId'];
      $id = $button['#info']['id'];
      $viewMode = $button['#info']['view_mode'];
      $skipHistoryLog = TRUE;
    }
    else {
      // Get our input.
      $input = $form_state->getValue('controls');
      $entityTypeId = $input['entity']['entity_type'] ?? NULL;

      // If no entity type is set, we show an error.
      if ($entityTypeId === NULL) {
        $this->messenger()->addError($this->t('An entity type ID must be entered.'));
        return $form;
      }

      // Get the entity ID.
      $id = $input['entity']['id'] ?? NULL;

      // If the ID isn't set, we also show an error.
      if ($id === NULL) {
        $this->messenger()->addError($this->t('An ID must be entered.'));
        return $form;
      }

      // Get other stuff.
      $theme = $input['theme'] ?? $this->helpers->themeManager->getActiveTheme()->getName();
      $lang = $input['lang'] ?? $this->helpers->languageManager->getCurrentLanguage();
      $viewMode = $input['entity']['view_mode'];
      $style = $input['style'];
    }

    // Okay, now we can attempt to load the entity.
    try {
      $storage = $this->helpers->entityTypeManager->getStorage($entityTypeId);
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException) {
      $this->messenger()->addError($this->t('An unexpected error occurred. Sorry! Please try again later.'));
      return $form;
    }
    $entity = $storage->load($id);

    // If the entity is not found, show a no results message.
    if ($entity === NULL) {
      $form_state->set('visualization', $this->t('No results found for <strong><code>@id</code></strong>.', ['@id' => $id]));
      $form_state->set('processingTime', NULL);
      return $form;
    }

    // Now we harmonize.
    $timeStart = microtime(TRUE);
    $trace = NULL;
    $data = $this->harmonize->harmonize($entity, [
      HarmonizerFlags::THEME          => $theme,
      HarmonizerFlags::LANG           => $lang,
      HarmonizerFlags::VIEW_MODE      => $viewMode,
      HarmonizerFlags::STYLE          => $style,
      HarmonizerFlags::TRACE          => TRUE,
      HarmonizerFlags::SKIP_CACHE     => TRUE,
    ], $trace);
    $timeEnd = microtime(TRUE);
    $processingTime = ($timeStart - $timeEnd) / 60;

    // Get dump output.
    $output = $this->helpers->getDumpOutput($data);

    // Set our visualization and processing time.
    $form_state->set('visualization', $output);
    $form_state->set('processingTime', '<code>Processed in ' . $processingTime . ' seconds.</code>');
    $form_state->set('trace', $trace);

    // Set the visualization in the states.
    if (!$skipHistoryLog) {
      $history = $this->state->get('harmonize__visualizer_history') ?? [];
      array_unshift($history, [
        'theme' => $theme,
        'lang' => $lang,
        'view_mode' => $viewMode,
        'style' => $style,
        'objectType' => 'entity',
        'entityTypeId' => $entityTypeId,
        'id' => $id,
      ]);
      $this->state->set('harmonize__visualizer_history', array_slice(array_unique($history, SORT_REGULAR), 0, 5));
    }

    return $form;
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   *
   * @noinspection PhpUnused
   */
  public function updateTraceStepData(array $form, FormStateInterface $form_state) : array {
    // Get the button that triggered this call.
    $button = $form_state->getTriggeringElement();
    if (!$button) {
      return $form;
    }

    // Get the delta of the trace step that was requested to be expanded.
    $delta = $button['#delta'];

    // Set open.
    $form_state->set("trace_open", TRUE);
    $form_state->set("trace_step_open_$delta", !$form_state->get("trace_step_open_$delta") ?? TRUE);

    // Set our form rebuild early.
    $form_state->setRebuild();

    return $form;
  }

}
