<?php

namespace Drupal\harmonize\Form\EntityRules;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\harmonize\Constants\HarmonizeConfig;
use Drupal\harmonize\Service\Helpers;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a class to handle the rule add form.
 *
 * @package Drupal\harmonize\Form
 *
 * @noinspection PhpUnused
 */
class HarmonizeAddRulesForm extends ConfigFormBase {

  /**
   * The Helpers service injected through DI.
   *
   * @var \Drupal\harmonize\Service\Helpers
   */
  protected Helpers $helpers;

  /**
   * Property to store the step in this multistep form.
   *
   * @var int
   */
  protected int $step = 1;

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   ConfigFactory service.
   * @param \Drupal\harmonize\Service\Helpers $helpers
   *   Harmonize Helpers service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, Helpers $helpers) {
    parent::__construct($configFactory);
    $this->helpers = $helpers;
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container) : HarmonizeAddRulesForm {
    return new static(
      $container->get('config.factory'),
      $container->get('harmonize.helpers'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'harmonize_admin_add_rules_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() : array {
    return [
      HarmonizeConfig::ENTITY_RULES,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) : array {
    // Some setup.
    $form['#prefix'] = '<div id="harmonize_add_rules_modal_form">';
    $form['#suffix'] = '</div>';
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    // The status messages that will contain any form errors.
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    // Actions.
    $form['actions'] = ['#type' => 'actions'];

    // Manage step 1.
    if ($this->step == 1) {
      // Now we want to get ALL the entity types installed on the site and list
      // them.
      $entityTypeDefinitions = $this->helpers->entityTypeManager->getDefinitions();

      // Get only content entity types.
      $contentEntityTypeDefinitions = array_filter($entityTypeDefinitions, function ($definition) {
        return $definition instanceof ContentEntityType;
      });

      // Create and populate our options array.
      $contentEntityTypeOptions = [];
      foreach ($contentEntityTypeDefinitions as $definition) {
        $id = $definition->id();
        $label = $definition->getLabel()->getUntranslatedString();
        $contentEntityTypeOptions[$id] = $label;
      }
      asort($contentEntityTypeOptions);

      $form['entity_type'] = [
        '#title' => $this->t('Entity Type'),
        '#type' => 'select',
        // '#required' => TRUE,
        '#description' => $this->t('Select the entity type this rule will be created for.'),
        '#options' => $contentEntityTypeOptions,
        '#attributes' => [
          'class' => [
            'use-ajax',
          ],
        ],
        '#ajax' => [
          'callback' => '::addBundlesSelection',
          'wrapper' => 'js-ajax-bundle-wrapper',
          'disable-refocus' => TRUE,
        ],
      ];

      $form['bundle'] = [
        '#title' => $this->t('Bundle'),
        '#type' => 'select',
        '#description' => $this->t('Select the entity type this rule will be created for.'),
        '#prefix' => '<div id="js-ajax-bundle-wrapper">',
        '#suffix' => '</div>',
        '#options' => [
          '' => $this->t('--'),
        ],
        "#validated" => TRUE,
      ];

      $form['actions']['continue'] = [
        '#type' => 'submit',
        '#name' => 'continue',
        '#value' => $this->t('Continue'),
        '#attributes' => [
          'class' => [
            'use-ajax',
          ],
        ],
        '#ajax' => [
          'callback' => '::ajax',
          'wrapper' => 'harmonize_add_rules_modal_form',
          'disable-refocus' => TRUE,
        ],
      ];
    }

    // Manage step 2.
    if ($this->step == 2) {
      // Get the selected entity type and bundle.
      $entityType = $form_state->getValue('entity_type');
      $bundle = $form_state->getValue('bundle');

      // Get all fields for this entity type.
      // @todo Can't DI cause AJAX messes with it. Find solution to that.
      if (empty($bundle)) {
        $fields = $this->helpers->getEntityTypeFieldDefinitions($entityType);
      }
      else {
        $fields = $this->helpers->getEntityTypeBundleFieldDefinitions($entityType, $bundle);
      }

      // We build a list of fields.
      /** @var \Drupal\Core\Field\FieldConfigInterface $field */
      foreach ($fields as $field) {
        $form['fields'][$field->id()] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['container-inline'],
          ],
        ];
        $form['fields'][$field->id()]['label']['#markup'] = $field->label() . " <small>(<code>" . $field->id() . "</code>)</small> ";
        $form['fields'][$field->id()]['add'] = [
          '#type' => 'submit',
          '#name' => 'add_field_' . $field->id(),
          '#value' => $this->t('Add'),
          '#submit' => ['::addField'],
          '#field' => $field,
          '#entityType' => $entityType,
          '#bundle' => $bundle,
          '#attributes' => [
            'class' => [
              'use-ajax',
              'button',
              'button-action',
              'button--primary',
              'button--small',
            ],
          ],
          '#ajax' => [
            'callback' => '::ajax',
            'wrapper' => 'harmonize_add_rules_modal_form',
            'disable-refocus' => TRUE,
          ],
        ];
      }

      $form['actions']['done'] = [
        '#type' => 'link',
        '#title' => $this->t('Done'),
        '#url' => Url::fromRoute('harmonize.entity_processing_rules'),
        '#attributes' => [
          'class' => ['button'],
        ],
      ];
    }

    return $form;
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function ajax(array $form, FormStateInterface $form_state) : array {
    return $form;
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   *
   * @noinspection PhpUnused
   */
  public function addBundlesSelection(array &$form, FormStateInterface $form_state) : AjaxResponse {
    // Get the selected entity type.
    $entityType = $form_state->getValue('entity_type');

    $bundleInfos = $this->helpers->entityTypeBundleInfo->getBundleInfo($entityType);
    if (!empty($bundleInfos)) {
      $bundleOptions = [
        '' => $this->t('--'),
      ];
      foreach ($bundleInfos as $id => $bundleInfo) {
        $bundleOptions[$id] = $bundleInfo['label'];
      }
      asort($bundleOptions);
      $form['bundle']['#options'] = $bundleOptions;
    }
    else {
      $form['bundle']['#value'] = NULL;
    }

    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#js-ajax-bundle-wrapper', $form['bundle']));

    $form_state->setRebuild();

    return $response;
  }

  /**
   * AJAX callback handler that adds a field to a ruleset.
   */
  public function addField(array $form, FormStateInterface $form_state) : array {
    // Get the button that triggered the ajax call.
    $button = $form_state->getTriggeringElement();
    if (!$button) {
      return $form;
    }

    // Get our data from the clicked button.
    /** @var \Drupal\Core\Field\FieldConfigInterface $field */
    $field = $button['#field'];
    $entityType = $button['#entityType'];
    $bundle = $button['#bundle'];

    // Translation Args.
    $args['%label'] = $field->getLabel();

    // Get current field config if any.
    $rules = $this->config(HarmonizeConfig::ENTITY_RULES)->get(HarmonizeConfig::ENTITY_RULES__RULES);

    // Check if the rule is set already.
    if ($bundle) {
      if (isset($rules[$entityType]['bundles'][$bundle]['rules'][$field->getName()])) {
        $this->messenger()->addError($this->t('Field %label is already defined in the rules.', $args));
        return $form;
      }
    }
    else {
      if (isset($rules[$entityType]['rules'][$field->getName()])) {
        $this->messenger()->addError($this->t('Field %label is already defined in the rules.', $args));
        return $form;
      }
    }

    // Now set default values for the new field rule.
    if ($bundle) {
      $rules[$entityType]['bundles'][$bundle]['rules'][$field->getName()] = [];
    }
    else {
      $rules[$entityType]['rules'][$field->getName()] = [];
    }

    // If we reach here then rules aren't already set, so we set them in config.
    $this->config(HarmonizeConfig::ENTITY_RULES)
      ->set(HarmonizeConfig::ENTITY_RULES__RULES, $rules)
      ->save();

    // A nice little message before we exit.
    $this->messenger()->addStatus($this->t('Field %label was added.', $args));
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Step 1 submission.
    if ($this->step === 1) {
      // Simply increment the step and move forward.
      // At this point, the user simply selected an Entity Type and Bundle if
      // applicable.
      $this->step++;
      $form_state->setRebuild();
    }

    // There are actually no step 2 submissions. We manage the rest without
    // actual submissions.
  }

}
