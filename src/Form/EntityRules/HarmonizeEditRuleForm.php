<?php

namespace Drupal\harmonize\Form\EntityRules;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\harmonize\Constants\HarmonizeConfig;
use Drupal\harmonize\Service\Helpers;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a class to handle the rule editing form.
 *
 * @package Drupal\harmonize\Form
 *
 * @noinspection PhpUnused
 */
class HarmonizeEditRuleForm extends ConfigFormBase {

  /**
   * The Helpers service injected through DI.
   *
   * @var \Drupal\harmonize\Service\Helpers
   */
  protected Helpers $helpers;

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   ConfigFactory service.
   * @param \Drupal\harmonize\Service\Helpers $helpers
   *   Harmonize Helpers service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, Helpers $helpers) {
    parent::__construct($configFactory);
    $this->helpers = $helpers;
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container) : HarmonizeEditRuleForm {
    return new static(
      $container->get('config.factory'),
      $container->get('harmonize.helpers'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'harmonize_admin_add_rules_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() : array {
    return [
      HarmonizeConfig::ENTITY_RULES,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entityTypeId = NULL, $bundle = NULL, $field = NULL) : array {
    // Set this for better organization of form values later.
    $form['#tree'] = TRUE;

    // Some setup.
    $form['#prefix'] = '<div id="harmonize_edit_rule_modal_form">';
    $form['#suffix'] = '</div>';
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    // The status messages that will contain any form errors.
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    // Set our parameters to our form state.
    $form_state->set('entityTypeId', $entityTypeId);
    $form_state->set('bundle', $bundle);
    $form_state->set('field', $field);

    // Get our field storage configuration.
    $fieldConfig = FieldStorageConfig::loadByName($entityTypeId, $field);

    // Actions.
    $form['actions'] = ['#type' => 'actions'];

    // Get current rules.
    $rules = $this->config(HarmonizeConfig::ENTITY_RULES)->get(HarmonizeConfig::ENTITY_RULES__RULES);
    if ($bundle) {
      $fieldRules = $rules[$entityTypeId]['bundles'][$bundle]['rules'][$field] ?? [];
    }
    else {
      $fieldRules = $rules[$entityTypeId]['rules'][$field] ?? [];
    }

    // Exclusion rule.
    $form['rules']['excluded'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Exclude'),
      '#description'   => $this->t(
        'If checked, this field will be excluded from preprocessing results.'
      ),
      '#default_value' => $fieldRules['excluded'] ?? FALSE,
    ];

    // Disable caching rule.
    $form['rules']['disabled_cache'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Disable caching for this field'),
      '#description'   => $this->t(
        'If checked, data preprocessed for this field will not be cached.'
      ),
      '#default_value' => $fieldRules['disabled_cache'] ?? FALSE,
      '#states'        => [
        'enabled' => [
          ':input[name="rules[excluded]"]' => ['checked' => FALSE],
        ],
      ],
    ];

    // Automatically harmonize entity.
    // This should only be shown for entity reference fields.
    if ($fieldConfig->getType() === "entity_reference" || $fieldConfig->getType() === "entity_reference_revisions") {
      $form['rules']['auto_harmonize'] = [
        '#type'          => 'checkbox',
        '#title'         => $this->t('Automatically Harmonize Referenced Entity'),
        '#description'   => $this->t(
          'Normally, a <strong>Harmonizer</strong> is returned when processing entity reference fields. Checking this will bypass that behaviour and directly return the preprocessed data of the referenced entity/entities.'
        ),
        '#default_value' => $fieldRules['auto_harmonize'] ?? FALSE,
        '#states'        => [
          'enabled' => [
            ':input[name="rules[excluded]"]' => ['checked' => FALSE],
          ],
        ],
      ];

      // Load styles from config & default theme.
      $styles = $this->helpers->getStyles();
      $defaultThemeStyles = $this->helpers->getHarmonyThemeStylesConfig($this->helpers->getDefaultTheme())['styles'] ?? [];

      // Merge our styles.
      // As mentioned elsewhere in the module, theme styles take precedence if
      // we have duplicates.
      $styles = array_merge($styles, $defaultThemeStyles);

      // Build the selector if we have styles.
      if (!empty($styles)) {
        $styleOptions = [
          '' => '-- Select --',
        ];
        foreach ($styles as $machineName => $values) {
          $styleOptions[$machineName] = $machineName;
        }
        // Entity type selector.
        $form['rules']['style'] = [
          '#title' => $this->t('Style'),
          '#type' => 'select',
          '#options' => $styleOptions,
          '#default_value' => $fieldRules['style'] ?? '',
          '#states'        => [
            'enabled' => [
              ':input[name="rules[excluded]"]' => ['checked' => FALSE],
            ],
          ],
        ];
      }
    }

    // Save button.
    $form['actions']['save'] = [
      '#type' => 'submit',
      '#name' => 'save',
      '#value' => $this->t('Save'),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'button',
          'button-action',
          'button--primary',
        ],
      ],
      '#ajax' => [
        'callback' => '::ajax',
        'wrapper' => 'harmonize_edit_rule_modal_form',
        'disable-refocus' => TRUE,
      ],
    ];

    // Finalization.
    $form['actions']['done'] = [
      '#type' => 'link',
      '#title' => $this->t('Done'),
      '#url' => Url::fromRoute('harmonize.entity_processing_rules'),
      '#attributes' => [
        'class' => ['button'],
      ],
    ];

    return $form;
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function ajax(array $form, FormStateInterface $form_state) : array {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get current rules.
    $rules = $this->config(HarmonizeConfig::ENTITY_RULES)->get(HarmonizeConfig::ENTITY_RULES__RULES);

    // Parameters.
    $entityTypeId = $form_state->get('entityTypeId');
    $bundle = $form_state->get('bundle');
    $field = $form_state->get('field');

    // Get submitted values.
    $values = $form_state->getValues();

    // Format booleans.
    foreach ($values['rules'] as $index => $value) {
      if ($value === 0 || $value === 1) {
        $values['rules'][$index] = (bool) $value;
      }
    }

    // Set rules either for a bundle or for an entity type.
    if (!empty($bundle)) {
      $rules[$entityTypeId]['bundles'][$bundle]['rules'][$field] = $values['rules'];
    }
    else {
      $rules[$entityTypeId]['rules'][$field] = $values['rules'];
    }

    // If we reach here then rules aren't already set, so we set them in config.
    $this->config(HarmonizeConfig::ENTITY_RULES)
      ->set(HarmonizeConfig::ENTITY_RULES__RULES, $rules)
      ->save();

    $this->messenger()->addStatus($this->t('Rules have been saved.'));
  }

}
