<?php

namespace Drupal\harmonize\Form\EntityRules;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\harmonize\Constants\HarmonizeConfig;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a class to handle the rule deletion form.
 *
 * @package Drupal\harmonize\Form
 *
 * @noinspection PhpUnused
 */
class HarmonizeDeleteRuleForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'harmonize_admin_add_rules_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() : array {
    return [
      HarmonizeConfig::ENTITY_RULES,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entityTypeId = NULL, $bundle = NULL, $field = NULL) : array {
    // Set this for better organization of form values later.
    $form['#tree'] = TRUE;

    // Some setup.
    $form['#prefix'] = '<div id="harmonize_delete_rule_modal_form">';
    $form['#suffix'] = '</div>';
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    // The status messages that will contain any form errors.
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    // Set our parameters to our form state.
    $form_state->set('entityTypeId', $entityTypeId);
    $form_state->set('bundle', $bundle);
    $form_state->set('field', $field);

    // Actions.
    $form['actions'] = ['#type' => 'actions'];

    // Validation text.
    $validationMarkup = $this->t("The following rule set will be deleted:") . "<br><br>";
    $validationMarkup .= "<code>$entityTypeId $bundle $field</code><br><br>";
    $validationMarkup .= "Are you sure you want to proceed?";
    $form['notice'] = [
      '#type' => 'container',
      '#markup' => $validationMarkup,
    ];

    $form['actions']['confirm'] = [
      '#type'       => 'submit',
      '#name'       => 'confirm',
      '#value'      => $this->t('Confirm'),
      '#attributes' => [
        'class' => [
          'button',
          'button-action',
          'button--primary',
        ],
      ],
    ];

    $form['actions']['cancel'] = [
      '#type'       => 'button',
      '#value'      => $this->t('Cancel'),
      '#attributes' => [
        'class' => [
          'use-ajax',
        ],
      ],
      '#ajax'       => [
        'callback'        => '::cancel',
        'wrapper'         => 'harmonize_add_rules_modal_form',
        'disable-refocus' => TRUE,
      ],
    ];

    return $form;
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function ajax(array $form, FormStateInterface $form_state) : array {
    return $form;
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function cancel(array &$form, FormStateInterface $form_state) : AjaxResponse {
    $response = new AjaxResponse();
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get current rules.
    $rules = $this->config(HarmonizeConfig::ENTITY_RULES)->get(HarmonizeConfig::ENTITY_RULES__RULES);

    // Parameters.
    $entityTypeId = $form_state->get('entityTypeId');
    $bundle = $form_state->get('bundle');
    $field = $form_state->get('field');

    if (!empty($bundle)) {
      unset($rules[$entityTypeId]['bundles'][$bundle]['rules'][$field]);
    }
    else {
      unset($rules[$entityTypeId]['rules'][$field]);
    }

    // If we reach here then rules aren't already set, so we set them in config.
    $this->configFactory->getEditable(HarmonizeConfig::ENTITY_RULES)
      ->set(HarmonizeConfig::ENTITY_RULES__RULES, $rules)
      ->save();

    // Translation Args.
    $args['%entityType'] = $entityTypeId;
    $args['%bundle'] = $bundle;
    $args['%field'] = $field;

    $this->messenger()->addStatus($this->t('Ruleset for %entityType %bundle %field has been deleted successfully.', $args));

    $path = Url::fromRoute('harmonize.entity_processing_rules')->toString();
    $response = new RedirectResponse($path);
    $response->send();
  }

}
