<?php

namespace Drupal\harmonize\Form\EntityRules;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\harmonize\Constants\HarmonizeConfig;
use Drupal\harmonize\Service\Helpers;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a configuration form Entity Processing Rules.
 *
 * @package Drupal\harmonize\Form
 *
 * @noinspection PhpUnused
 */
class HarmonizeEntityProcessingRulesForm extends ConfigFormBase {

  /**
   * The Helpers service injected through DI.
   *
   * @var \Drupal\harmonize\Service\Helpers
   */
  protected Helpers $helpers;

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   ConfigFactory service.
   * @param \Drupal\harmonize\Service\Helpers $helpers
   *   Harmonize Helpers service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, Helpers $helpers) {
    parent::__construct($configFactory);
    $this->helpers = $helpers;
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container) : HarmonizeEntityProcessingRulesForm {
    return new static(
      $container->get('config.factory'),
      $container->get('harmonize.helpers'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'harmonize_admin_entity_processing_rules_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() : array {
    return [
      HarmonizeConfig::ENTITY_RULES,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) : array {
    // Set this for better organization of form values later.
    $form['#tree'] = TRUE;

    // Button to add new rules.
    $form['add-rule'] = [
      '#type' => 'link',
      '#title' => $this->t('Add rules'),
      '#url' => Url::fromRoute('harmonize.add_rules'),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'button',
          'button-action',
          'button--primary',
          'button--small',
        ],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode([
          'width' => 700,
        ]),
      ],
    ];

    // Now we want to list rules that are already defined.
    $entityRules = $this->config(HarmonizeConfig::ENTITY_RULES)->get(HarmonizeConfig::ENTITY_RULES__RULES);

    // Create a section for our theme rules.
    $form['rules'] = [
      '#type'   => 'container',
      '#open'   => TRUE,
      '#prefix' => $this->t("<br><br><h2>Rules</h2>
                   <p>Rules are global settings that affect the data returned when Harmonize processes entity data.</p>
                   <p>These settings are best used for exclusions of fields or disabling caching.</p>"),
    ];

    // Build our tables.
    if (!empty($entityRules)) {
      $this->buildRulesTables($entityRules, $form['rules']);
    }

    // We do an interesting check here.
    // If the array has 3 elements, it means no rules were added, so we add an
    // empty message.
    if (count($form['rules']) <= 3) {
      $form['rules']['#prefix'] .= $this->t("<p>There are currently no rules defined. Use the button above to add new rules.</p>");
    }

    // Let's add a neat little section describing how to build styles in themes.
    $form['infos'] = [
      '#type' => 'container',
      '#open' => TRUE,
      '#prefix' => $this->t('<br><hr><br><h2>Defining Rules In Themes</h2><p>You can define rules by creating a <code>THEME_NAME.harmony.rules.yml</code> file at the root of a theme.<br>Copy the contents below to get started.</p>'),
    ];
    $form['infos']['example_yml'] = [
      '#type' => 'details',
      '#title' => $this->t("<strong>THEME_NAME.harmony.rules.yml Example</strong>"),
    ];

    $modulePath = \Drupal::service('extension.list.module')->getPath('harmonize');
    $exampleYml = file_get_contents($modulePath . "/files/THEME_NAME.harmony.rules.yml");

    $form['infos']['example_yml']['yml'] = [
      '#type' => 'textarea',
      '#value' => $exampleYml,
      '#rows' => 40,
    ];

    // Now let's add rules declared in themes.
    // Get current theme use config.
    $defaultThemeMachineName = $this->helpers->getDefaultTheme();

    // Now we want to list rules that are defined in the theme.
    $themeRules = $this->helpers->getHarmonyThemeRulesConfig($defaultThemeMachineName);

    // If theme rules are empty, we're done.
    if (empty($themeRules)) {
      return $form;
    }
    else {
      // Create a section for our theme rules.
      $form['theme_rules'] = [
        '#type' => 'container',
        '#open' => TRUE,
        '#prefix' => $this->t('<br><hr><br><h2>Theme Rules</h2><p>The rules below have been defined in a <code>THEME_NAME.harmony.rules.yml</code> file.</p>'),
      ];
      $form['theme_rules'][$defaultThemeMachineName] = [
        '#type' => 'details',
        '#open' => FALSE,
        '#title' => $defaultThemeMachineName,
        '#noOperations' => TRUE,
      ];

      // Build our tables.
      $this->buildRulesTables($themeRules, $form['theme_rules'][$defaultThemeMachineName], FALSE);
    }

    // Return the form with all necessary fields.
    return $form;
  }

  /**
   * Build a table of rules defined.
   *
   * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
   */
  public function buildRulesTableBase(string $title, bool $showOperations = TRUE) : array {
    $container = [
      '#type' => 'container',
      '#title' => $title,
      '#open' => TRUE,
      '#theme' => 'harmonize_admin_fields_table',
      '#parents' => [],
      '#header' => [
        $this->t('Field (Type)'),
        $this->t('Rules'),
      ],
    ];
    if ($showOperations) {
      $container['#header'][] = [
        'data' => $this->t('Operations'),
        'colspan' => 2,
      ];
    }
    return $container;
  }

  /**
   * Build a table of rules defined.
   */
  public function buildRulesSubTable(string $title, bool $showOperations = TRUE) : array {
    $table = $this->buildRulesTableBase($title, $showOperations);
    $table['#type'] = 'details';
    return $table;
  }

  /**
   * Build a collection of tables of rules defined.
   */
  public function buildRulesTables(array $rules, array &$element, $showOperations = TRUE) : void {
    foreach ($rules as $entityTypeId => $subConfig) {
      // Set flag that will tell us if there are bundle rules.
      $bundleRulesExist = FALSE;

      // Get the entity type.
      try {
        $entityType = $this->helpers->entityTypeManager->getDefinition($entityTypeId);
      }
      catch (PluginNotFoundException) {
        return;
      }

      // Build table.
      $sectionLabel = $entityType->getLabel();

      // Define our section.
      $element[$entityTypeId] = [
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => $sectionLabel,
      ];

      // Build base table.
      $element[$entityTypeId]['general'] = $this->buildRulesTableBase($sectionLabel, $showOperations);

      // Get the rules set for this entity type.
      $rules = $subConfig['rules'] ?? [];

      // If the rules are empty, we remove the table header.
      if (empty($rules)) {
        unset($element[$entityTypeId]['general']['#header']);
      }
      else {
        // Build our rows.
        $element[$entityTypeId]['general']['fields'] = $this->buildFieldsRowsForEntityType($rules, $entityTypeId, $showOperations);
      }

      // Now we need sub tables for bundles if applicable.
      if (isset($subConfig['bundles'])) {
        $bundlesRules = $subConfig['bundles'];
        foreach ($bundlesRules as $bundleId => $bundleSubConfig) {
          $bundleInfo = $this->helpers->entityTypeBundleInfo->getBundleInfo($entityTypeId)[$bundleId] ?? NULL;

          if (empty($bundleInfo)) {
            continue;
          }

          // Build table.
          $element[$entityTypeId][$bundleId] = $this->buildRulesSubTable($bundleInfo['label'], $showOperations);

          // Get the rules set for this bundle.
          $brules = $bundleSubConfig['rules'] ?? [];

          // If the rules are empty, we remove the table header.
          if (empty($brules)) {
            unset($element[$entityTypeId][$bundleId]['#header']);
            unset($element[$entityTypeId][$bundleId]);
          }
          else {
            // We know rules exist, so we can set our flag.
            $bundleRulesExist = TRUE;

            // Build our rows.
            $element[$entityTypeId][$bundleId]['fields'] = $this->buildFieldsRowsForBundle($brules, $entityTypeId, $bundleId, $showOperations);
          }
        }
      }

      // If there are no bundle rules set and no entity type rules set,
      // don't show.
      if (empty($rules) && !$bundleRulesExist) {
        unset($element[$entityTypeId]);
      }
    }
  }

  /**
   * Build rows of fields for a rules table.
   */
  public function buildFieldsRows(array $rules, array $fieldConfigs, array $info, $showOperations = TRUE) : array {
    $build = [];
    foreach ($rules as $fieldMachineName => $ruleset) {
      // Get field configuration.
      /** @var \Drupal\Core\Field\FieldConfigInterface $field */
      $field = $fieldConfigs[$fieldMachineName];

      // If the field could not be loaded, we can stop here.
      if (empty($field)) {
        continue;
      }

      $build[$fieldMachineName]['title'] = [
        '#markup' => "$fieldMachineName <small><code>({$field->getType()})</code></small>",
      ];
      $ruleset = $this->formatRuleset($ruleset);
      $build[$fieldMachineName]['rules'] = [
        '#markup' => !empty($ruleset) ? $ruleset : "<code>" . $this->t('No rules defined for this field.') . "</code>",
      ];

      $routes = [
        'edit' => isset($info['bundle']) ? 'harmonize.entity_processing_rules.edit_bundle' : 'harmonize.entity_processing_rules.edit',
        'delete' => isset($info['bundle']) ? 'harmonize.entity_processing_rules.delete_bundle' : 'harmonize.entity_processing_rules.delete',
      ];

      $routeParameters = [
        'entityTypeId' => $info['entityTypeId'],
        'bundle' => $info['bundle'] ?? NULL,
        'field' => $fieldMachineName,
      ];

      if ($showOperations) {
        // Edit button.
        $build[$fieldMachineName]['edit'] = [
          '#type' => 'link',
          '#title' => $this->t('Edit'),
          '#url' => Url::fromRoute($routes['edit'], $routeParameters),
          '#attributes' => [
            'class' => [
              'use-ajax',
              'button',
              'button-action',
              'button--primary',
              'button--small',
            ],
            'data-dialog-type' => 'modal',
            'data-dialog-options' => Json::encode([
              'width' => 700,
            ]),
          ],
        ];

        // Remove button.
        $build[$fieldMachineName]['remove'] = [
          '#type' => 'link',
          '#title' => $this->t('Remove'),
          '#url' => Url::fromRoute($routes['delete'], $routeParameters),
          '#attributes' => [
            'class' => [
              'use-ajax',
              'button',
              'button-action',
              'button--primary',
              'button--small',
            ],
            'data-dialog-type' => 'modal',
            'data-dialog-options' => Json::encode([
              'width' => 700,
            ]),
          ],
        ];
      }
    }
    return $build;
  }

  /**
   * Build rows of fields for an entity type rules table.
   *
   * @see HarmonizeEntityProcessingRulesForm::buildFieldsRows()
   */
  public function buildFieldsRowsForEntityType(array $rules, string $entityTypeId, bool $showOperations = TRUE) : array {
    // Get the fields for this entity type.
    $fields = $this->helpers->getEntityTypeFieldDefinitions($entityTypeId);

    // Build info.
    $info = [
      'entityTypeId' => $entityTypeId,
    ];

    return $this->buildFieldsRows($rules, $fields, $info, $showOperations);
  }

  /**
   * Build rows of fields for a bundle rules table.
   *
   * @see HarmonizeEntityProcessingRulesForm::buildFieldsRows()
   */
  public function buildFieldsRowsForBundle(array $rules, string $entityTypeId, string $bundle, bool $showOperations = TRUE) : array {
    // Get the fields for this entity type.
    $fields = $this->helpers->getEntityTypeBundleFieldDefinitions($entityTypeId, $bundle);

    // Build info.
    $info = [
      'entityTypeId' => $entityTypeId,
      'bundle' => $bundle,
    ];

    return $this->buildFieldsRows($rules, $fields, $info, $showOperations);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) : void {
    // Save the configuration.
    $this->config(HarmonizeConfig::ENTITY_RULES)->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Format a list of rules into readable markup.
   *
   * @param array $ruleset
   *   The set of rules to format.
   *
   * @return string|null
   *   The formatted markup. Won't return NULL unless I do something stupid.
   */
  private function formatRuleset(array $ruleset) : ?string {
    $formatted = ' ';
    $firstRuleAdded = FALSE;
    foreach ($ruleset as $rule => $value) {
      if ($firstRuleAdded && !empty($value)) {
        $formatted .= "<br>";
      }
      switch ($rule) {
        case 'excluded':
          if ($value) {
            $title = $this->t('This field will be excluded from preprocessed data.');
            $formatted .= "<strong><span title=\"$title\">Excluded</span></strong>";
          }
          break;

        case 'disabled_cache':
          if ($value) {
            $title = $this->t('Preprocessed data for this field will not be cached.');
            $formatted .= "<strong><span title=\"$title\">Cache Disabled</span></strong>";
          }
          break;

        case 'auto_harmonize':
          if ($value) {
            $title = $this->t('When preprocessing this field, the data for referenced entities will automatically be preprocessed.');
            $formatted .= "<strong><span title=\"$title\">Process Referenced Entities</span></strong>";
          }
          break;

        case 'style':
          if ($value) {
            $title = $this->t("When preprocessing this field, the following Style will be used: @style.", ['@style' => $value]);
            $formatted .= "<strong><span title=\"$title\">Style: <code>$value</code></span></strong>";
          }
          break;
      }
      if ($formatted !== ' ') {
        $firstRuleAdded = TRUE;
      }
    }
    return $formatted === ' ' ? NULL : $formatted;
  }

}
