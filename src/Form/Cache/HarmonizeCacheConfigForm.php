<?php

namespace Drupal\harmonize\Form\Cache;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\harmonize\Constants\HarmonizeConfig;

/**
 * Provides a configuration form for caching settings.
 *
 * @package Drupal\harmonize\Form
 *
 * @noinspection PhpUnused
 */
class HarmonizeCacheConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'harmonize_admin_cache_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() : array {
    return [
      HarmonizeConfig::CACHE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) : array {
    // Set this for better organization of form values later.
    $form['#tree'] = TRUE;

    // Get the current configurations.
    $cacheEnabled = $this->config(HarmonizeConfig::CACHE)->get(HarmonizeConfig::CACHE__ENABLED);
    $cacheExclusions = $this->config(HarmonizeConfig::CACHE)->get(HarmonizeConfig::CACHE__EXCLUSIONS);
    $cacheExclusions = empty($cacheExclusions) ? [] : (is_array($cacheExclusions) ? implode("\n", $cacheExclusions) : $cacheExclusions);

    // Enable Caching.
    $form[HarmonizeConfig::CACHE__ENABLED] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enable Caching'),
      '#description'   => $this->t(
        'Note: This will only cache the <strong>base</strong> preprocessing of data.<br>
              Any data alterations through <strong>Refiners</strong> or <strong>Harmony Files</strong> are not cached by the module.<br>
              This is intended, since the front-end result will be cached by the theme layer.<br>
              Pro Tip: Keep this on!!'
      ),
      '#default_value' => $cacheEnabled ?? TRUE,
      '#weight'        => -15,
    ];

    // Configure cache exclusions.
    $form[HarmonizeConfig::CACHE__EXCLUSIONS] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Exclusions'),
      '#description'   => $this->t(
        'On each line, enter the ID of a harmonizer you would like to disable caching for.<br>
              <em>e.g. <strong>entity.node.page</strong></em><br>
              <em>e.g. <strong>entity.paragraph.slider</strong></em><br>
              <em>e.g. <strong>region.header</strong></em><br>
              <em>e.g. <strong>field.field_media</strong></em>'
      ),
      '#default_value' => !empty($cacheExclusions) ? $cacheExclusions : '',
      '#weight'        => -15,
      '#states'        => [
        'visible' => [
          ':input[name="cache_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Return the form with all necessary fields.
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) : void {
    // Retrieve the configuration.
    $values = $form_state->getValues();

    // Format cache exclusions values.
    $exclusions = empty($values[HarmonizeConfig::CACHE__EXCLUSIONS]) ? [] : array_map('trim', explode("\n", $values[HarmonizeConfig::CACHE__EXCLUSIONS]));

    $this->config(HarmonizeConfig::CACHE)
      ->set(HarmonizeConfig::CACHE__ENABLED, $values[HarmonizeConfig::CACHE__ENABLED])
      ->set(HarmonizeConfig::CACHE__EXCLUSIONS, $exclusions)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
