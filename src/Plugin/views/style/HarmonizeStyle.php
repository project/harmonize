<?php

namespace Drupal\harmonize\Plugin\views\style;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\harmonize\Service\Harmonize;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The style plugin for serialized output formats using the Harmonizer service.
 *
 * @noinspection AnnotationMissingUseInspection
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "harmonizer",
 *   title = @Translation("Harmonize"),
 *   help = @Translation("Harmonizes views row data using the Harmonize module."),
 *   display_types = {"data"}
 * )
 *
 * @noinspection PhpUnused
 */
class HarmonizeStyle extends StylePluginBase implements CacheableDependencyInterface {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesGrouping = FALSE;

  /**
   * Harmonizer manager service.
   *
   * @var \Drupal\harmonize\Service\Harmonize
   */
  protected Harmonize $harmonizeService;

  /**
   * Current page request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected ?Request $currentRequest;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) : HarmonizeStyle {
    /* @noinspection PhpParamsInspection */
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('harmonize'),
      $container->get('request_stack')
    );
  }

  /**
   * Constructs a HarmonizerStyle object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pluginId
   *   The plugin_id for the plugin instance.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   * @param \Drupal\harmonize\Service\Harmonize $harmonizeService
   *   Harmonizer manager service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack service.
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, Harmonize $harmonizeService, RequestStack $requestStack) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->harmonizeService = $harmonizeService;
    $this->currentRequest = $requestStack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Alter the view to include the total rows count.
    $this->view->get_total_rows = TRUE;
    parent::query();
  }

  /**
   * {@inheritdoc}
   */
  public function render(): bool|array|string {
    $rows = [];

    foreach ($this->view->result as $row) {
      if ($entity = $row->_entity) {
        $rows[] = $this->harmonizeService->harmonize($entity) ?? NULL;
      }
    }

    $result = [
      'meta' => [
        'arguments' => $this->getArguments(),
        'limit' => $this->view->query->getLimit(),
        // @todo For some reason the query object doesn't have a proper way to have the offset value.
        'offset' => $this->view->query->offset ?? 0,
        'total' => \intval($this->view->total_rows),
        'displayed' => count($rows),
      ],
      'data' => $rows,
    ];

    // @todo The Drupal\Component\Serialization\Json object doesn't support the "JSON_PRETTY_PRINT" option.
    return json_encode($result, JSON_PRETTY_PRINT | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT);
  }

  /**
   * Finds the views contextual filters' values.
   *
   * @return array
   *   Keyed list of results.
   */
  protected function getArguments() : array {
    $filters = [];

    foreach ($this->view->argument as $key => $argument) {
      $paramKey = $argument->options['default_argument_options']['query_param'] ?? $key;
      $paramValue = $argument->getValue() ?? $argument->options['default_argument_options']['query_param']['fallback'];
      $filters[$paramKey] = $paramValue;
    }

    return $filters;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() : array {
    return ['request_format'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() : array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() : int {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormats() : array {
    return ['data'];
  }

}
