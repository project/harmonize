<?php

namespace Drupal\harmonize\Service;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\harmonize\Constants\HarmonizeConfig;
use Drupal\harmonize\Harmonizer\HarmonizerInterface;
use Drupal\harmonize\Harmonizer\MasterHarmonizerFactory;

/**
 * Service used to access the module's main functionality.
 *
 * @package Drupal\harmonize\Service
 */
final class Harmonize {

  /**
   * Helpers service injected through DI.
   *
   * Provides helper functions specific to Harmonizer functionality.
   *
   * @var Helpers
   */
  public Helpers $helpers;

  /**
   * Drupal's Theme Manager injected through DI.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  public ThemeManagerInterface $themeManager;

  /**
   * Drupal's module handler injected through DI.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  public ModuleHandlerInterface $moduleHandler;

  /**
   * Drupal's event dispatcher injected through DI.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  public ContainerAwareEventDispatcher $eventDispatcher;

  /**
   * Use DI to inject Drupal's configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  public ConfigFactoryInterface $configFactory;

  /**
   * Drupal's Admin Context router service injected through DI.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  public AdminContext $adminContext;

  /**
   * Drupal's Current Route Match service injected through DI.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  public CurrentRouteMatch $currentRouteMatch;

  /**
   * Harmonizer cache factory service injected through DI.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  public CacheBackendInterface $cacheBackend;

  /**
   * Harmonizer Language Manager service injected through DI.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  public LanguageManagerInterface $languageManager;

  /**
   * File URL Generator service injected through DI.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  public FileUrlGeneratorInterface $fileUrlGenerator;

  /**
   * The Drupal Logger Channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  public LoggerChannelInterface $loggerChannel;

  /**
   * Static flag that determines if the Harmonize functionality is activated.
   *
   * @var bool|null
   */
  public static ?bool $powered = NULL;

  /**
   * Static array of configurations loaded for the module.
   *
   * @var array
   */
  public static array $config;

  /**
   * Service that contains all functionality for the harmonizer module.
   *
   * This service manages the creation and loading of all harmonizers.
   *
   * @param Helpers $harmonizerHelpers
   *   Harmonizer Helpers service injected through DI.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   *   Theme Manager service injected through DI.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module Handler service injected through DI.
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $eventDispatcher
   *   Event Dispatcher service injected through DI.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration Factory service injected through DI.
   * @param \Drupal\Core\Routing\AdminContext $adminContext
   *   Admin Context service injected through DI.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   Current Route Match service injected through DI.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Harmonizer cache factory service injected through DI.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Language Manager service injected through DI.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   File URL Generator service injected through DI.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Logger Channel factory injected through DI.
   */
  public function __construct(
    Helpers $harmonizerHelpers,
    ThemeManagerInterface $themeManager,
    ModuleHandlerInterface $moduleHandler,
    ContainerAwareEventDispatcher $eventDispatcher,
    ConfigFactoryInterface $configFactory,
    AdminContext $adminContext,
    CurrentRouteMatch $currentRouteMatch,
    CacheBackendInterface $cacheBackend,
    LanguageManagerInterface $languageManager,
    FileUrlGeneratorInterface $fileUrlGenerator,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    $this->helpers = $harmonizerHelpers;
    $this->themeManager = $themeManager;
    $this->moduleHandler = $moduleHandler;
    $this->eventDispatcher = $eventDispatcher;
    $this->configFactory = $configFactory;
    $this->adminContext = $adminContext;
    $this->currentRouteMatch = $currentRouteMatch;
    $this->cacheBackend = $cacheBackend;
    $this->languageManager = $languageManager;
    $this->fileUrlGenerator = $fileUrlGenerator;
    $this->loggerChannel = $loggerChannelFactory->get('harmonize');
    $this->initializeConfigurations();
  }

  /**
   * Harmonizes an object, providing its values in a neatly organized array.
   *
   * This function uses the MasterHarmonizerFactory to build a harmonizer for
   * the provided object. This Master Factory then calls sub-factories, to
   * determine the best type of Harmonizer class for the job.
   *
   * @param mixed $object
   *   Object that will be harmonized.
   * @param array $flags
   *   Flags to set on the harmonizers that alter harmonizer behavior.
   * @param mixed $traceOutput
   *   Variable to output a trace into.
   *
   * @return mixed
   *   Result of the harmonization process. Returns data if successful.
   *
   * @noinspection PhpMissingReturnTypeInspection
   * @noinspection PhpMissingParamTypeInspection
   */
  public function harmonize($object, array $flags = [], &$traceOutput = NULL) {
    // Only run harmonization if it's activated.
    if (!$this->isPowered()) {
      return [];
    }

    // Fetch/build the harmonizer for this object.
    /** @var \Drupal\harmonize\Harmonizer\HarmonizerInterface $harmonizer */
    $harmonizer = $this->getHarmonizer($object, $flags);

    // Return nothing if the harmonizer could not be loaded.
    if ($harmonizer === NULL) {
      return [];
    }

    // Get the data.
    $data = $harmonizer->harmonize();

    // If the trace flag is set, we can set it to the trace output.
    if ($harmonizer->traceable()) {
      $traceOutput = $harmonizer->getTrace();
    }

    // Return the data.
    return $data;
  }

  /**
   * Fetch harmonizer using the Master Factory.
   *
   * @param mixed $object
   *   Object to fetch a harmonizer for.
   * @param array $flags
   *   Flags that affect the actions taken when harmonizing the object.
   *
   * @return \Drupal\harmonize\Harmonizer\HarmonizerInterface|null
   *   The created harmonizer if successful. Returns NULL otherwise.
   *
   * @noinspection PhpMissingParamTypeInspection
   */
  public function getHarmonizer($object, array $flags = []) : ?HarmonizerInterface {
    // Build the harmonizer appropriate harmonizer for the given object.
    return MasterHarmonizerFactory::build($object, $flags, $this);
  }

  /**
   * Return configuration.
   */
  public function config(string $id, bool $editable = FALSE) : ?Config {
    if ($editable) {
      return $this->configFactory->getEditable($id);
    }
    return $this->configFactory->get($id);
  }

  /**
   * Return configuration.
   */
  private function initializeConfigurations() : void {
    // We only need to load configurations if we're powered.
    if (!$this->isPowered()) {
      return;
    }

    // Set static configurations once per page load.
    self::$config = [
      HarmonizeConfig::MAIN__REMOVE_FIELD_UNDERSCORE_PREFIX => $this->config(HarmonizeConfig::MAIN)->get(HarmonizeConfig::MAIN__REMOVE_FIELD_UNDERSCORE_PREFIX) ?? FALSE,
      HarmonizeConfig::MAIN__LOADING_ALGORITHM              => $this->config(HarmonizeConfig::MAIN)->get(HarmonizeConfig::MAIN__LOADING_ALGORITHM) ?? 'fcfs',
      HarmonizeConfig::MAIN__AUTO_HARMONIZED_ENTITIES       => $this->config(HarmonizeConfig::MAIN)->get(HarmonizeConfig::MAIN__AUTO_HARMONIZED_ENTITIES) ?? [],
      HarmonizeConfig::MAIN__MAX_DEPTH                      => $this->config(HarmonizeConfig::MAIN)->get(HarmonizeConfig::MAIN__MAX_DEPTH) ?? 6,
      HarmonizeConfig::ENTITY_RULES__RULES                  => $this->config(HarmonizeConfig::ENTITY_RULES)->get(HarmonizeConfig::ENTITY_RULES__RULES) ?? [],
      HarmonizeConfig::CACHE__ENABLED                       => $this->config(HarmonizeConfig::CACHE)->get(HarmonizeConfig::CACHE__ENABLED) ?? TRUE,
      HarmonizeConfig::CACHE__EXCLUSIONS                    => $this->config(HarmonizeConfig::CACHE)->get(HarmonizeConfig::CACHE__EXCLUSIONS) ?? [],
    ];
  }

  /**
   * Determines whether the Harmonizer should run.
   *
   * Certain conditions will disable it completely for certain pages.
   *
   * @return bool
   *   Return boolean stating if the harmonizer is activated.
   */
  private function isPowered() : bool {
    // If it already ran, return the stored value.
    if (self::$powered !== NULL) {
      return self::$powered;
    }

    // Initialize the flag variable. By default, the harmonizer is on.
    $powered = TRUE;

    // Get admin routes.
    $acceptedAdminRoutes = $this->config(HarmonizeConfig::MAIN)->get(HarmonizeConfig::MAIN__ALLOWED_ADMIN_ROUTES) ?? [];

    // Disable harmonization in admin context.
    // We will accept certain admin routes for specific cases.
    // This will accept the Layout Builder routes to allow proper preview of
    // elements that are harmonized. Note that this is only relevant if the
    // Layout Builder module is enabled. Nothing should break if it isn't.
    // This Layout Builder fix was added before the admin routes configuration
    // was developed and will be kept here for now as it is harmless.
    $acceptedAdminRoutes = array_merge($acceptedAdminRoutes, [
      // This is the route for the visualizer.
      // We want Harmonize to work here for obvious reasons.
      'harmonize.visualize',

      // Layout builder admin routes.
      'layout_builder.add_block',
      'layout_builder.move_block',
      'layout_builder.remove_block',
    ]);

    // If we're not in an accepted admin route, we power off Harmonize.
    if (
      $this->adminContext->isAdminRoute($this->currentRouteMatch->getRouteObject())
      && !in_array($this->currentRouteMatch->getRouteName(), $acceptedAdminRoutes)
    ) {
      $powered = FALSE;
    }

    // Set the static flag.
    // This should only run once per page load.
    self::$powered = $powered;

    return $powered;
  }

}
