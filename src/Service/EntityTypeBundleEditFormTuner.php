<?php

namespace Drupal\harmonize\Service;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\harmonize\Constants\HarmonizeConfig;

/**
 * Provides a form tuner for Entity Type Bundle Edit forms.
 *
 * @package Drupal\harmonize\Service\FormTuner
 */
final class EntityTypeBundleEditFormTuner extends FormTunerBase {

  /**
   * ID of this form tuner.
   */
  public const ID = 'harmonize.form_tuner.entity_type_bundle_edit';

  /**
   * {@inheritDoc}
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityBundleBase $formEntity
   */
  public function alter(array &$form, FormStateInterface $formState, EntityInterface $formEntity) : void {
    // Get entity type ID and bundle.
    $entityTypeId = $formEntity->getEntityType()->getBundleOf();
    $bundle = $formEntity->id();

    // Attach library.
    $form['#attached']['library'][] = 'harmonize/entity_type_edit_form';

    // Try to get existing configurations.
    $config = \Drupal::service('harmonize')->config(HarmonizeConfig::MAIN);

    // Wrap everything in a fieldset.
    $form['harmonize'] = [
      '#type' => 'details',
      '#title' => $this->t('Harmonize settings'),
      '#collapsed' => FALSE,
      '#collapsible' => TRUE,
      '#tree' => FALSE,
      '#weight' => 10,
      '#group' => 'additional_settings',
      '#attributes' => ['class' => ['harmonize-entity-settings-form']],
    ];

    $form['harmonize']['preprocess_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Preprocess data for templates'),
      '#default_value' => $config->get(HarmonizeConfig::MAIN__PREPROCESSING)["{$entityTypeId}__$bundle"] ?? FALSE,
      '#description' => $this->t('Preprocesses entity fields, making clean data accessible for template rendering.
                        <br><br>Grants access to the <strong>Manage preprocessing</strong> tab, where preprocessed fields can be customized.'),
      '#attributes' => ['class' => ['harmonize-entity-settings-preprocess-enabled']],
    ];

    // Attach our submit handler.
    $form['actions']['submit']['#submit'][] = get_class() . '::submitHandler';
  }

  /**
   * {@inheritDoc}
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityBundleBase $formEntity
   */
  public function alterAccountForm(array &$form, FormStateInterface $formState) : void {
    // Get entity type ID and bundle.
    $entityTypeId = 'user';
    $bundle = 'user';

    // Attach library.
    $form['#attached']['library'][] = 'harmonize/entity_type_edit_form';

    // Try to get existing configurations.
    $config = \Drupal::service('harmonize')->config(HarmonizeConfig::MAIN);

    // Wrap everything in a fieldset.
    $form['harmonize'] = [
      '#type' => 'details',
      '#title' => $this->t('Harmonize settings'),
      '#collapsed' => FALSE,
      '#collapsible' => TRUE,
      '#tree' => FALSE,
      '#weight' => 10,
      '#group' => 'additional_settings',
      '#attributes' => ['class' => ['harmonize-entity-settings-form']],
    ];

    $form['harmonize']['preprocess_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Preprocess data for templates'),
      '#default_value' => $config->get(HarmonizeConfig::MAIN__PREPROCESSING)["{$entityTypeId}__$bundle"] ?? FALSE,
      '#description' => $this->t('Preprocesses entity fields, making clean data accessible for template rendering.
                        <br><br>Grants access to the <strong>Manage preprocessing</strong> tab, where preprocessed fields can be customized.'),
      '#attributes' => ['class' => ['harmonize-entity-settings-preprocess-enabled']],
    ];

    // Attach our submit handler.
    $form['actions']['submit']['#submit'][] = get_class() . '::accountSubmitHandler';
  }

  /**
   * Custom submit handler.
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function submitHandler(array $form, FormStateInterface $formState) {
    // Get stuff we need before proceeding.
    $formObject = $formState->getFormObject();
    /* @noinspection PhpPossiblePolymorphicInvocationInspection */
    $formEntity = $formObject->getEntity();
    $entityTypeId = $formEntity->getEntityType()->getBundleOf();
    $bundle = $formEntity->id();

    // Try to get existing configurations.
    $config = \Drupal::service('harmonize')->config(HarmonizeConfig::MAIN, TRUE);

    // Now we get the form values we need and add them to the entity.
    $preprocess_enabled = $formState->getValue('preprocess_enabled');

    // Set them in config and save.
    $currentConfig = $config->get(HarmonizeConfig::MAIN__PREPROCESSING) ?? [];
    $currentConfig["{$entityTypeId}__$bundle"] = $preprocess_enabled;
    $config->set(HarmonizeConfig::MAIN__PREPROCESSING, $currentConfig);
    $config->save();
  }

  /**
   * Custom submit handler (account form).
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function accountSubmitHandler(array $form, FormStateInterface $formState) {
    // Get stuff we need before proceeding.
    /* @noinspection PhpPossiblePolymorphicInvocationInspection */
    $entityTypeId = 'user';
    $bundle = 'user';

    // Try to get existing configurations.
    $config = \Drupal::service('harmonize')->config(HarmonizeConfig::MAIN, TRUE);

    // Now we get the form values we need and add them to the entity.
    $preprocess_enabled = $formState->getValue('preprocess_enabled');

    // Set them in config and save.
    $currentConfig = $config->get(HarmonizeConfig::MAIN__PREPROCESSING) ?? [];
    $currentConfig["{$entityTypeId}__$bundle"] = $preprocess_enabled;
    $config->set(HarmonizeConfig::MAIN__PREPROCESSING, $currentConfig);
    $config->save();
  }

}
