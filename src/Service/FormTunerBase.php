<?php

namespace Drupal\harmonize\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Base class for Form Tuners.
 *
 * Form Tuners are quite simply services that are meant to alter core
 * Drupal forms.
 *
 * @package Drupal\harmonize\Service\FormTuner
 */
abstract class FormTunerBase {

  use StringTranslationTrait;

  /**
   * Config Factory service injected through DI.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  public ConfigFactoryInterface $configFactory;

  /**
   * FormTuner constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration Factory service injected through DI.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * Alter form.
   *
   * @param array $form
   *   Render array of the form being altered.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   * @param \Drupal\Core\Entity\EntityInterface $formEntity
   *   Form entity.
   */
  abstract public function alter(array &$form, FormStateInterface $formState, EntityInterface $formEntity) : void;

}
