<?php

namespace Drupal\harmonize\Service;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\field\FieldStorageConfigInterface;
use Drupal\harmonize\Entity\EntitySettings;
use Drupal\harmonize\Entity\Style;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;
use Symfony\Component\Yaml\Yaml;

/**
 * Provides helper functions to be used across the module's code.
 *
 * @package Drupal\harmonize\Service
 */
final class Helpers {
  use StringTranslationTrait;

  /**
   * Use DI to inject Drupal's configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  public ConfigFactoryInterface $configFactory;

  /**
   * Entity Display Repository Service from Drupal injected through DI.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  public EntityDisplayRepositoryInterface $entityDisplayRepository;

  /**
   * Entity Type Manager Service from Drupal injected through DI.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public EntityTypeManagerInterface $entityTypeManager;

  /**
   * Entity Type Bundle Info Service from Drupal injected through DI.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  public EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * Entity Field Manager Service from Drupal injected through DI.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  public EntityFieldManagerInterface $entityFieldManager;

  /**
   * Language Manager Service from Drupal injected through DI.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  public LanguageManagerInterface $languageManager;

  /**
   * Theme Extension List Service from Drupal injected through DI.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  public ThemeManagerInterface $themeManager;

  /**
   * Theme Extension List Service from Drupal injected through DI.
   *
   * @var \Drupal\Core\Extension\ExtensionList
   */
  public ExtensionList $themeExtensionList;

  /**
   * Harmonizer cache factory service injected through DI.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  public CacheBackendInterface $cacheBackend;

  /**
   * The Drupal Logger Channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  public LoggerChannelInterface $loggerChannel;

  /**
   * Helpers constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration Factory service injected through DI.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entityDisplayRepository
   *   Drupal's EntityDisplayRepositoryInterface service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Drupal's EntityTypeManager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   Drupal's EntityTypeBundleInfo service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   Drupal's EntityFieldManager service.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   *   Drupal's Theme Manager service.
   * @param \Drupal\Core\Extension\ExtensionList $themeExtensionList
   *   Drupal's Theme Extension List service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Drupal's LanguageManager service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Caching service for Harmonize.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Logger Channel factory injected through DI.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    EntityDisplayRepositoryInterface $entityDisplayRepository,
    EntityTypeManagerInterface $entityTypeManager,
    EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    EntityFieldManagerInterface $entityFieldManager,
    ThemeManagerInterface $themeManager,
    ExtensionList $themeExtensionList,
    LanguageManagerInterface $languageManager,
    CacheBackendInterface $cacheBackend,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    $this->configFactory = $configFactory;
    $this->entityDisplayRepository = $entityDisplayRepository;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->entityFieldManager = $entityFieldManager;
    $this->themeManager = $themeManager;
    $this->themeExtensionList = $themeExtensionList;
    $this->languageManager = $languageManager;
    $this->cacheBackend = $cacheBackend;
    $this->loggerChannel = $loggerChannelFactory->get('harmonize');
  }

  /**
   * Return configuration.
   */
  public function config(string $id) : ?ImmutableConfig {
    return $this->configFactory->get($id);
  }

  /**
   * Get the default theme of the site.
   */
  public function getDefaultTheme() : string {
    return $this->configFactory->get('system.theme')->get('default');
  }

  /**
   * Helper function to check if a view mode exists for an entity type.
   *
   * @param string $entityTypeId
   *   Entity type to check view modes for.
   * @param string $id
   *   The ID/Machine Name of the view mode.
   *
   * @return bool
   *   Returns TRUE if the view mode exists, returns FALSE otherwise.
   */
  public function viewModeExists(string $entityTypeId, string $id) : bool {
    // The 'default' one isn't listed in the repository, but it exists.
    if ($id === 'default') {
      return TRUE;
    }

    // Load all existing view modes.
    $modes = $this->entityDisplayRepository->getAllViewModes();

    // Returns whether the view mode is set.
    return isset($modes[$entityTypeId][$id]);
  }

  /**
   * Helper function to check if a view mode is enabled for an entity type.
   *
   * @param string $entityTypeId
   *   Entity type to check view modes for.
   * @param string $bundle
   *   Bundle to check view modes for.
   * @param string $id
   *   The ID/Machine Name of the view mode.
   *
   * @return bool
   *   Returns TRUE if the view mode is enabled, returns FALSE otherwise.
   */
  public function viewModeEnabled(string $entityTypeId, string $bundle, string $id) : bool {
    // The 'default' one is always enabled.
    if ($id === 'default') {
      return TRUE;
    }

    // Load view display.
    $display = $this->entityDisplayRepository->getViewDisplay($entityTypeId, $bundle, $id);

    // Returns whether the view mode is enabled.
    return !empty($display) && $display->status();
  }

  /**
   * Helper function to check if a view mode exists for an entity type.
   *
   * @param string $entityTypeId
   *   Entity type to check view modes for.
   *
   * @return bool
   *   Returns TRUE if the view mode exists, returns FALSE otherwise.
   *
   * @noinspection PhpUnused
   */
  public function getAvailableViewModes(string $entityTypeId) : bool {
    // Load all existing view modes.
    $modes = $this->entityDisplayRepository->getAllViewModes();

    // Returns whether the view mode is set.
    return $modes[$entityTypeId];
  }

  /**
   * Helper function to get display settings for an entity type bundle.
   *
   * @param string $entityTypeId
   *   Entity type to get display settings for.
   * @param string $bundle
   *   Bundle to get display settings for.
   * @param string $viewMode
   *   The view mode we want settings for.
   * @param string $fieldName
   *   The field we want to get display settings for.
   *
   * @return array|null
   *   The display settings for the provided field.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getViewModeDisplaySettings(string $entityTypeId, string $bundle, string $viewMode, string $fieldName) : ?array {
    // Alright. Self-explanatory. Kind of.
    try {
      $entityViewDisplay = $this->entityTypeManager
        ->getStorage('entity_view_display')
        ->load($entityTypeId . '.' . $bundle . '.' . $viewMode);

      // If the entity view display could not be found,
      // we fall back to the 'default' view mode.
      if ($entityViewDisplay === NULL) {
        $entityViewDisplay = $this->entityTypeManager
          ->getStorage('entity_view_display')
          ->load($entityTypeId . '.' . $bundle . '.default');
      }
      /* @noinspection PhpPossiblePolymorphicInvocationInspection */
      $renderer = $entityViewDisplay->getRenderer($fieldName);

      return $renderer?->getSettings();
    }
    catch (\Exception $e) {
      // Send a message that states the view mode settings could not be found.
      // Drupal by default renders entities in the 'full' view mode, but on
      // new websites, this view mode is sometimes not initialized in the
      // database.
      return NULL;
    }
  }

  /**
   * Load entity settings.
   *
   * @param string $entityTypeId
   *   Entity Type ID to load settings for.
   * @param string $bundle
   *   Entity Bundle to load settings for.
   * @param string $viewMode
   *   View mode to load settings for.
   *
   * @return \Drupal\harmonize\Entity\EntitySettings|null
   *   Returns our settings if found.
   */
  public function loadEntitySettings(string $entityTypeId, string $bundle, string $viewMode): ?EntitySettings {
    $settings = EntitySettings::load("$entityTypeId.$bundle.$viewMode");
    if (empty($settings)) {
      // Fallback to default view mode...
      // Inconsistencies in Drupal forces us to do this.
      $settings = EntitySettings::load("$entityTypeId.$bundle.default");
      if (empty($settings)) {
        return NULL;
      }
    }
    return $settings;
  }

  /**
   * Get all style configurations.
   *
   * @param bool $entities
   *   If set to TRUE, will return the actual entities instead of their values.
   *
   * @return array|null
   *   The styles and their values.
   */
  public function getStyles(bool $entities = FALSE) : ?array {
    try {
      $stylesQuery = $this->entityTypeManager->getStorage('harmonize_style')->getQuery();
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      $this->loggerChannel->error($this->t("An error occurred when trying to load styles.<br><br>"));
      $this->loggerChannel->error($e->getMessage());
      return [];
    }

    $styleIds = $stylesQuery->execute();
    $styles = Style::loadMultiple($styleIds);

    if ($entities) {
      return $styles;
    }

    return array_map(function ($style) {
      return $style->data();
    }, $styles);
  }

  /**
   * Get the configured style, if any.
   *
   * @param string $id
   *   The ID of the style we want to check for.
   *
   * @returns bool
   *   Returns TRUE if the style exists, returns FALSE otherwise.
   */
  public function styleExists(string $id): bool {
    // Load the appropriate style.
    $style = Style::load($id);

    // If the style is properly loaded we continue.
    if (!empty($style)) {
      return TRUE;
    }

    // Load Theme configurations.
    $harmonyThemeConfig = $this->getHarmonyThemeStylesConfig();

    // If we have a style of this name declared in the theme, we merge.
    // @todo Don't we want theme styles to take precedence? Review this.
    if (isset($harmonyThemeConfig['styles'][$id])) {
      return TRUE;
    }

    // Return FALSE if the style is not found.
    return FALSE;
  }

  /**
   * Attempt to get Harmony styles configurations from the current theme.
   *
   * @noinspection ReturnTypeCanBeDeclaredInspection
   *
   * @return array|null
   *   The harmony theme configurations.
   */
  public function getHarmonyThemeStylesConfig(string $theme = NULL) : ?array {
    // Get the active theme if the theme is null.
    if (empty($theme)) {
      $theme = $this->themeManager->getActiveTheme()->getName();
    }

    return $this->getHarmonyThemeConfig($theme, '.harmony.styles.yml');
  }

  /**
   * Attempt to get Harmony rules configurations from the current theme.
   *
   * @noinspection ReturnTypeCanBeDeclaredInspection
   *
   * @return array|null
   *   The harmony theme configurations.
   */
  public function getHarmonyThemeRulesConfig(string $theme = NULL) : ?array {
    // Get the active theme if the theme is null.
    if (empty($theme)) {
      $theme = $this->themeManager->getActiveTheme()->getName();
    }

    return $this->getHarmonyThemeConfig($theme, '.harmony.rules.yml');
  }

  /**
   * Attempt to get Harmony configurations from the current theme.
   *
   * @noinspection ReturnTypeCanBeDeclaredInspection
   *
   * @return array|null
   *   The harmony theme configurations.
   */
  public function getHarmonyThemeConfig(string $theme, string $fileSuffix) : ?array {
    // Cache.
    $cid = 'harmonize.themes.' . $theme . $fileSuffix;
    $cachedConfig = $this->cacheBackend->get($cid);

    if (!$cachedConfig) {
      // We want to check if a Harmony declaration file exists in the
      // theme. If one does, we want to read it and see if a
      // declaration exists for this particular context to set styles.
      $themeDirectory = $this->themeExtensionList->getPath($theme);
      $harmonyDeclarationPath = $themeDirectory . '/' . $theme . $fileSuffix;

      // If no declaration file exists, stop.
      if (!file_exists($harmonyDeclarationPath)) {
        return NULL;
      }

      // Get the configuration file.
      $harmonyConfig = Yaml::parse(file_get_contents($harmonyDeclarationPath));

      // Store it in the cache.
      $this->cacheBackend->set($cid, $harmonyConfig);

      // Return it.
      return $harmonyConfig;
    }

    // Return the cached config.
    return $cachedConfig->data;
  }

  /**
   * Get the list of fields for a given entity.
   *
   * @param string $entityTypeId
   *   Entity Type in string machine_name format.
   *
   * @return array
   *   Return list of all fields for the given entity/bundle pair.
   */
  public function getEntityTypeFieldDefinitions(string $entityTypeId) : array {
    // Return only custom fields, filtering out base fields.
    return array_filter($this->entityFieldManager->getFieldStorageDefinitions($entityTypeId), function ($object) {
      return in_array(FieldStorageConfigInterface::class, class_implements($object), TRUE);
    });
  }

  /**
   * Get the list of fields for a given entity.
   *
   * @param string $entityTypeId
   *   Entity Type in string machine_name format.
   * @param string $bundle
   *   Entity Bundle in string machine_name format.
   *
   * @return array
   *   Return list of all fields for the given entity/bundle pair.
   */
  public function getEntityTypeBundleFieldDefinitions(string $entityTypeId, string $bundle) : array {
    // Return only custom fields, filtering out base fields.
    return array_filter($this->entityFieldManager->getFieldDefinitions($entityTypeId, $bundle), function ($object) {
      return in_array(FieldConfigInterface::class, class_implements($object), TRUE);
    });
  }

  /**
   * Ensure an entity is translated to the current running language.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity to fetch translation from.
   * @param string|null $lang
   *   The language to ensure the content is translated to.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   Return translated entity in current language if found.
   */
  public function ensureTranslated(ContentEntityInterface $entity, string $lang = NULL) : ?ContentEntityInterface {
    // Get current language.
    $language = $lang ?? $this->languageManager->getCurrentLanguage()->getId();

    // Remember to check if translation exists.
    // Drupal isn't handling the printing of the translation automatically.
    if ($entity->hasTranslation($language)) {
      $entity = $entity->getTranslation($language);
    }

    return $entity;
  }

  /**
   * Get the output of the dump() function as a string.
   *
   * @param mixed $var
   *   Variable to dump.
   *
   * @return string|null
   *   The output.
   *
   * @noinspection PhpMissingParamTypeInspection
   */
  public function getDumpOutput($var) : ?string {
    $cloner = new VarCloner();
    $dumper = new HtmlDumper();
    $output = '';

    try {
      $dumper->dump(
        $cloner->cloneVar($var),
        function ($line, $depth) use (&$output) {
          // A negative depth means "end of dump".
          if ($depth >= 0) {
            // Adds a two spaces indentation to the line.
            $output .= str_repeat('  ', $depth) . $line . "\n";
          }
        }
      );
    }
    catch (\ErrorException) {
      return NULL;
    }

    return $output;
  }

}
