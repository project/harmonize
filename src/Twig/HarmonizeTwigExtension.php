<?php

namespace Drupal\harmonize\Twig;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityHarmonizerInterface;
use Drupal\harmonize\Harmonizer\HarmonizerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Harmonize twig extension with some useful functions and filters.
 *
 * The extension consumes quite a lot of dependencies. Most of them are not used
 * on each page request. For performance reasons services are wrapped in static
 * callbacks.
 *
 * @noinspection PhpUnused
 */
class HarmonizeTwigExtension extends AbstractExtension {

  /**
   * The module handler to invoke alter hooks.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The theme manager to invoke alter hooks.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected ThemeManagerInterface $themeManager;

  /**
   * Constructs HarmonizeTwigExtension.
   */
  public function __construct(ModuleHandlerInterface $module_handler, ThemeManagerInterface $theme_manager) {
    $this->moduleHandler = $module_handler;
    $this->themeManager = $theme_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() : array {
    // Functions array.
    $functions = [
      new TwigFunction('harmonize', [self::class, 'harmonize']),
    ];

    // Enable modules & themes to alter functions.
    $this->moduleHandler->alter('harmonize_twig_functions', $functions);
    $this->themeManager->alter('harmonize_twig_functions', $functions);

    return $functions;
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters() : array {
    // Filters array.
    // No filters for now. Maybe later.
    $filters = [
      new TwigFilter('harmonize', [self::class, 'harmonize']),
      new TwigFilter('get', [self::class, 'get']),
    ];

    // Enable modules & themes to alter filters.
    $this->moduleHandler->alter('harmonize_twig_filters', $filters);
    $this->themeManager->alter('harmonize_twig_filters', $filters);

    return $filters;
  }

  /**
   * {@inheritdoc}
   */
  public function getTests() : array {
    // Filters array.
    // No tests for now. Maybe later.
    $tests = [];

    // Enable modules & themes to alter tests.
    $this->moduleHandler->alter('harmonize_twig_tests', $tests);
    $this->themeManager->alter('harmonize_twig_tests', $tests);

    return $tests;
  }

  /**
   * Builds the render array for a block.
   */
  public static function harmonize($object, array $flags = []) : array {
    // If the object is not an Entity Harmonizer, then what are they going?
    if ($object instanceof HarmonizerInterface) {
      return $object->harmonize();
    }
    return \Drupal::service('harmonize')->harmonize($object, $flags);
  }

  /**
   * Gets field from a harmonizer.
   */
  public static function get($object, string $fieldName) {
    // If the object is not an Entity Harmonizer, then what are they going?
    if (!($object instanceof EntityHarmonizerInterface)) {
      return NULL;
    }
    return $object->get($fieldName);
  }

}
