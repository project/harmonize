<?php

namespace Drupal\harmonize\Entity;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Defines the Entity Settings entity.
 *
 * @ConfigEntityType(
 *   id = "harmonize_entity_settings",
 *   label = @Translation("Harmonize settings"),
 *   handlers = {},
 *   config_prefix = "entity_settings",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "populate_variables" = "populate_variables",
 *     "display_filtering" = "display_filtering",
 *     "display_settings" = "display_settings",
 *   },
 *   config_export = {
 *     "id",
 *     "targetEntityType",
 *     "bundle",
 *     "viewMode",
 *     "processed",
 *     "excluded",
 *   },
 *   links = {}
 * )
 */
class EntitySettings extends ConfigEntityBase implements EntitySettingsInterface {

  /**
   * Unique ID for the config entity.
   *
   * @var string
   */
  protected $id;

  /**
   * Entity type to be displayed.
   *
   * @var string
   */
  protected $targetEntityType;

  /**
   * Bundle to be displayed.
   *
   * @var string
   */
  protected $bundle;

  /**
   * A list of field definitions eligible for configuration in this display.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface[]
   */
  protected $fieldDefinitions;

  /**
   * View or form mode to be displayed.
   *
   * @var string
   */
  protected string $viewMode = 'default';

  /**
   * List of processed fields, keyed by component name.
   *
   * @var array
   */
  protected array $processed = [];

  /**
   * List of fields that are set to be excluded.
   *
   * @var array
   */
  protected array $excluded = [];

  /**
   * The plugin manager used by this entity type.
   *
   * @var \Drupal\Component\Plugin\PluginManagerBase
   */
  protected PluginManagerInterface $pluginManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values, $entity_type) {
    $this->pluginManager = \Drupal::service('plugin.manager.field.formatter');

    if (!isset($values['targetEntityType']) || !isset($values['bundle'])) {
      throw new \InvalidArgumentException('Missing required properties for an EntitySettings entity.');
    }

    if (!$this->entityTypeManager()->getDefinition($values['targetEntityType'])->entityClassImplements(FieldableEntityInterface::class)) {
      throw new \InvalidArgumentException('EntitySettings entities can only handle fieldable entity types.');
    }

    parent::__construct($values, $entity_type);

    $this->init();
  }

  /**
   * Initializes the display.
   *
   * This fills in default options for components:
   * - that are not explicitly known as either "visible" or "hidden" in the
   *   display,
   * - or that are not supposed to be configurable.
   */
  protected function init() {
    $default_region = 'processed';

    // Fill in defaults for fields.
    $fields = $this->getFieldDefinitions();
    foreach ($fields as $name => $definition) {
      if (!$definition->isDisplayConfigurable('view') || (!isset($this->processed[$name]) && !isset($this->excluded[$name]))) {
        $options = $definition->getDisplayOptions('view');

        if (!empty($options['region']) && $options['region'] === 'hidden') {
          $this->removeComponent($name);
        }
        elseif ($options) {
          $options += ['region' => $default_region];
          $this->setComponent($name, $options);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityTypeId(): string {
    return $this->targetEntityType;
  }

  /**
   * {@inheritdoc}
   */
  public function getViewMode() {
    return $this->get('viewMode');
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetBundle(): string {
    return $this->bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetBundle($bundle): EntitySettings {
    $this->set('bundle', $bundle);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function id(): string {
    return $this->targetEntityType . '.' . $this->bundle . '.' . $this->viewMode;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    // Ensure that a region is set on each component.
    foreach ($this->getComponents() as $name => $component) {
      // Ensure that a region is set.
      if (isset($this->processed[$name]) && !isset($component['region'])) {
        // Directly set the component to bypass other changes in setComponent().
        $this->processed[$name]['region'] = $this->getDefaultRegion();
      }
    }

    ksort($this->processed);
    ksort($this->excluded);
    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();
    $target_entity_type = $this->entityTypeManager()->getDefinition($this->targetEntityType);

    // Create dependency on the bundle.
    $bundle_config_dependency = $target_entity_type->getBundleConfigDependency($this->bundle);
    $this->addDependency($bundle_config_dependency['type'], $bundle_config_dependency['name']);

    // If field.module is enabled, add dependencies on 'field_config' entities
    // for both displayed and hidden fields. We intentionally leave out base
    // field overrides, since the field still exists without them.
    if (\Drupal::moduleHandler()->moduleExists('field')) {
      $components = $this->processed + $this->excluded;
      $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($this->targetEntityType, $this->bundle);
      foreach (array_intersect_key($field_definitions, $components) as $field_definition) {
        if ($field_definition instanceof ConfigEntityInterface && $field_definition->getEntityTypeId() == 'field_config') {
          $this->addDependency('config', $field_definition->getConfigDependencyName());
        }
      }
    }

    // Depend on configured modes.
    if ($this->viewMode != 'default') {
      $mode_entity = $this->entityTypeManager()->getStorage('entity_view_mode')->load($target_entity_type->id() . '.' . $this->viewMode);
      $this->addDependency('config', $mode_entity->getConfigDependencyName());
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function toArray(): array {
    $properties = parent::toArray();
    // Do not store options for fields whose display is not set to be
    // configurable.
    foreach ($this->getFieldDefinitions() as $field_name => $definition) {
      if (!$definition->isDisplayConfigurable('view')) {
        unset($properties['processed'][$field_name]);
        unset($properties['excluded'][$field_name]);
      }
    }

    return $properties;
  }

  /**
   * Create a copy of these settings.
   *
   * @param string $mode
   *   The resulting view mode of the copy.
   *
   * @return EntitySettings
   *   The resulting copy.
   */
  public function createCopy(string $mode): EntitySettings {
    $settings = $this->createDuplicate();
    $settings->viewMode = $mode;
    return $settings;
  }

  /**
   * Get components that are part of these settings.
   *
   * @return array
   *   Array of components that are part of these settings.
   */
  public function getComponents(): array {
    return $this->processed;
  }

  /**
   * Get settings for a processed component.
   *
   * @param string $name
   *   The name of the component to retrieve.
   *
   * @return array|null
   *   Specific settings for the requested component.
   */
  public function getComponent(string $name): ?array {
    return $this->processed[$name] ?? NULL;
  }

  /**
   * Set a component as processed in these settings.
   *
   * @param string $name
   *   The name of the component to set.
   * @param array $options
   *   The options to set for this component.
   *
   * @return EntitySettings
   *   Return the current settings.
   */
  public function setComponent(string $name, array $options = []): EntitySettings {
    // If no weight specified, make sure the field sinks at the bottom.
    if (!isset($options['weight'])) {
      $max = $this->getHighestWeight();
      $options['weight'] = isset($max) ? $max + 1 : 0;
    }

    // Ensure we always have an empty settings and array.
    $options += ['settings' => [], 'third_party_settings' => []];

    $this->processed[$name] = $options;
    unset($this->excluded[$name]);

    return $this;
  }

  /**
   * Remove a component from the list of processed components.
   *
   * @param string $name
   *   Name of the component to remove.
   *
   * @return EntitySettings
   *   The current entity settings.
   */
  public function removeComponent(string $name): EntitySettings {
    $this->excluded[$name] = TRUE;
    unset($this->processed[$name]);

    return $this;
  }

  /**
   * Get the processed component with the highest weight.
   *
   * @return int|null
   *   Returns the highest weight.
   */
  public function getHighestWeight(): ?int {
    $weights = [];

    // Collect weights for the components in the display.
    foreach ($this->processed as $options) {
      if (isset($options['weight'])) {
        $weights[] = $options['weight'];
      }
    }

    // Let other modules feedback about their own additions.
    $weights = array_merge($weights, \Drupal::moduleHandler()->invokeAll('field_info_max_weight', [
      $this->targetEntityType,
      $this->bundle,
      'view',
      $this->viewMode,
    ]));

    return $weights ? max($weights) : NULL;
  }

  /**
   * Gets the field definition of a field.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface|null
   *   Return the field definition if found.
   */
  protected function getFieldDefinition($field_name): ?FieldDefinitionInterface {
    $definitions = $this->getFieldDefinitions();
    return $definitions[$field_name] ?? NULL;
  }

  /**
   * Gets the definitions of the fields that are candidate for display.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   Returns an array of field definitions.
   */
  protected function getFieldDefinitions(): array {
    if (!isset($this->fieldDefinitions)) {
      $definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($this->targetEntityType, $this->bundle);
      $definitions = array_filter($definitions, [
        $this,
        'fieldHasDisplayOptions',
      ]);
      $this->fieldDefinitions = $definitions;
    }

    return $this->fieldDefinitions;
  }

  /**
   * Determines if a field has options for a given display.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $definition
   *   A field definition.
   *
   * @return array|null
   *   Return the display options for the field if found.
   */
  private function fieldHasDisplayOptions(FieldDefinitionInterface $definition): ?array {
    // The display only cares about fields that specify display options.
    // Discard base fields that are not rendered through formatters / widgets.
    return $definition->getDisplayOptions('view');
  }

  /**
   * Gets the default region.
   *
   * @return string
   *   The default region for this display.
   */
  protected function getDefaultRegion(): string {
    return 'processed';
  }

  /**
   * {@inheritdoc}
   */
  public function onDependencyRemoval(array $dependencies) {
    $changed = parent::onDependencyRemoval($dependencies);
    foreach ($dependencies['config'] as $entity) {
      if ($entity->getEntityTypeId() == 'field_config') {
        // Remove components for fields that are being deleted.
        $this->removeComponent($entity->getName());
        unset($this->excluded[$entity->getName()]);
        $changed = TRUE;
      }
    }
    return $changed;
  }

}
