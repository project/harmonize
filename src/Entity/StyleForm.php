<?php

namespace Drupal\harmonize\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form to edit Style entities.
 *
 * @package Drupal\harmonize\Form\Entity\Style
 *
 * @noinspection PhpUnused
 */
class StyleForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) : array {
    // Get the base entity form stuff.
    $form = parent::form($form, $form_state);

    // Setup.
    $form['#tree'] = TRUE;
    $form['#prefix'] = '<div id="harmonize_style_admin_form">';
    $form['#suffix'] = '</div>';
    $form['#attached']['library'][] = 'harmonize/style_admin_form';

    // The status messages that will contain any form errors.
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    // Get the current values of the style entity.
    /** @var \Drupal\harmonize\Entity\StyleInterface $style */
    $style = $this->entity;

    // The readable name of the style.
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $style->label(),
      '#description' => $this->t("Choose a name for the style you are creating.<br>The machine name must be unique and will be used to identify the style when using Harmonize features."),
      '#required' => TRUE,
    ];

    // The machine name of the style.
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $style->id(),
      '#machine_name' => [
        'exists' => '\Drupal\harmonize\Entity\Style::load',
      ],
    ];

    // Container for our repeating fields.
    $form['fields_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Add a field'),
      '#description' => $this->t('Fields added to a style are entity agnostic.<br>Think of styles as a field filter mechanism. When an entity is processed with a style, any field that has not been defined in said style will be skipped.'),
      '#tree' => TRUE,
    ];

    // Create a field where the user can search for fields in the site.
    $form['fields_wrapper']['field_search'] = [
      '#type' => 'textfield',
      '#autocomplete_route_name' => 'harmonize.autocomplete.entity_fields',
      '#wrapper_attributes' => [
        'class' => ['field-search-wrapper'],
      ],
    ];

    // Create an "Add" button.
    $form['fields_wrapper']['add'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#wrapper_attributes' => [
        'class' => ['field-add-button-wrapper'],
      ],
      '#attributes' => [
        'class' => [
          'use-ajax',
        ],
      ],
      '#submit' => [
        [$this, 'addField'],
      ],
      '#ajax' => [
        'callback' => [$this, 'ajax'],
        'wrapper' => 'harmonize_style_admin_form',
      ],
    ];

    // Create a fieldset for our fields.
    $form['fields'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Fields'),
      '#prefix' => '<div id="js-ajax-fields-list-wrapper">',
      '#suffix' => '</div>',
    ];

    // Now we try to get fields already set in our entity.
    $fields = $style->getFields();
    $form_state->set('fields', $form_state->get('fields') ?? $fields);

    // If there are no fields, we add an empty message.
    if (empty($form_state->get('fields'))) {
      $form['fields']['#markup'] = $this->t('There are currently no fields assigned to this style.');
    }

    // Set up fields.
    foreach ($form_state->get('fields') as $field) {
      $form['fields'][$field] = [
        '#type' => 'container',
      ];
      $form['fields'][$field]['value'] = [
        '#type' => 'textfield',
        '#disabled' => TRUE,
        '#default_value' => $field ?? '',
        '#wrapper_attributes' => [
          'class' => ['field-row-wrapper'],
        ],
      ];
      $form['fields'][$field]['remove'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#field' => $field,
        '#wrapper_attributes' => [
          'class' => ['field-remove-button-wrapper'],
        ],
        '#attributes' => [
          'class' => [
            'use-ajax',
          ],
        ],
        '#submit' => [
          [$this, 'removeField'],
        ],
        '#ajax' => [
          'callback' => [$this, 'ajax'],
          'wrapper' => 'harmonize_style_admin_form',
        ],
      ];
    }

    return $form;
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function ajax(array $form, FormStateInterface $formState) : array {
    return $form;
  }

  /**
   * AJAX callback handler to add a field to the style.
   */
  public function addField(array $form, FormStateInterface $formState) : array {
    // Get values.
    $add = $formState->getValue('fields_wrapper')['field_search'] ?? NULL;

    // If they try to add nothing...
    // Gotta be sure you know?
    if (empty($add)) {
      $this->messenger()->addError($this->t('You cannot add an empty field value.'));
    }

    // Get existing fields if any.
    $fields = $formState->get('fields') ?? [];

    // If the field is already there, we throw an error.
    if (in_array($add, $fields, TRUE)) {
      $this->messenger()->addError($this->t('This field is already defined in the Style.'));
    }

    // Add field to the array.
    $fields[] = $add;

    // Set the value in the form state. :)
    $formState->set('fields', $fields);
    $formState->setRebuild();

    return $form;
  }

  /**
   * AJAX callback handler to remove a field from the style.
   */
  public function removeField(array $form, FormStateInterface $formState): array {
    // Get the button that triggered this call.
    $button = $formState->getTriggeringElement();
    if (!$button) {
      return $form;
    }

    // Get the field we want to remove.
    $fieldToRemove = $button['#field'];

    // Get existing fields if any.
    $fields = $formState->get('fields') ?? [];

    // If the field is already there, we throw an error.
    if (!in_array($fieldToRemove, $fields, TRUE)) {
      $this->messenger()->addError($this->t('Somehow, someway, you have managed to try to delete a field that does not exist...'));
    }

    // Remove field from the array.
    if (($key = array_search($fieldToRemove, $fields)) !== FALSE) {
      unset($fields[$key]);
    }

    // Set the value in the form state. :)
    $formState->set('fields', $fields);
    $formState->setRebuild();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    parent::copyFormValuesToEntity($entity, $form, $form_state);
    $formValues = $form_state->getValues();
    $fields = [];

    if (empty($formValues['fields'])) {
      return;
    }

    foreach ($formValues['fields'] as $elements) {
      $fields[] = $elements['value'];
    }

    if ($entity instanceof ConfigEntityInterface) {
      $entity->set('fields', $fields);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $form_state->setRedirect('entity.harmonize_style.collection');
    $this->messenger()
      ->addStatus($this->t('Successfully updated %label', [
        '%label' => $this->entity->label(),
      ]));
  }

}
