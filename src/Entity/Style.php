<?php

namespace Drupal\harmonize\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Style entity.
 *
 * @ConfigEntityType(
 *   id = "harmonize_style",
 *   label = @Translation("Style"),
 *   handlers = {
 *     "list_builder" = "Drupal\harmonize\Entity\StyleListBuilder",
 *     "form" = {
 *       "default" = "Drupal\harmonize\Entity\StyleForm",
 *       "add" = "Drupal\harmonize\Entity\StyleForm",
 *       "edit" = "Drupal\harmonize\Entity\StyleForm",
 *       "delete" = "Drupal\harmonize\Entity\StyleDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "styles",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/config/harmonize/style/add",
 *     "edit-form" = "/admin/config/harmonize/style/{harmonize_style}/edit",
 *     "delete-form" = "/admin/config/harmonize/style/{harmonize_style}/delete",
 *     "collection" = "/admin/config/harmonize/styles"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "fields",
 *   }
 * )
 */
class Style extends ConfigEntityBase implements StyleInterface {

  /**
   * The unique identifier of the style.
   *
   * @var string
   */
  protected string $id;

  /**
   * The readable name of the style.
   *
   * @var string|null
   */
  protected ?string $label;

  /**
   * The list of fields that this style is set to harmonize.
   *
   * Think of this as a sort of filter. When you harmonize an entity with a
   * Style, only the fields specified in the style will be processed.
   *
   * @var array
   */
  protected array $fields = [];

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
   */
  public function data() : array {
    return [
      'fields' => $this->getFields(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFields() : array {
    return $this->fields ?? [];
  }

}
