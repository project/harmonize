<?php

namespace Drupal\harmonize\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Style entities.
 */
interface StyleInterface extends ConfigEntityInterface {

  /**
   * Fetches all data of this style.
   *
   * @return array
   *   Data of the style.
   */
  public function data() : array;

  /**
   * Fetches all fields of this style.
   *
   * @return array
   *   Fields that are set in the style.
   */
  public function getFields() : array;

}
