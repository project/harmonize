<?php

namespace Drupal\harmonize\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Entity settings entities.
 */
interface EntitySettingsInterface extends ConfigEntityInterface {

}
