<?php

namespace Drupal\harmonize\Entity;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\field_ui\FieldUI;
use Drupal\field_ui\Form\EntityViewDisplayEditForm;
use Drupal\harmonize\Constants\HarmonizeConfig;

/**
 * Edit form for the EntitySettings entity type.
 *
 * We extend the EntityViewDisplayEditForm class since our form is quite
 * similar.
 *
 * This decision proved to be quite messy in the end, and although it
 * works for now, the goal will be to abandon the dependency on the
 * EntityViewDisplayEditForm class, and rewrite the functionality.
 *
 * No rush on that though.
 *
 * Function Overrides will change what we need to change.
 *
 * @internal
 */
class EntitySettingsEditForm extends EntityViewDisplayEditForm {

  /**
   * The entity being used by this form.
   *
   * @var EntitySettings
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id): EntitySettings {
    $route_parameters = $route_match->getParameters()->all();

    return $this->getEntitySettings($route_parameters['entity_type_id'], $route_parameters['bundle'], $route_parameters['view_mode_name']);
  }

  /**
   * Get appropriate EntitySettings entity given the arguments.
   *
   * If one is not found, create it here.
   *
   * @param string $entity_type_id
   *   Target entity type of the EntitySettings entity.
   * @param string $bundle
   *   Target bundle of the EntitySettings entity.
   * @param string $view_mode
   *   Target view_mode of the EntitySettings entity.
   *
   * @returns EntitySettings
   *   The appropriate EntitySettings entity to be used in this form.
   */
  protected function getEntitySettings(string $entity_type_id, string $bundle, string $view_mode): EntitySettings {
    return EntitySettings::load("$entity_type_id.$bundle.$view_mode") ?? EntitySettings::create([
      'id' => "$entity_type_id.$bundle.$view_mode",
      'targetEntityType' => $entity_type_id,
      'bundle' => $bundle,
      'viewMode' => $view_mode,
    ]);
  }

  /**
   * Get the regions needed to create the overview form.
   *
   * @see \Drupal\field_ui\Form\EntityDisplayFormBase::getRegions()
   */
  public function getRegions(): array {
    return [
      'content' => [
        'title' => $this->t('Processed Fields'),
        'message' => $this->t('No field is currently processed.'),
      ],
      'hidden' => [
        'title' => $this->t('Excluded Fields', [], ['context' => 'Plural']),
        'message' => $this->t('No field is currently excluded.'),
      ],
    ];
  }

  /**
   * Returns an associative array of all regions.
   *
   * @see \Drupal\field_ui\Form\EntityDisplayFormBase::getRegionsOptions()
   */
  public function getRegionOptions(): array {
    $options = [];
    foreach ($this->getRegions() as $region => $data) {
      $options[$region] = $data['title'];
    }
    return $options;
  }

  /**
   * Collects the definitions of fields whose preprocessing is configurable.
   *
   * Base fields are ignored.
   * We're really only interested in custom fields with configurable
   * display options.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   The array of field definitions
   */
  protected function getFieldDefinitions(): array {
    return array_filter($this->entityFieldManager->getFieldDefinitions($this->entity->getTargetEntityTypeId(), $this->entity->getTargetBundle()), function (FieldDefinitionInterface $field_definition) {
      return $field_definition->isDisplayConfigurable('view') && !($field_definition instanceof BaseFieldDefinition);
    });
  }

  /**
   * Returns an array containing the table headers.
   *
   * @return array
   *   The table header.
   */
  protected function getTableHeader(): array {
    return [
      $this->t('Field'),
      $this->t('Weight'),
      $this->t('Parent'),
      $this->t('Region'),
      '',
      '',
      ['data' => $this->t('Settings'), 'colspan' => 3],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = EntityForm::form($form, $form_state);

    // Check if preprocessing is enabled for this entity type and bundle first.
    $config = \Drupal::service('harmonize')->config(HarmonizeConfig::MAIN);
    $enabled = $config->get('preprocessing')["{$this->entity->getTargetEntityTypeId()}__{$this->entity->getTargetBundle()}"] ?? FALSE;

    if (!$enabled) {
      $this->messenger()->addWarning($this->t('Preprocessing is not enabled for this entity type bundle.'));
      $form['notice'] = [
        '#type' => 'text',
        '#markup' => $this->t('Preprocessing is not enabled for this entity type bundle.'),
      ];
      return $form;
    }

    $field_definitions = $this->getFieldDefinitions();

    $form += [
      '#entity_type' => $this->entity->getTargetEntityTypeId(),
      '#bundle' => $this->entity->getTargetBundle(),
      '#fields' => array_keys($field_definitions),
    ];

    if (empty($field_definitions) && $route_info = FieldUI::getOverviewRouteInfo($this->entity->getTargetEntityTypeId(), $this->entity->getTargetBundle())) {
      $this->messenger()->addWarning($this->t('There are no fields yet added. You can add new fields on the <a href=":link">Manage fields</a> page.', [':link' => $route_info->toString()]));
      $form['notice'] = [
        '#type' => 'text',
        '#markup' => $this->t('There are no fields yet added. You can add new fields on the <a href=":link">Manage fields</a> page.', [':link' => $route_info->toString()]),
      ];
      return $form;
    }

    $table = [
      '#type' => 'field_ui_table',
      '#header' => $this->getTableHeader(),
      '#regions' => $this->getRegions(),
      '#attributes' => [
        'class' => ['field-ui-overview'],
        'id' => 'field-display-overview',
      ],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'field-weight',
        ],
        [
          'action' => 'match',
          'relationship' => 'parent',
          'group' => 'field-parent',
          'subgroup' => 'field-parent',
          'source' => 'field-name',
        ],
        [
          'action' => 'match',
          'relationship' => 'parent',
          'group' => 'field-region',
          'subgroup' => 'field-region',
          'source' => 'field-name',
        ],
      ],
    ];

    // Field rows.
    foreach ($field_definitions as $field_name => $field_definition) {
      $table[$field_name] = $this->buildFieldRow($field_definition, $form, $form_state);
    }

    $form['fields'] = $table;

    // Custom display settings.
    if ($this->entity->getViewMode() == 'default') {
      // Only show the settings if there is at least one custom display mode.
      $display_mode_options = $this->getDisplayModeOptions();
      // Unset default option.
      unset($display_mode_options['default']);
      if ($display_mode_options) {
        $form['modes'] = [
          '#type' => 'details',
          '#title' => $this->t('Custom display settings'),
        ];
        // Prepare default values for the 'Custom display settings' checkboxes.
        $default = [];
        if ($enabled_displays = array_filter($this->getDisplayStatuses())) {
          $default = array_keys(array_intersect_key($display_mode_options, $enabled_displays));
        }
        natcasesort($display_mode_options);
        $form['modes']['display_modes_custom'] = [
          '#type' => 'checkboxes',
          '#title' => $this->t('Use custom display settings for the following @display_context modes', ['@display_context' => $this->displayContext]),
          '#options' => $display_mode_options,
          '#default_value' => $default,
        ];
        // Provide link to manage display modes.
        $form['modes']['display_modes_link'] = $this->getDisplayModesLink();
      }
    }

    // In overviews involving nested rows from contributed modules (i.e
    // field_group), the 'plugin type' selects can trigger a series of changes
    // in child rows. The #ajax behavior is therefore not attached directly to
    // the selects, but triggered by the client-side script through a hidden
    // #ajax 'Refresh' button. A hidden 'refresh_rows' input tracks the name of
    // affected rows.
    $form['refresh_rows'] = ['#type' => 'hidden'];
    $form['refresh'] = [
      '#type' => 'submit',
      '#value' => $this->t('Refresh'),
      '#op' => 'refresh_table',
      '#submit' => ['::multistepSubmit'],
      '#ajax' => [
        'callback' => '::multistepAjax',
        'wrapper' => 'field-display-overview-wrapper',
        'effect' => 'fade',
        // The button stays hidden, so we hide the Ajax spinner too. Ad-hoc
        // spinners will be added manually by the client-side script.
        'progress' => 'none',
      ],
      '#attributes' => ['class' => ['visually-hidden']],
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Save'),
    ];

    $form['#attached']['library'][] = 'field_ui/drupal.field_ui';

    return $form;
  }

  /**
   * Builds the table row structure for a single field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   A table row array.
   */
  protected function buildFieldRow(FieldDefinitionInterface $field_definition, array $form, FormStateInterface $form_state): array {
    $field_name = $field_definition->getName();
    $display_options = $this->entity->getComponent($field_name);
    $label = $field_definition->getLabel();

    $regions = array_keys($this->getRegions());
    $field_row = [
      '#attributes' => ['class' => ['draggable', 'tabledrag-leaf']],
      '#row_type' => 'field',
      '#region_callback' => [$this, 'getRowRegion'],
      '#js_settings' => [
        'rowHandler' => 'field',
        'defaultPlugin' => 'undefined',
      ],
      'human_name' => [
        '#plain_text' => $label,
      ],
      'weight' => [
        '#type' => 'textfield',
        '#title' => $this->t('Weight for @title', ['@title' => $label]),
        '#title_display' => 'invisible',
        '#default_value' => $display_options ? $display_options['weight'] : '0',
        '#size' => 3,
        '#attributes' => ['class' => ['field-weight']],
      ],
      'parent_wrapper' => [
        'parent' => [
          '#type' => 'select',
          '#title' => $this->t('Label display for @title', ['@title' => $label]),
          '#title_display' => 'invisible',
          '#options' => array_combine($regions, $regions),
          '#empty_value' => '',
          '#attributes' => ['class' => ['js-field-parent', 'field-parent']],
          '#parents' => ['fields', $field_name, 'parent'],
        ],
        'hidden_name' => [
          '#type' => 'hidden',
          '#default_value' => $field_name,
          '#attributes' => ['class' => ['field-name']],
        ],
      ],
      'region' => [
        '#type' => 'select',
        '#title' => $this->t('Region for @title', ['@title' => $label]),
        '#title_display' => 'invisible',
        '#options' => $this->getRegionOptions(),
        '#default_value' => $display_options ? $display_options['region'] : 'hidden',
        '#attributes' => ['class' => ['field-region']],
      ],
      'type' => [
        '#type' => 'hidden',
        '#default_value' => $field_definition->getType(),
      ],
    ];

    // This is unused for us.
    // We just have it here to be able to leverage field_ui.js.
    $field_row['settings'] = [
      'type' => [
        '#type' => 'hidden',
        '#attributes' => ['class' => ['field-plugin-type']],
        '#cell_attributes' => ['colspan' => 0],
      ],
      'settings_edit_form' => [],
    ];

    if (!$display_options || $display_options['region'] === 'hidden') {
      return $field_row;
    }

    // Base button element for the various plugin settings actions.
    $base_button = [
      '#submit' => ['::multistepSubmit'],
      '#ajax' => [
        'callback' => '::multistepAjax',
        'wrapper' => 'field-display-overview-wrapper',
        'effect' => 'fade',
      ],
      '#field_name' => $field_name,
    ];

    if ($form_state->get('field_settings_edit') == $field_name) {
      // We are currently editing this field's plugin settings. Display the
      // settings form and submit buttons.
      $field_row['settings']['edit_form'] = [];

      // Generate the settings form and allow other modules to alter it.
      $settings_form = $this->fieldSettingsForm($field_definition);

      if ($settings_form) {
        $field_row['settings']['#cell_attributes'] = ['colspan' => 3];
        $field_row['settings']['settings_edit_form'] = [
          '#type' => 'container',
          '#attributes' => ['class' => ['field-plugin-settings-edit-form']],
          '#parents' => ['fields', $field_name, 'settings_edit_form'],
          'label' => [
            '#markup' => $this->t('Field Settings'),
          ],
          'settings' => $settings_form,
          'actions' => [
            '#type' => 'actions',
            'save_settings' => $base_button + [
                '#type' => 'submit',
                '#button_type' => 'primary',
                '#name' => $field_name . '_field_settings_update',
                '#value' => $this->t('Update'),
                '#op' => 'update',
              ],
            'cancel_settings' => $base_button + [
                '#type' => 'submit',
                '#name' => $field_name . '_field_settings_update_settings_cancel',
                '#value' => $this->t('Cancel'),
                '#op' => 'cancel',
                // Do not check errors for the 'Cancel' button, but make sure we
                // get the value of the 'plugin type' select.
                '#limit_validation_errors' => [['fields', $field_name, 'type']],
              ],
          ],
        ];
        $field_row['#attributes']['class'][] = 'field-plugin-settings-editing';
      }
    }
    else {
      $field_row['settings_summary'] = [];
      $field_row['settings_edit'] = [];

      // Display a summary of the current plugin settings, and (if the
      // summary is not empty) a button to edit them.
      $summary = $this->fieldSettingsSummary($field_definition);

      if (!empty($summary)) {
        $field_row['settings_summary'] = [
          '#type' => 'inline_template',
          '#template' => '<div class="field-plugin-summary">{{ summary|safe_join("<br />") }}</div>',
          '#context' => ['summary' => $summary],
          '#cell_attributes' => ['class' => ['field-plugin-summary-cell']],
        ];
      }

      // Check selected settings to display edit link or not.
      $settings_form = $this->fieldSettingsForm($field_definition);
      if ($settings_form) {
        $field_row['settings_edit'] = $base_button + [
            '#type' => 'image_button',
            '#name' => $field_name . '_settings_edit',
            '#src' => 'core/misc/icons/787878/cog.svg',
            '#attributes' => [
              'class' => [
                'field-plugin-settings-edit',
              ],
              'alt' => $this->t('Edit'),
            ],
            '#op' => 'edit',
            // Do not check errors for the 'Edit' button, but make sure we get
            // the value of the 'plugin type' select.
            '#limit_validation_errors' => [['fields', $field_name, 'type']],
            '#prefix' => '<div class="field-plugin-settings-edit-wrapper">',
            '#suffix' => '</div>',
          ];
      }
    }

    return $field_row;
  }

  /**
   * Returns the region to which a row in the display overview belongs.
   *
   * @param array $row
   *   The row element.
   *
   * @return string|null
   *   The region name this row belongs to.
   */
  public function getRowRegion(&$row): ?string {
    $regions = $this->getRegions();
    if (!isset($regions[$row['region']['#value']])) {
      $row['region']['#value'] = 'content';
    }
    return $row['region']['#value'];
  }

  /**
   * Return the default settings for fields.
   *
   * The default settings depend on the type of field we're dealing with.
   *
   * @param string $field_name
   *   Field name of the field we want to generate default settings for.
   * @param string $field_type
   *   Field type of the field we want to generate default settings for.
   *
   * @returns array
   *   Array of default settings.
   */
  public function fieldDefaultSettings(string $field_name, string $field_type): array {
    // Common defaults.
    $defaults = [
      'cache' => TRUE,
    ];

    // Add defaults depending on field type.
    switch ($field_type) {
      case 'entity_reference':
      case 'entity_reference_revisions':
        $defaults += [
          'auto_preprocess' => FALSE,
          'view_mode' => 'default',
        ];
        // Check if display has a view mode for this field.
        $fieldDisplaySettings = \Drupal::service('harmonize')->helpers->getViewModeDisplaySettings($this->entity->getTargetEntityTypeId(), $this->entity->getTargetBundle(), $this->entity->getViewMode(), $field_name);

        if (!empty($fieldDisplaySettings) && !empty($fieldDisplaySettings['view_mode'])) {
          $defaults['view_mode'] = 'inherit';
        }
        break;
    }

    return $defaults;
  }

  /**
   * Generate a form for per-field settings.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Field definition.
   *
   * @returns array
   *   The form to render.
   */
  public function fieldSettingsForm(FieldDefinitionInterface $field_definition): array {
    // Common form elements.
    $elements['cache'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Cache Preprocess Result'),
      '#default_value' => $this->getFieldSetting($field_definition, 'cache') ?? TRUE,
    ];

    // Get the field type.
    $type = $field_definition->getType();

    // Add defaults depending on field type.
    switch ($type) {
      case 'entity_reference':
      case 'entity_reference_revisions':
        // Get the target entity type of this field.
        $target_entity_type = $field_definition->getFieldStorageDefinition()->getSetting('target_type');

        // Check if this entity type is set to be automatically harmonized.
        $autoPreprocess = $this->fieldIsSetToAutoPreprocess($field_definition) || $this->getFieldSetting($field_definition, 'auto_preprocess');

        // Add elements.
        $elements['auto_preprocess'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Automatically Process Nested Entities'),
          '#description' => $this->fieldIsSetToAutoPreprocess($field_definition) ? $this->t('<em>This setting is overridden by either <a href="/admin/config/harmonize/settings">Main Settings</a> or <a href="/admin/config/harmonize/entity-rules">Entity Rules Settings</a></em>') : '',
          '#default_value' => $autoPreprocess,
          '#disabled' => $this->fieldIsSetToAutoPreprocess($field_definition),
        ];

        // Build view mode options.
        $viewModeOptions = [];

        // Check if display has a view mode for this field.
        $fieldDisplaySettings = \Drupal::service('harmonize')->helpers->getViewModeDisplaySettings($this->entity->getTargetEntityTypeId(), $this->entity->getTargetBundle(), $this->entity->getViewMode(), $field_definition->getName());

        if (!empty($fieldDisplaySettings) && !empty($fieldDisplaySettings['view_mode'])) {
          $viewModeOptions['inherit'] = $this->t('Inherit from display');
        }

        // Add 'default' view mode option.
        $viewModeOptions['default'] = $this->t('Default');

        // Get available view modes for the target entity type of the field.
        $viewModes = $this->entityDisplayRepository->getViewModes($target_entity_type);
        if (!empty($viewModes)) {
          foreach ($viewModes as $viewModeId => $viewModeData) {
            $viewModeOptions[$viewModeId] = $viewModeData['label'];
          }
        }

        // Get view modes of target entity type.
        $elements['view_mode'] = [
          '#type' => 'select',
          '#title' => $this->t('View Mode'),
          '#default_value' => $this->getFieldSetting($field_definition, 'view_mode') ?? 'default',
          '#options' => $viewModeOptions,
          '#disabled' => count($viewModeOptions) === 1,
        ];
        break;
    }

    return $elements;
  }

  /**
   * Returns whether a field is set to auto preprocess.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Field definition of the field to check.
   *
   * @return bool
   *   Returns TRUE if the field is set to auto preprocess.
   */
  public function fieldIsSetToAutoPreprocess(FieldDefinitionInterface $field_definition): bool {
    // Get the target entity type of this field.
    $target_entity_type = $field_definition->getFieldStorageDefinition()->getSetting('target_type');

    // Check if this entity type is set to be automatically harmonized.
    $auto_harmonized_entities_in_config = \Drupal::service('harmonize')->config(HarmonizeConfig::MAIN)->get(HarmonizeConfig::MAIN__AUTO_HARMONIZED_ENTITIES);
    $auto_preprocess_in_config = in_array($target_entity_type, $auto_harmonized_entities_in_config, TRUE);

    // Get our rule configurations.
    $rules = \Drupal::service('harmonize')->config(HarmonizeConfig::ENTITY_RULES)->get(HarmonizeConfig::ENTITY_RULES__RULES);

    // Init.
    $auto_preprocess_in_bundle_rules = FALSE;
    $auto_preprocess_in_entity_rules = FALSE;

    // Config rules check.
    if (!empty($rules)) {
      // Convenience.
      $fieldName = $field_definition->getName();

      // Get the parent entity.
      $parentEntity = $this->entity->getTargetEntityTypeId();
      $parentBundle = $this->entity->getTargetBundle();

      // Get bundle rules if any.
      $bundleRules = $rules[$parentEntity]['bundles'][$parentBundle] ?? NULL;

      // Check if the field is set in the rules.
      if ($bundleRules && array_key_exists($fieldName, $bundleRules['rules'] ?? [])) {
        if (isset($bundleRules['rules'][$fieldName]['auto_harmonize'])) {
          $auto_preprocess_in_bundle_rules = $bundleRules['rules'][$fieldName]['auto_harmonize'];
        }
      }

      // Check entity rules.
      $entityRules = $rules[$parentEntity] ?? NULL;

      // Check if the field is set in the rules.
      if ($entityRules && array_key_exists($fieldName, $entityRules['rules'] ?? [])) {
        if (isset($entityRules['rules'][$fieldName]['auto_harmonize'])) {
          $auto_preprocess_in_entity_rules = $bundleRules['rules'][$fieldName]['auto_harmonize'] ?? FALSE;
        }
      }
    }

    return $auto_preprocess_in_config || $auto_preprocess_in_bundle_rules || $auto_preprocess_in_entity_rules;

  }

  /**
   * Build a summary of settings for a field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Field definition.
   *
   * @return array
   *   Summary array.
   */
  public function fieldSettingsSummary(FieldDefinitionInterface $field_definition): array {
    $summary = [];
    $summary[] = $this->t('Caching: <strong>@cache</strong>', ['@cache' => $this->getFieldSetting($field_definition, 'cache') ? 'Enabled' : 'Disabled']);

    // Get the field type.
    $type = $field_definition->getType();

    switch ($type) {
      case 'entity_reference':
      case 'entity_reference_revisions':
        $autoPreprocessEnabledInConfigOrRules = $this->fieldIsSetToAutoPreprocess($field_definition);
        $autoPreprocess = $autoPreprocessEnabledInConfigOrRules || $this->getFieldSetting($field_definition, 'auto_preprocess') ? 'Enabled' : 'Disabled';
        if ($autoPreprocessEnabledInConfigOrRules) {
          $autoPreprocess .= ' (' . $this->t('Overridden') . ')';
        }
        $summary[] = $this->t('Automatically Process Nested Entities: <strong>@auto_preprocess</strong>', ['@auto_preprocess' => $autoPreprocess]);
        $viewMode = $this->getFieldSetting($field_definition, 'view_mode');
        $definedViewModesInDrupal = $this->entityDisplayRepository->getViewModes($field_definition->getFieldStorageDefinition()->getSetting('target_type'));

        if ($viewMode === 'inherit') {
          $fieldDisplaySettings = \Drupal::service('harmonize')->helpers->getViewModeDisplaySettings($this->entity->getTargetEntityTypeId(), $this->entity->getTargetBundle(), $this->entity->getViewMode(), $field_definition->getName());
          if (!empty($fieldDisplaySettings['view_mode'])) {
            $viewModeLabel = $fieldDisplaySettings['view_mode'] === 'default' ? 'Default' : $definedViewModesInDrupal[$fieldDisplaySettings['view_mode']]['label'];
            $viewModeLabel .= ' (Inherited from display)';
          }
          else {
            $viewModeLabel = 'Default';
          }
        }
        else {
          if ($viewMode === 'default') {
            $viewModeLabel = $this->t('Default');
          }
          else {
            $viewModeLabel = $definedViewModesInDrupal[$viewMode]['label'];
          }
        }
        $summary[] = $this->t('View Mode: <strong>@view_mode</strong>', ['@view_mode' => $viewModeLabel]);
        break;
    }

    return $summary;
  }

  /**
   * Get a setting for a field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Field definition.
   * @param string $key
   *   The key of the field to retrieve settings for.
   *
   * @returns
   *   Returns the setting if found, otherwise return the default setting.
   */
  public function getFieldSetting(FieldDefinitionInterface $field_definition, string $key) {
    $settings = $this->entity->getComponent($field_definition->getName())['settings'] ?? NULL;
    return $settings[$key] ?? $this->fieldDefaultSettings($field_definition->getName(), $field_definition->getType())[$key];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // If the main "Save" button was submitted while a field settings subform
    // was being edited, update the new incoming settings when rebuilding the
    // entity, just as if the subform's "Update" button had been submitted.
    if ($edit_field = $form_state->get('field_settings_edit')) {
      $form_state->set('field_settings_update', $edit_field);
    }

    EntityForm::submitForm($form, $form_state);
    $form_values = $form_state->getValues();

    // Handle the 'display modes' checkboxes if present.
    if ($this->entity->getViewMode() == 'default' && !empty($form_values['display_modes_custom'])) {
      $display_modes = $this->getDisplayModes();
      $current_statuses = $this->getDisplayStatuses();

      $statuses = [];
      foreach ($form_values['display_modes_custom'] as $mode => $value) {
        if (!empty($value) && empty($current_statuses[$mode])) {
          // If no display exists for the newly enabled view mode, initialize
          // it with those from the 'default' view mode, which were used so
          // far.
          if (!$this->entityTypeManager->getStorage('entity_view_display')->load($this->entity->getTargetEntityTypeId() . '.' . $this->entity->getTargetBundle() . '.' . $mode)) {
            $display = $this->getEntityDisplay($this->entity->getTargetEntityTypeId(), $this->entity->getTargetBundle(), 'default')->createCopy($mode);
            $display->save();
            $settings = $this->getEntitySettings($this->entity->getTargetEntityTypeId(), $this->entity->getTargetBundle(), 'default')->createCopy($mode);
            $settings->save();
          }

          $display_mode_label = $display_modes[$mode]['label'];
          $url = $this->getOverviewUrl($mode);
          $this->messenger()->addStatus($this->t('The %display_mode mode now uses custom display settings. You might want to <a href=":url">configure them</a>.', [
            '%display_mode' => $display_mode_label,
            ':url' => $url->toString(),
          ]));
        }
        $statuses[$mode] = !empty($value);
      }

      $this->saveDisplayStatuses($statuses);
    }

    $this->messenger()->addStatus($this->t('Your settings have been saved.'));
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state): void {
    $form_values = $form_state->getValues();

    // Collect data for fields.
    foreach ($form['#fields'] as $field_name) {
      $values = $form_values['fields'][$field_name];

      if ($values['region'] == 'hidden') {
        $entity->removeComponent($field_name);
      }
      else {
        $options = $entity->getComponent($field_name);

        if (empty($options)) {
          $options = ['settings' => []];
        }

        // Update field settings only if the submit handler told us to.
        if ($form_state->get('field_settings_update') === $field_name) {
          // Only store settings actually used by the selected plugin.
          $default_settings = $this->fieldDefaultSettings($field_name, $values['type']);
          $options['settings'] = isset($values['settings_edit_form']['settings']) ? array_intersect_key($values['settings_edit_form']['settings'], $default_settings) : [];
          $form_state->set('field_settings_update', NULL);
        }

        // If the field is set but no settings exist, set default settings.
        if ($options['settings'] === []) {
          $options['settings'] = $this->fieldDefaultSettings($field_name, $values['type']);
        }

        $options['weight'] = $values['weight'];
        $options['region'] = $values['region'];
        // Only formatters have configurable label visibility.
        if (isset($values['label'])) {
          $options['label'] = $values['label'];
        }
        $entity->setComponent($field_name, $options);
      }
    }
  }

  /**
   * Form submission handler for multistep buttons.
   */
  public function multistepSubmit($form, FormStateInterface $form_state): void {
    $trigger = $form_state->getTriggeringElement();
    $op = $trigger['#op'];

    switch ($op) {
      case 'edit':
        // Store the field whose settings are currently being edited.
        $field_name = $trigger['#field_name'];
        $form_state->set('field_settings_edit', $field_name);
        break;

      case 'update':
        // Set the field back to 'non edit' mode, and update $this->entity with
        // the new settings from the next rebuild.
        $field_name = $trigger['#field_name'];
        $form_state->set('field_settings_edit', NULL);
        $form_state->set('field_settings_update', $field_name);
        $this->entity = $this->buildEntity($form, $form_state);
        break;

      case 'cancel':
        // Set the field back to 'non edit' mode.
        $form_state->set('field_settings_edit', NULL);
        break;

      case 'refresh_table':
        // If the currently edited field is one of the rows to be refreshed, set
        // it back to 'non edit' mode.
        $updated_rows = explode(' ', $form_state->getValue('refresh_rows'));
        $field_settings_edit = $form_state->get('field_settings_edit');
        if ($field_settings_edit && in_array($field_settings_edit, $updated_rows)) {
          $form_state->set('field_settings_edit', NULL);
        }
        break;
    }

    $form_state->setRebuild();
  }

  /**
   * Ajax handler for multistep buttons.
   */
  public function multistepAjax($form, FormStateInterface $form_state): array {
    $trigger = $form_state->getTriggeringElement();
    $op = $trigger['#op'];

    // Pick the elements that need to receive the ajax-new-content effect.
    switch ($op) {
      case 'edit':
        $updated_rows = [$trigger['#field_name']];
        $updated_columns = ['settings'];
        break;

      case 'update':
      case 'cancel':
        $updated_rows = [$trigger['#field_name']];
        $updated_columns = ['settings', 'settings_summary', 'settings_edit'];
        break;

      case 'refresh_table':
        $updated_rows = array_values(explode(' ', $form_state->getValue('refresh_rows')));
        $updated_columns = ['settings_summary', 'settings_edit'];
        break;
    }

    foreach ($updated_rows as $name) {
      foreach ($updated_columns as $key) {
        $element = &$form['fields'][$name][$key];
        $element['#prefix'] = '<div class="ajax-new-content">' . ($element['#prefix'] ?? '');
        $element['#suffix'] = ($element['#suffix'] ?? '') . '</div>';
      }
    }

    // Return the whole table.
    return $form['fields'];
  }

  /**
   * Returns entity (form) displays for the current entity display type.
   *
   * @return \Drupal\Core\Entity\Display\EntityDisplayInterface[]
   *   An array holding entity displays or entity form displays.
   */
  protected function getDisplays(): array {
    $load_ids = [];
    $entity_type = $this->entityTypeManager->getDefinition('entity_view_display');
    $config_prefix = $entity_type->getConfigPrefix();
    $ids = $this->configFactory()->listAll($config_prefix . '.' . $this->entity->getTargetEntityTypeId() . '.' . $this->entity->getTargetBundle() . '.');
    foreach ($ids as $id) {
      $config_id = str_replace($config_prefix . '.', '', $id);
      [, , $display_mode] = explode('.', $config_id);
      if ($display_mode != 'default') {
        $load_ids[] = $config_id;
      }
    }
    return $this->entityTypeManager->getStorage('entity_view_display')->loadMultiple($load_ids);
  }

  /**
   * Saves the updated display mode statuses.
   *
   * @param array $display_statuses
   *   An array holding updated form or view mode statuses.
   */
  protected function saveDisplayStatuses($display_statuses) {
    $displays = $this->getDisplays();
    foreach ($displays as $display) {
      // Only update the display if the status is changing.
      $new_status = $display_statuses[$display->get('mode')];
      if ($new_status !== $display->status()) {
        $display->set('status', $new_status);
        $display->save();
      }
    }
  }

}
