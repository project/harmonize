<?php

namespace Drupal\harmonize\Entity;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Builds the form to delete style entities.
 *
 * @noinspection PhpUnused
 */
class StyleDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() : TranslatableMarkup {
    return $this->t('Are you sure you want to delete %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() : Url {
    return new Url('entity.harmonize_style.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() : TranslatableMarkup {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      $this->entity->delete();
    }
    catch (EntityStorageException) {
      $this->messenger()->addError(
        $this->t('There was a problem deleting @label.',
          [
            '@label' => $this->entity->label(),
          ]
        )
      );
    }

    $this->messenger()->addStatus(
      $this->t('Deleted @label.',
        [
          '@label' => $this->entity->label(),
        ]
      )
    );

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
