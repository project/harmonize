<?php

namespace Drupal\harmonize\Entity;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\harmonize\Service\Helpers;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of Style entities.
 *
 * @noinspection PhpUnused
 */
class StyleListBuilder extends ConfigEntityListBuilder {

  /**
   * The config factory that knows what is overwritten.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The Harmonize Helper service.
   *
   * @var \Drupal\harmonize\Service\Helpers
   */
  protected Helpers $helpers;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) : StyleListBuilder {
    /* @noinspection PhpParamsInspection */
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('config.factory'),
      $container->get('harmonize.helpers'),
    );
  }

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\harmonize\Service\Helpers $helpers
   *   The Harmonize helper service.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    ConfigFactoryInterface $config_factory,
    Helpers $helpers
  ) {
    parent::__construct($entity_type, $storage);
    $this->configFactory = $config_factory;
    $this->helpers = $helpers;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() : array {
    $header['label'] = $this->t('Style');
    $header['id'] = $this->t('Machine Name');
    $header['fields'] = $this->t('Fields');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) : array {
    // Set the properties to show.
    $row['label'] = $entity->label();
    $row['id'] = new FormattableMarkup("<code>@machineName</code>", ['@machineName' => $entity->id()]);

    if ($entity instanceof StyleInterface) {
      $markup = "<ul>";
      foreach ($entity->getFields() as $field) {
        $markup .= "<li><code>$field</code></li>";
      }
      $markup .= "</ul>";
      $row['fields'] = new FormattableMarkup($markup, []);
    }

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() : array {
    // Perform parent build tasks.
    $build = parent::render();

    // Let's add a neat little section describing how to build styles in themes.
    $build['infos'] = [
      '#type' => 'container',
      '#open' => TRUE,
      '#prefix' => $this->t('<br><hr><br><h2>Defining Styles In Themes</h2><p>You can define styles by creating a <code>THEME_NAME.harmony.styles.yml</code> file at the root of a theme.<br>Copy the contents below to get started.</p>'),
    ];
    $build['infos']['example_yml'] = [
      '#type' => 'details',
      '#title' => $this->t("<strong>THEME_NAME.harmony.styles.yml Example</strong>"),
    ];

    $modulePath = \Drupal::service('extension.list.module')->getPath('harmonize');
    $exampleYml = file_get_contents($modulePath . "/files/THEME_NAME.harmony.styles.yml");

    $build['infos']['example_yml']['yml'] = [
      '#type' => 'textarea',
      '#value' => $exampleYml,
      '#rows' => 25,
    ];

    // Now let's add styles declared in themes.
    // Get current theme use config.
    $defaultThemeMachineName = $this->configFactory->get('system.theme')->get('default');

    // Get any theme config.
    $themeConfig = $this->helpers->getHarmonyThemeStylesConfig($defaultThemeMachineName);

    // Get styles definitions.
    $styles = $themeConfig['styles'] ?? NULL;

    // If we don't have styles, we can stop here.
    if (empty($styles)) {
      return $build;
    }

    // Now we build a second table with theme defined styles.
    $build['extras'] = [
      '#type' => 'container',
      '#open' => TRUE,
      '#prefix' => $this->t('<br><hr><br><h2>Theme Styles</h2><p>The styles below have been defined in a <code>THEME_NAME.harmony.styles.yml</code> file.</p>'),
    ];
    $build['extras'][$defaultThemeMachineName] = [
      '#type' => 'details',
      '#title' => $this->t("<strong>@theme</strong>", ['@theme' => $defaultThemeMachineName]),
      '#description' => $this->t("Below are styles defined in the <code>@fileName</code> file at the root of the <strong>@theme</strong> theme<br><br>", [
        '@fileName' => "$defaultThemeMachineName.harmony.yml",
        '@theme' => $defaultThemeMachineName,
      ]),
    ];
    $build['extras'][$defaultThemeMachineName]["table"] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Machine Name'),
        $this->t('Fields'),
      ],
      '#rows' => [],
    ];

    $conflicts = [];
    foreach ($styles as $style => $info) {
      if (empty($info['fields'])) {
        continue;
      }
      if (in_array($style, array_keys($build['table']['#rows']))) {
        $conflicts[] = $style;
      }
      $row['id'] = new FormattableMarkup("<code>@machineName</code>", ['@machineName' => $style]);
      $fields = $info['fields'];
      $markup = "<ul>";
      foreach ($fields as $field) {
        $markup .= "<li><code>$field</code></li>";
      }
      $markup .= "</ul>";
      $row['fields'] = new FormattableMarkup($markup, []);
      $build['extras'][$defaultThemeMachineName]['table']['#rows'][] = $row;
    }

    if (!empty($conflicts)) {
      $markup = $this->t("Be advised that the following styles are defined both in the default theme <strong><code>(@theme)</code></strong> and in our entities:<br>", ['@theme' => $defaultThemeMachineName]);
      $markup .= "<ul>";
      foreach ($conflicts as $style) {
        $markup .= "<li><code>$style</code></li>";
      }
      $markup .= "</ul>";
      $markup .= $this->t("Styles declared in the <strong>theme</strong> will take priority over styles of the same machine name created as entities.");
      \Drupal::messenger()->addWarning(new FormattableMarkup($markup, []));
    }

    return $build;
  }

}
