<?php

namespace Drupal\harmonize\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\harmonize\Service\Helpers;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a route controller for the autocomplete in Style edit forms.
 *
 * @noinspection PhpUnused
 */
class EntityFieldsAutoCompleteController extends ControllerBase {

  /**
   * Entity Type Manager Service from Drupal injected through DI.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity Field Manager Service from Drupal injected through DI.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  private EntityFieldManagerInterface $entityFieldManager;

  /**
   * The helpers service injected through DI.
   *
   * @var \Drupal\harmonize\Service\Helpers
   */
  protected Helpers $helpers;

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   EntityTypeManager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   EntityFieldManager service.
   * @param \Drupal\harmonize\Service\Helpers $helpers
   *   Harmonize Helpers service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityFieldManagerInterface $entityFieldManager, Helpers $helpers) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->helpers = $helpers;
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container) : EntityFieldsAutoCompleteController {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('harmonize.helpers')
    );
  }

  /**
   * Handler for the autocomplete request.
   *
   * @noinspection PhpUnused
   */
  public function handleAutocomplete(Request $request) : JsonResponse {
    // Initialize our results array.
    $results = [];

    // Get the input entered by the user.
    // If there's no input, we return empty results.
    $input = $request->query->get('q');
    if (!$input) {
      return new JsonResponse($results);
    }

    // Hand sanitizer.
    $input = Xss::filter($input);

    // Get all fields.
    $fields = $this->entityFieldManager->getFieldMap();

    // Since the above call gives us a map by entity type, we go through that
    // and simply get all field machine names.
    $machineNames = [];
    foreach ($fields as $map) {
      foreach ($map as $fieldMachineName => $info) {
        $machineNames[] = $fieldMachineName;
      }
    }

    // Now we loop through our machine names.
    $machineNames = array_unique($machineNames);
    foreach ($machineNames as $name) {
      if (str_starts_with($name, 'field_') && str_contains($name, $input)) {
        $results[] = [
          'value' => $name,
          'label' => $name,
        ];
      }
    }

    // Return our results wrapped in a JSON response.
    return new JsonResponse($results);
  }

}
