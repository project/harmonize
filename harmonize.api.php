<?php

/**
 * @file
 * Hooks for the harmonize module.
 */

/**
 * Allows modules & themes to alter Harmonizer suggestions.
 *
 * Suggestions control how Harmonizers load their data alterations objects.
 * Use modules to fine-tune or tweak these suggestions.
 *
 * Note that the array should be organized from least-specific to most
 * specific suggestions. The most specific ones are higher in priority.
 *
 * TYPE here refers to the harmonizer type. Possible values:
 *  - entity
 *  - field
 *  - region
 *  - form
 *  - menu
 *
 * @param array $suggestions
 *   The suggestions to alter.
 * @param \Drupal\harmonize\Harmonizer\HarmonizerInterface $harmonizer
 *   The harmonizer we are altering suggestions for.
 */
function hook_harmonize_suggestions_TYPE_alter(array &$suggestions, \Drupal\harmonize\Harmonizer\HarmonizerInterface $harmonizer) : void {
  $suggestions[] = $harmonizer->getType() . "." . $harmonizer->getDepth();
}

/**
 * Simple example of altering suggestions for Entity harmonizers.
 */
function hook_harmonize_suggestions_entity_alter(array &$suggestions, \Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityHarmonizerInterface $harmonizer) : void {
  // Obtain entity from our harmonizer.
  $entity = $harmonizer->getEntity();
  // Adds a suggestion that contains the language.
  // i.e. "entity.ENTITY_TYPE_ID.ID.LANG".
  // Add it at a specific spot to retain order.
  array_splice(
    $suggestions,
    // We add it after the suggestion "entity.ENTITY_TYPE_ID.ID".
    // After array_search, add + 1 to add it to the spot after the search.
    array_search($harmonizer->getType() . "." . $entity->getEntityTypeId() . "." . $entity->id(), $suggestions) + 1,
    0,
    $harmonizer->getType() . "." . $entity->getEntityTypeId() . "." . $entity->id() . "." . $entity->language()->getId()
  );
}
