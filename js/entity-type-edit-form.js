/**
 * @file
 */

(function ($, Drupal) {

  Drupal.behaviors.entityForm = {
    attach: function (context, settings) {
      // Display additional text in the vertical tab summary.
      $(context).find('.harmonize-entity-settings-form').drupalSetSummary(function (context) {
        let summaryParts = [];
        let $populateCheckbox = $('.harmonize-entity-settings-preprocess-enabled', context);
        if ($populateCheckbox.is(":checked")) {
          summaryParts.push('Preprocessing enabled');
        }
        if (summaryParts.length > 0) {
          return summaryParts.join(', ');
        }
      });

    }
  }

})(jQuery, Drupal);
