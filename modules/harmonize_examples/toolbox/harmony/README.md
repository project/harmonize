Use this directory if you are making use of the Harmonize Module's methodology.

You can place this directory at the root of the Drupal theme you are using.

Refer to the **Harmonize Examples** module to find examples of how to use this
folder properly! You can also always refer to the
[README](https://git.drupalcode.org/project/harmonize) for detailed information.

If the Harmonize Module is not being used, this directory can safely be
removed from your theme.
