<?php

namespace Drupal\harmonize_examples\Plugin\harmonize\Refiner\Extras;

use Drupal\harmonize\PluginManager\Refinery\RefinerBase;

/**
 * Provide plugin to alter harmonizer data.
 *
 * This will alter preprocessed data for all entities.
 *
 * This showcases the use of the 'themes' definition in the annotation below.
 * You can use this to only allow Refiners to execute in specific themes.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Refiner(
 *   id = "harmonize_examples.entity.refiner",
 *   target = "entity",
 *   weight = 0,
 *   themes = {
 *     "harmonize_examples"
 *   }
 * )
 *
 * @package Drupal\harmonize\Plugin\Harmonizer\Refiner
 *
 * @noinspection PhpUnused
 */
final class EntityRefinerForThemeExample extends RefinerBase {

  /**
   * Add personal tweaks to data in this function.
   *
   * {@inheritdoc}
   */
  public function refine(&$consignment, &$harmony, $object) : void {
    // Alter Harmonization Data here!
    $consignment['theme'] = 'yes!';
  }

}
