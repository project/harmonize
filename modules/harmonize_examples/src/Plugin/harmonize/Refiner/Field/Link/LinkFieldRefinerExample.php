<?php

namespace Drupal\harmonize_examples\Plugin\harmonize\Refiner\Field\Link;

use Drupal\Component\Utility\UrlHelper;
use Drupal\harmonize\PluginManager\Refinery\RefinerBase;

/**
 * Provide plugin to alter harmonizer data for all Link Type fields.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Refiner(
 *   id = "harmonize_examples.field.link.refiner",
 *   target = "field.link"
 * )
 *
 * @package Drupal\harmonize\Plugin\Harmonizer\Refiner
 *
 * @noinspection PhpUnused
 */
final class LinkFieldRefinerExample extends RefinerBase {

  /**
   * Add personal tweaks to data in this function.
   *
   * {@inheritdoc}
   */
  public function refine(&$consignment, &$harmony, $object): void {
    // Alter Harmonization Data here!
    // Add a modifier to tell us if the link is external or internal.
    $consignment['external'] = UrlHelper::isExternal($consignment['attributes']['href']);
  }

}
