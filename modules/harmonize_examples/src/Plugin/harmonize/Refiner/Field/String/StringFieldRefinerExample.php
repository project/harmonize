<?php

namespace Drupal\harmonize_examples\Plugin\harmonize\Refiner\Field\String;

use Drupal\harmonize\PluginManager\Refinery\RefinerBase;

/**
 * Provide plugin to alter harmonizer data for all String Type fields.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Refiner(
 *   id = "harmonize_examples.field_string_refiner",
 *   target = "field.string"
 * )
 *
 * @package Drupal\harmonize\Plugin\Harmonizer\Refiner
 *
 * @noinspection PhpUnused
 */
final class StringFieldRefinerExample extends RefinerBase {

  /**
   * Add personal tweaks to data in this function.
   *
   * {@inheritdoc}
   */
  public function refine(&$consignment, &$harmony, $object): void {
    // Alter Harmonization Data here!
    // Let's add some random text to the data.
    $consignment .= '+refined-string';
  }

}
