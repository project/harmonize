<?php

namespace Drupal\harmonize_examples\Plugin\harmonize\Refiner\Field\Date;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\harmonize_refinery\PluginManager\Refinery\RefinerBase;
use Drupal\harmonize\Service\Harmonize;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide plugin to alter harmonizer data for all Email Type fields.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Refiner(
 *   id = "harmonize_examples.field.datetime.refiner",
 *   target = "field.datetime"
 * )
 *
 * @package Drupal\harmonize\Plugin\Harmonizer\Refiner
 *
 * @noinspection PhpUnused
 */
final class DateTimeFieldRefinerExample extends RefinerBase {

  /**
   * The DateFormatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The Refiner base constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\harmonize\Service\Harmonize $harmonizeService
   *   The Harmonize service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    Harmonize $harmonizeService,
    DateFormatterInterface $dateFormatter
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $harmonizeService);
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) : DateTimeFieldRefinerExample {
    /* @noinspection PhpParamsInspection */
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('harmonize'),
      $container->get('date.formatter')
    );
  }

  /**
   * Add personal tweaks to data in this function.
   *
   * {@inheritdoc}
   */
  public function refine(&$consignment, &$harmony, $object): void {
    // Alter Harmonization Data here!
    // Let's format the date in a specific way.
    $consignment = $this->dateFormatter->format(strtotime($consignment), 'custom', 'M\. d', date_default_timezone_get());
  }

}
