<?php

namespace Drupal\harmonize_examples\Plugin\harmonize\Refiner\Entity;

use Drupal\harmonize\PluginManager\Refinery\RefinerBase;

/**
 * Provide plugin to alter harmonizer data.
 *
 * This will alter preprocessed data for all entities.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Refiner(
 *   id = "harmonize_examples.entity.refiner",
 *   target = "entity",
 *   weight = 0
 * )
 *
 * @package Drupal\harmonize\Plugin\Harmonizer\Refiner
 *
 * @noinspection PhpUnused
 */
final class EntityRefinerExample extends RefinerBase {

  /**
   * Add personal tweaks to data in this function.
   *
   * {@inheritdoc}
   */
  public function refine(&$consignment, &$harmony, $object) : void {
    // Alter Harmonization Data here!
    // For example, say you want to add the entity type ID to the data.
    /** @var \Drupal\Core\Entity\ContentEntityInterface $object */
    $consignment['entity_type_id'] = $object->getEntityTypeId();
  }

}
