<?php

namespace Drupal\harmonize_examples\Plugin\harmonize\Refiner\Entity\Node;

use Drupal\harmonize\PluginManager\Refinery\RefinerBase;

/**
 * Provide plugin to alter harmonizer data for all Pages.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Refiner(
 *   id = "harmonize_examples.entity.node.page.refiner",
 *   target = "entity.node.page",
 *   weight = 0
 * )
 *
 * @package Drupal\harmonize\Plugin\Harmonizer\Refiner
 *
 * @noinspection PhpUnused
 */
final class PageNodeEntityRefinerExample extends RefinerBase {

  /**
   * Add personal tweaks to data in this function.
   *
   * {@inheritdoc}
   */
  public function refine(&$consignment, &$harmony, $object) : void {
    // Alter Harmonization Data here!
    // For example, say you want to add the changed time to the data.
    /** @var \Drupal\node\NodeInterface $object */
    $consignment['changed_time_2'] = $object->getChangedTime();
  }

}
