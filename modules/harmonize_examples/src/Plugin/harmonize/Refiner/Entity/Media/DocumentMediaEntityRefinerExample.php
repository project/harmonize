<?php

namespace Drupal\harmonize_examples\Plugin\harmonize\Refiner\Entity\Media;

use Drupal\harmonize\PluginManager\Refinery\RefinerBase;

/**
 * Provide plugin to alter harmonizer data for an entity.
 *
 * Alters data for Media entities of type 'document'.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Refiner(
 *   id = "harmonize_examples.entity.media.document.refiner",
 *   target = "entity.media.document",
 *   weight = 0
 * )
 *
 * @package Drupal\harmonize\Plugin\Harmonizer\Refiner
 */
final class DocumentMediaEntityRefinerExample extends RefinerBase {

  /**
   * Add personal tweaks to harmonized data in this function.
   *
   * {@inheritdoc}
   */
  public function refine(&$consignment, &$harmony, $object): void {
    // Alter Harmonization Data here!
    // Here is an example of altering the data to make it more accessible.
    $harmony = [
      'title' => $consignment['media_title'],
      'uri' => $consignment['field_media_document']['uri'],
      'src' => $consignment['field_media_document']['href'] ?? '',
      'extension' => !empty($consignment['field_media_document']['href']) ? pathinfo($consignment['field_media_document']['href'])['extension'] : NULL,
    ];
  }

}
