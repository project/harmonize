<?php

namespace Drupal\harmonize_examples\Plugin\harmonize\Refiner\Entity\Media;

use Drupal\harmonize\PluginManager\Refinery\RefinerBase;

/**
 * Provide plugin to alter harmonizer data for an entity.
 *
 * Alters data for Media entities of type 'image'.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Refiner(
 *   id = "harmonize_examples.entity.media.image.refiner",
 *   target = "entity.media.image",
 *   weight = 0
 * )
 *
 * @package Drupal\harmonize\Plugin\Harmonizer\Refiner
 *
 * @noinspection PhpUnused
 */
final class ImageMediaEntityRefinerExample extends RefinerBase {

  /**
   * Add personal tweaks to data in this function.
   *
   * {@inheritdoc}
   */
  public function refine(&$consignment, &$harmony, $object): void {
    // Shortcut to access data from the 'field_media_image' field.
    // This basically reduces the nesting needed to access the data we want.
    $harmony = [
      // We add this to specify the image type we're dealing with.
      'type' => 'image',

      // The data from the actual image, found in the 'field_media_image' field.
      'uri' => $consignment['field_media_image']['uri'] ?? '',
      'src' => $consignment['field_media_image']['attributes']['src'] ?? '',
      'attributes' => $consignment['field_media_image']['attributes'] ?? [],
      'alt' => $consignment['field_media_image']['attributes']['alt'] ?? '',
    ];
  }

}
