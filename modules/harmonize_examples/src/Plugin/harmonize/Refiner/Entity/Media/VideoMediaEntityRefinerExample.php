<?php

namespace Drupal\harmonize_examples\Plugin\harmonize\Refiner\Entity\Media;

use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\harmonize_refinery\PluginManager\Refinery\RefinerBase;
use Drupal\harmonize\Service\Harmonize;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide plugin to alter harmonizer data for an entity.
 *
 * Alters data for Media entities of type 'video'.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Refiner(
 *   id = "harmonize_examples.entity.media.video.refiner",
 *   target = "entity.media.video",
 *   weight = 0
 * )
 *
 * @package Drupal\harmonize\Plugin\Harmonizer\Refiner
 */
final class VideoMediaEntityRefinerExample extends RefinerBase {

  /**
   * The FileUrlGenerator service.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected FileUrlGeneratorInterface $fileUrlGenerator;

  /**
   * The Refiner base constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\harmonize\Service\Harmonize $harmonizeService
   *   The Harmonize service.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    Harmonize $harmonizeService,
    FileUrlGeneratorInterface $fileUrlGenerator
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $harmonizeService);
    $this->fileUrlGenerator = $fileUrlGenerator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) : VideoMediaEntityRefinerExample {
    /* @noinspection PhpParamsInspection */
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('harmonize'),
      $container->get('file_url_generator')
    );
  }

  /**
   * Add personal tweaks to harmonized data in this function.
   *
   * {@inheritdoc}
   */
  public function refine(&$consignment, &$harmony, $object): void {
    $harmony = [
      'type' => 'video',
      'src' => $this->fileUrlGenerator->transformRelative($this->fileUrlGenerator->generateAbsoluteString($consignment['field_media_video_file']['uri'])),
    ];
  }

}
