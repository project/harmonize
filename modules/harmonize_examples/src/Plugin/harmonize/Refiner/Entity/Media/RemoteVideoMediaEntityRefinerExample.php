<?php

namespace Drupal\harmonize_examples\Plugin\harmonize\Refiner\Entity\Media;

use Drupal\harmonize\PluginManager\Refinery\RefinerBase;

/**
 * Provide plugin to alter harmonizer data for an entity.
 *
 * Alters data for Media entities of type 'remote_video'.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Refiner(
 *   id = "harmonize_examples.entity.media.remote_video.refiner",
 *   target = "entity.media.remote_video",
 *   weight = 0
 * )
 *
 * @package Drupal\harmonize\Plugin\Harmonizer\Refiner
 */
final class RemoteVideoMediaEntityRefinerExample extends RefinerBase {

  /**
   * Add personal tweaks to harmonized data in this function.
   *
   * {@inheritdoc}
   */
  public function refine(&$consignment, &$harmony, $object) : void {
    $videoUrl = $consignment['field_media_oembed_video'];
    $videoProvider = $this->extractRemoteVideoProviderFromUrl($videoUrl);
    $videoId = $this->extractRemoteVideoIdFromUrl($videoProvider, $videoUrl);

    $harmony = [
      'type' => 'remote_video',
      'src' => $videoId,
      'provider' => $videoProvider,
      'id' => $videoId,
      'url' => $videoUrl,
    ];
  }

  /**
   * Detect if a video is either from YouTube or Vimeo.
   *
   * @param string $url
   *   Absolute URL leading to the video itself.
   *
   * @return string|null
   *   The name of the provider found.
   */
  private function extractRemoteVideoProviderFromUrl(string $url): ?string {
    $youtubeRegex = '/^(https?:\/\/)?(www\.youtube\.com|youtu\.?be)\/.+$/';
    $vimeoRegex = '/vimeo\.com\/\d+/';

    if (preg_match($youtubeRegex, $url)) {
      return 'youtube';
    }

    if (preg_match($vimeoRegex, $url)) {
      return 'vimeo';
    }

    return 'unknown';
  }

  /**
   * Extract a YouTube or Vimeo ID from their URL.
   *
   * @param string $provider
   *   Either YouTube or Vimeo.
   * @param string $url
   *   Absolute URL leading to the video itself.
   *
   * @return string|null
   *   The video unique identifier.
   */
  private function extractRemoteVideoIdFromUrl(string $provider, string $url): ?string {
    switch ($provider) {
      case 'youtube':
        $regex
          = '/^.*(?:youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*$/i';
        break;

      case 'vimeo':
        $regex
          = '/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/[^\/]*\/videos\/|album\/\d+\/video\/|video\/|)(\d+)(?:[a-zA-Z\d_\-]+)?/i';
        break;

      default:
        return NULL;
    }

    preg_match($regex, $url, $matches);
    return $matches[1] ?? NULL;
  }

}
