# Harmony Files Directory

Welcome to the Harmony Files Directory. Here, you can manage alterations to the
preprocessed data that Harmonize returns to your `$variables` array and subsequently
your Twig templates.

This folder should already contain files you can use and/or base yourself on. The functionality
is self-explanatory as you view these files. To add to this, documentation has been added as well
to aid you in your preprocessing endeavours.

You will find that this folder works quite similarly to the `templates` folder in your theme.

## Configurations

Configurations for Harmony Files can be found in the website's admin interface. You can visit
`/admin/config/harmonize/harmony-files` to adjust some settings.

Mainly, you can change the file extension of the files that will be loaded. By default, the module will load all
files with the `.harmony` extension.

## Pre-Existing Alterations

Please note that the example files found here certainly do work on your website. These out of the box alterations serve
two purposes:

* To show examples of what can be done.
* To provide some baseline alterations that make data organization cleaner out of the box.

While some pre-existing alterations exist, a large majority of the files here contain no specific alterations and serve as
example files to guide developers in the right direction. They can safely be removed with no worries.

## Functions.php

The `functions.php` file can be used to declare utility functions that are re-usable across all
harmony files.

There are examples of this in the files as well.

## Suggestions

Suggestions can be used when creating harmony files. This is similar to how Template Suggestions work.

For example, as seen in this directory, a file called `entity.node.page.harmony` can be used to alter data for
a `node` entity of bundle `page`.

Additional data such as the **View Mode** or the **Node ID** can also be implemented. For each type of harmonizer,
there are different possible suggestions.

### Default Suggestions

#### Entities

* `entity`
* `entity.ENTITY_TYPE_ID` - i.e. `entity.node`
* `entity.ENTITY_TYPE_ID.ENTITY_ID` - i.e. `entity.node.123`
* `entity.ENTITY_TYPE_ID.BUNDLE` - i.e. `entity.node.basic_page`
* `entity.ENTITY_TYPE_ID.BUNDLE.ENTITY_ID` - i.e. `entity.node.basic_page.123`
* `entity.ENTITY_TYPE_ID.VIEW_MODE` - i.e. `entity.node.teaser`
* `entity.ENTITY_TYPE_ID.VIEW_MODE.ENTITY_ID` - i.e. `entity.node.teaser.123`
* `entity.ENTITY_TYPE_ID.BUNDLE.VIEW_MODE` - i.e. `entity.node.basic_page.teaser`
* `entity.ENTITY_TYPE_ID.BUNDLE.VIEW_MODE.ENTITY_ID` - i.e. `entity.node.basic_page.teaser.123`

#### Fields

* `field`
* `field.FIELD_TYPE`
* `field.FIELD_TYPE.FIELD_NAME`
* `field.FIELD_TYPE.FIELD_NAME.TARGET_ENTITY_TYPE_ID`
* `field.FIELD_TYPE.FIELD_NAME.TARGET_ENTITY_TYPE_ID.TARGET_BUNDLE`

### Custom Suggestions

Using hooks, your modules and themes can alter template suggestions per entity type.

See `harmonize.api.php` for details on how to accomplish this. Below are examples of a hook that can be used:

```php
<?php
/**
 * Simple example of altering suggestions for Entity harmonizers.
 */
function YOUR_THEME_harmonize_suggestions_entity_alter(array &$suggestions, \Drupal\harmonize\Harmonizer\EntityHarmonizer\EntityHarmonizerInterface $harmonizer) : void {
  // Obtain entity from our harmonizer.
  $entity = $harmonizer->getEntity();
  // Adds a suggestion that contains the language.
  // i.e. "entity.ENTITY_TYPE_ID.ID.LANG".
  // Add it at a specific spot to retain order.
  array_splice(
    $suggestions,
    // We add it after the suggestion "entity.ENTITY_TYPE_ID.ID".
    // After array_search, add + 1 to add it to the spot after the search.
    array_search($harmonizer->getType() . "." . $entity->getEntityTypeId() . "." . $entity->id(), $suggestions) + 1,
    0,
    $harmonizer->getType() . "." . $entity->getEntityTypeId() . "." . $entity->id() . "." . $entity->language()->getId()
  );
}
```

## Services & Dependency Injection

_Coming soon!_
