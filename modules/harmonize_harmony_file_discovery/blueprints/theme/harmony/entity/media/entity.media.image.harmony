<?php

/**
 * @file
 * Override & customize data harmonizations for a given Drupal object or entity.
 *
 * @harmonizerType entity
 * @entityType media
 * @bundle image
 *
 * This is a custom file to handle the reformatting of data before it reaches
 * Drupal templates.
 *
 * Available variables:
 * @var array $consignment
 *    The data array (modify in place).
 *    This is comparable to the '$variables' array in preprocess functions.
 * @var array $harmony
 *    The final data that CAN be returned to the Twig template.
 *    Usage of this variable is optional, and it is empty by default.
 *    If this is left empty, then the data in $consignment will be returned.
 * @var \Drupal\media\MediaInterface $object
 *    The original object being harmonized. Sometimes you want to access the
 *    original Drupal object if Harmonize does not provide certain information.
 * @var array $suggestions
 *    File suggestions that can be used in this context.
 *    Dump this variable to find out what other suggestions you can use for this harmonizer.
 *
 * You have the choice to either modify the $consignment array which contains
 * the default data, or assign new data to the empty $harmony variable. If
 * $harmony is empty by the end of this file, $consignment will be sent to
 * templates. Otherwise, $harmony will be sent to templates.
 */

// Shortcut to access data from the 'field_media_image' field.
// This basically reduces the nesting needed to access the data we want.
$harmony = [
  // We add this to specify the image type we're dealing with.
  'type' => 'image',

  // The data from the actual image, found in the 'field_media_image' field.
  'uri' => $consignment['field_media_image']['uri'] ?? '',
  'src' => $consignment['field_media_image']['attributes']['src'] ?? '',
  'attributes' => $consignment['field_media_image']['attributes'] ?? [],
  'alt' => $consignment['field_media_image']['attributes']['alt'] ?? '',
];
