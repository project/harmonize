<?php

use Drupal\Core\Url;
use Drupal\image\Entity\ImageStyle;
use Drupal\responsive_image\Entity\ResponsiveImageStyle;

/**
 * @file
 * This is simply a file to declare reusable functions in your .harmony files.
 */

/**
 * Get an image source, generated in a specified image style.
 *
 * @param string $uri
 *   URI of the original image.
 * @param string $imageStyleMachineName
 *   Machine name of the image style to use.
 *
 * @return string
 *   The path to the image generated in the appropriate style. Returns the original image otherwise.
 */
function get_image_in_style(string $uri, string $imageStyleMachineName): string {
  // Get image style storage.
  $imageStyle = ImageStyle::load($imageStyleMachineName);

  if ($imageStyle === NULL) {
    \Drupal::messenger()->addError("The image style $imageStyleMachineName doesn't exist.");
  }
  else {
    return $imageStyle->buildUrl($uri);
  }

  return Url::fromUri($uri)->toString();
}

/**
 * Get responsive image info for an image in a given responsive image style.
 *
 * @param string $uri
 *   Original image URI.
 * @param string $risMachineName
 *   Responsive image style machine name.
 *
 * @return array
 *   Array of responsive image information. Returns an empty array otherwise.
 */
function get_responsive_image_info(string $uri, string $risMachineName) : array {
  // Get image style storage.
  $responsiveImageStyle = ResponsiveImageStyle::load($risMachineName);

  if ($responsiveImageStyle === NULL) {
    \Drupal::messenger()->addError("The responsive image style $risMachineName doesn't exist.");
  }
  else {
    $data = [];
    $data['sources'] = [];
    $breakpoints = array_reverse(\Drupal::service('breakpoint.manager')->getBreakpointsByGroup($responsiveImageStyle->getBreakpointGroup()));
    foreach ($responsiveImageStyle->getImageStyleMappings() as $mapping) {
      $breakpointId = $mapping['breakpoint_id'];
      /* @var \Drupal\breakpoint\Breakpoint $breakpoint */
      $breakpoint = $breakpoints[$breakpointId];
      $multiplier = $mapping['multiplier'];
      switch ($mapping['image_mapping_type']) {
        case 'image_style': {
          $data['breakpoints'][$breakpointId . '.' . $multiplier]['type'] = 'style';
          $data['breakpoints'][$breakpointId . '.' . $multiplier]['srcset'] = get_image_in_style($uri, $mapping['image_mapping']);
          $data['breakpoints'][$breakpointId . '.' . $multiplier]['media'] = $breakpoint->getMediaQuery();
          $data['sources'][] = [
            'media' => $breakpoint->getMediaQuery(),
            'srcset' => get_image_in_style($uri, $mapping['image_mapping']),
          ];
          break;
        }
        case 'sizes': {
          $data['breakpoints'][$breakpointId . '.' . $multiplier]['type'] = 'sizes';
          $data['breakpoints'][$breakpointId . '.' . $multiplier]['sizes'] = $mapping['image_mapping']['sizes'];
          $data['breakpoints'][$breakpointId . '.' . $multiplier]['sizes'] = str_replace("\r\n", ', ', $data['breakpoints'][$breakpointId . '.' . $multiplier]['sizes']);
          $data['breakpoints'][$breakpointId . '.' . $multiplier]['srcset'] = [];
          foreach ($mapping['image_mapping']['sizes_image_styles'] as $style) {
            $imageSrc = get_image_in_style($uri, $style);
            $styleConfig = (ImageStyle::load($style)->getEffects()->getConfiguration());

            foreach ($styleConfig as $config) {
              if (isset($config['data'])) {
                if (isset($config['data']['width'])) {
                  $imageStyleWidth = $config['data']['width'];
                }
              }
            }
            $data['breakpoints'][$breakpointId . '.' . $multiplier]['srcset'][] = $imageSrc . ' ' . $imageStyleWidth . 'w';
          }
          $data['breakpoints'][$breakpointId . '.' . $multiplier]['srcset'] = implode(', ', $data['breakpoints'][$breakpointId . '.' . $multiplier]['srcset']);
          break;
        }
      }
    }
    return $data;
  }

  return [];
}

/**
 * Detect if a video is either from YouTube or Vimeo.
 *
 * @param string $url
 *   Absolute URL leading to the video itself.
 *
 * @return string|null
 *   The name of the provider found.
 */
function extract_remote_video_provider_from_url(string $url): ?string {
  $youtubeRegex = '/^(https?:\/\/)?(www\.youtube\.com|youtu\.?be)\/.+$/';
  $vimeoRegex = '/vimeo\.com\/\d+/';

  if (preg_match($youtubeRegex, $url)) {
    return 'youtube';
  }

  if (preg_match($vimeoRegex, $url)) {
    return 'vimeo';
  }

  return 'unknown';
}

/**
 * Extract a YouTube or Vimeo ID from their URL.
 *
 * @param string $provider
 *   Either YouTube or Vimeo.
 * @param string $url
 *   Absolute URL leading to the video itself.
 *
 * @return string|null
 *   The video unique identifier.
 */
function extract_remote_video_id_from_url(string $provider, string $url): ?string {
  switch ($provider) {
    case 'youtube':
      $regex
        = '/^.*(?:youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*$/i';
      break;

    case 'vimeo':
      $regex
        = '/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/[^\/]*\/videos\/|album\/\d+\/video\/|video\/|)(\d+)(?:[a-zA-Z\d_\-]+)?/i';
      break;

    default:
      return NULL;
  }

  preg_match($regex, $url, $matches);
  return $matches[1] ?? NULL;
}
