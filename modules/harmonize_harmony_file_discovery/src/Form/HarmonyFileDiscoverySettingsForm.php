<?php

namespace Drupal\harmonize_harmony_file_discovery\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\harmonize_harmony_file_discovery\Constants\HarmonyFileDiscoveryConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Provides a configuration form for the Harmony Files module.
 *
 * @package Drupal\harmonize\Form
 *
 * @noinspection PhpUnused
 */
class HarmonyFileDiscoverySettingsForm extends ConfigFormBase {
  use MessengerTrait;

  /**
   * The Theme Manager service injected through DI.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected ThemeManagerInterface $themeManager;

  /**
   * The Theme Extension List service injected through DI.
   *
   * @var \Drupal\Core\Extension\ThemeExtensionList
   */
  protected ThemeExtensionList $themeExtensionList;

  /**
   * The Module Extension List service injected through DI.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected ModuleExtensionList $moduleExtensionList;

  /**
   * The File System service injected through DI.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   ConfigFactory service.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   *   Theme Manager service.
   * @param \Drupal\Core\Extension\ThemeExtensionList $themeExtensionList
   *   Theme Extension List service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   Module Extension List service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File System service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    ThemeManagerInterface $themeManager,
    ThemeExtensionList $themeExtensionList,
    ModuleExtensionList $moduleExtensionList,
    FileSystemInterface $fileSystem
  ) {
    parent::__construct($configFactory);
    $this->themeManager = $themeManager;
    $this->themeExtensionList = $themeExtensionList;
    $this->moduleExtensionList = $moduleExtensionList;
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container) : HarmonyFileDiscoverySettingsForm {
    return new static(
      $container->get('config.factory'),
      $container->get('theme.manager'),
      $container->get('extension.list.theme'),
      $container->get('extension.list.module'),
      $container->get('file_system'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'harmonize_harmony_file_discovery_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() : array {
    return [
      HarmonyFileDiscoveryConfig::MAIN,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) : array {
    // Set this for better organization of form values later.
    $form['#tree'] = TRUE;

    // Get system theme config.
    $systemThemeConfig = $this->configFactory->get('system.theme');
    $defaultTheme = $systemThemeConfig->get('default');
    $defaultThemePath = $this->themeExtensionList->getPath($defaultTheme);
    $harmonyDirectoryPath = $defaultThemePath . '/harmony';

    // If the Harmony Directory does not exist, we throw a warning.
    if (!file_exists($harmonyDirectoryPath)) {
      $this->messenger()->addError($this->t("The <code>harmony</code> directory does not exist in your site's default theme (<code>@defaultTheme</code>). <br>Use the button below to generate the directory and start altering your preprocessed data.", [
        '@defaultTheme' => $defaultTheme,
      ]));

      $form['generate'] = [
        '#type' => 'submit',
        '#name' => 'generate',
        '#value' => $this->t('Generate Harmony Files Directory'),
        '#attributes' => [
          'class' => [
            'button',
            'button--primary',
          ],
        ],
        '#submit' => [
          '::generateHarmonyFilesDirectory',
        ],
      ];
    }

    // Get the current configurations.
    $fileExtension = $this->config(HarmonyFileDiscoveryConfig::MAIN)->get(
      HarmonyFileDiscoveryConfig::MAIN__HARMONY_FILE_EXTENSION
    );

    // Add a field for the file extension..
    $form[HarmonyFileDiscoveryConfig::MAIN__HARMONY_FILE_EXTENSION] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('File Extension'),
      '#description'   => $this->t('Customize the file extension for harmony files in your theme.'),
      '#default_value' => !empty($fileExtension) ? $fileExtension : HarmonyFileDiscoveryConfig::MAIN__HARMONY_FILE_EXTENSION__DEFAULT,
    ];

    // Return the form with all necessary fields.
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) : void {
    // Retrieve the configuration.
    $values = $form_state->getValues();

    $this->configFactory->getEditable(HarmonyFileDiscoveryConfig::MAIN)
      ->set(HarmonyFileDiscoveryConfig::MAIN__HARMONY_FILE_EXTENSION, $values[HarmonyFileDiscoveryConfig::MAIN__HARMONY_FILE_EXTENSION])
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Custom submission handler to generate directory.
   *
   * @param array $form
   *   Render array of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function generateHarmonyFilesDirectory(array &$form, FormStateInterface $form_state) : void {
    $systemThemeConfig = $this->configFactory->get('system.theme');
    $defaultTheme = $systemThemeConfig->get('default');
    $defaultThemePath = $this->themeExtensionList->getPath($defaultTheme);
    $harmonyDirectoryPath = $defaultThemePath . '/harmony';
    $modulePath = $this->moduleExtensionList->getPath('harmonize_harmony_file_discovery');
    $harmonyDirectoryBlueprints = $modulePath . '/blueprints/theme/harmony';

    // We'll use the Symfony file system here.
    $symfonyFs = new Filesystem();

    $symfonyFs->mirror($harmonyDirectoryBlueprints, $harmonyDirectoryPath);

    $this->messenger()->addMessage($this->t("The <code>harmony</code> directory has been created in your site's default theme (<code>@defaultTheme</code>).", [
      '@defaultTheme' => $defaultTheme,
    ]));
  }

}
