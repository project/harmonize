<?php

namespace Drupal\harmonize_harmony_file_discovery\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\harmonize\Constants\HarmonizationEvents;
use Drupal\harmonize\Constants\HarmonizeConfig;
use Drupal\harmonize\Constants\HarmonizerFlags;
use Drupal\harmonize\Event\HarmonizationEvent;
use Drupal\harmonize\Harmonizer\HarmonizerInterface;
use Drupal\harmonize\Service\Harmonize;
use Drupal\harmonize_harmony_file_discovery\Constants\HarmonyFileDiscoveryConfig;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes to events to apply modifications of harmonization data.
 *
 * @package Drupal\custom_events\EventSubscriber
 *
 * @noinspection PhpUnused
 */
class HarmonyEventSubscriber implements EventSubscriberInterface {
  use StringTranslationTrait;

  /**
   * Static variable to track if functions.php file was included.
   *
   * We only want to include it once per page.
   *
   * @var bool
   */
  private static bool $functionsFileIncluded = FALSE;

  /**
   * Static variable to track if inject.yml file was loaded.
   *
   * We only want to load it once per page.
   *
   * @var bool
   */
  private static bool $servicesInjected = FALSE;

  /**
   * Define the path that will be crawled in the theme for Handshake files.
   *
   * This is the path relative to the root of the Drupal theme.
   *
   * @var string
   */
  public const ROOT = '/harmony';

  /**
   * The Theme Manager service injected through DI.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  private ThemeManagerInterface $themeManager;

  /**
   * The Theme Extension List service injected through DI.
   *
   * @var \Drupal\Core\Extension\ExtensionList
   */
  private ExtensionList $themeExtensionList;

  /**
   * The Config Factory service injected through DI.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private ConfigFactoryInterface $configFactory;

  /**
   * A flag that determines whether we want to stop including files.
   *
   * @var bool
   */
  private bool $break = FALSE;

  /**
   * A flag that determines whether we found a file.
   *
   * @var bool
   */
  private bool $loadedFile = FALSE;

  /**
   * HarmonyEventSubscriber constructor.
   *
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   *   Theme Manager injected through DI.
   * @param \Drupal\Core\Extension\ExtensionList $themeExtensionList
   *   Drupal's Theme Extension List service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config Factory injected through DI.
   */
  public function __construct(
    ThemeManagerInterface $themeManager,
    ExtensionList $themeExtensionList,
    ConfigFactoryInterface $configFactory
  ) {
    $this->themeManager = $themeManager;
    $this->themeExtensionList = $themeExtensionList;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
   */
  public static function getSubscribedEvents() : array {
    return [
      HarmonizationEvents::CORE => ['onHarmonize', 100],
    ];
  }

  /**
   * React to a Harmonizer event.
   *
   * @param \Drupal\harmonize\Event\HarmonizationEvent $event
   *   Event that occurred, containing the harmonizer that fired the event.
   *
   * @noinspection PhpUnused
   */
  public function onHarmonize(HarmonizationEvent $event) : void {
    // Reset our trackers.
    $this->break = FALSE;
    $this->loadedFile = FALSE;

    $harmony = [];
    $consignment = $event->harmonizer->getData();
    $object = $event->harmonizer->getObject();

    // The default theme for this is the active one.
    $theme = $this->themeManager->getActiveTheme()->getName();

    // If the THEME flag is set, we want to change the directory.
    // Note that the site WILL crash if an invalid theme is given.
    // We want this to happen. :)
    if ($event->harmonizer->hasFlag(HarmonizerFlags::THEME) && !empty($event->harmonizer->getFlag(HarmonizerFlags::THEME))) {
      $theme = $event->harmonizer->getFlag(HarmonizerFlags::THEME);
    }

    // Trace.
    $event->harmonizer->trace($this->t("[Harmony Event] Theme has been resolved as <strong><code>[@theme]</code></strong>", ['@theme' => $theme]));

    // Set our directory.
    $directory = $this->themeExtensionList->getPath($theme) . self::ROOT;

    // Load 'functions.php' if it exists.
    if (file_exists($directory . '/functions.php') && !HarmonyEventSubscriber::$functionsFileIncluded) {
      include_once $directory . '/functions.php';
      HarmonyEventSubscriber::$functionsFileIncluded = TRUE;
    }

    // If 'FCFS' is set, we only want to get the first harmony file we find.
    if ($this->isFcfs()) {
      $event->harmonizer->trace($this->t("[Harmony Event] <strong>FCFS</strong> Algorithm Detected...Only a single harmony file will be loaded"));
    }

    // Get loading suggestions from the harmonizer.
    $suggestions = $event->harmonizer->getCachedSuggestions();

    // We reverse the array here.
    // FCFS should load the most specific suggestion first.
    // Granted, devs must make sure suggestions are organized properly.
    $suggestions = array_reverse($suggestions);

    // With each part of the signature, include files matching the part.
    foreach ($suggestions as $suggestion) {
      // Include all files we can find.
      $this->includeAll(
        $directory, $suggestion, $event->harmonizer, $consignment, $harmony, $object, $suggestions
      );

      // If this is set to break, we can stop.
      if ($this->break) {
        break;
      }
    }

    // If no files were loaded, we can stop here.
    if (!$this->loadedFile) {
      $event->harmonizer->trace($this->t("[Harmony Event] No harmony files were found. Data will be returned in its current state"), TRUE);
    }
  }

  /**
   * Scan the path, recursively including all PHP files.
   *
   * @param string $dir
   *   Directory to look through to include files.
   * @param string $filename
   *   Filename to look for recursively.
   * @param \Drupal\harmonize\Harmonizer\HarmonizerInterface $harmonizer
   *   Data from the harmonizer, already processed by default.
   * @param mixed $consignment
   *   Data from the harmonizer, already processed by default.
   * @param mixed $harmony
   *   Data of the harmonizer that will be released to templates.
   * @param mixed $object
   *   The object being harmonized.
   * @param array $suggestions
   *   Additional suggestions that can be used for the harmonizer.
   *
   * @noinspection PhpMissingParamTypeInspection
   */
  private function includeAll(
    string $dir,
    string $filename,
    HarmonizerInterface $harmonizer,
    &$consignment,
    &$harmony,
    &$object,
    $suggestions
  ) : void {
    if ($this->break) {
      return;
    }
    $escaped_filename = str_replace('.', '\.', $filename);
    $file_extension = $this->configFactory->get(HarmonyFileDiscoveryConfig::MAIN)->get(HarmonyFileDiscoveryConfig::MAIN__HARMONY_FILE_EXTENSION) ?? HarmonyFileDiscoveryConfig::MAIN__HARMONY_FILE_EXTENSION__DEFAULT;
    $escaped_file_extension = str_replace(
      '.', '\.', $file_extension
    );

    $scan = glob("$dir/*");

    foreach ($scan as $path) {
      $test = '/\/' . $escaped_filename . $escaped_file_extension . '$/';
      if (preg_match($test, $path)) {
        // Include the file.
        include $path;

        // Confirm that we loaded at least one file.
        $this->loadedFile = TRUE;

        // Trace.
        $harmonizer->trace($this->t("[Harmony Event] Harmony file found: <strong><code>@file</code></strong> has been loaded and data alterations have been applied", ['@file' => $path]));

        // If the harmony object is empty, that means it wasn't reformatted.
        // We'll return the original consignment.
        if (!empty($harmony)) {
          $harmonizer->setData($harmony);
          $harmonizer->trace($this->t("[Harmony Event] <strong><code>\$harmony</code></strong> variable was detected...Data has been overwritten with the contents of the variable"), TRUE);
        }
        else {
          $harmonizer->setData($consignment);
          $harmonizer->trace($this->t("[Harmony Event] Data was modified and returned with alterations from files"), TRUE);
        }

        // If we're in FCFS mode, we want to stop after one include.
        if ($this->isFcfs()) {
          $harmonizer->trace($this->t("[Harmony Event] Due to <strong>FCFS</strong>, no other harmony files will be loaded. Proceeding", [
            '@file' => $path,
          ]));
          $this->break = TRUE;
        }
      }
      elseif (is_dir($path)) {
        $this->includeAll($path, $filename, $harmonizer, $consignment, $harmony, $object, $suggestions);
      }
    }
  }

  /**
   * Helper function to tell us if we're in 'FCFS' mode.
   *
   * @return bool
   *   Returns TRUE if we're in FCFS mode, returns FALSE otherwise.
   */
  private function isFcfs() : bool {
    return Harmonize::$config[HarmonizeConfig::MAIN__LOADING_ALGORITHM] === 'fcfs';
  }

}
