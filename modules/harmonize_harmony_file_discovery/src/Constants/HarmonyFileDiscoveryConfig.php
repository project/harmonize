<?php

namespace Drupal\harmonize_harmony_file_discovery\Constants;

/**
 * Defines constants for the harmonization system.
 */
final class HarmonyFileDiscoveryConfig {

  /**
   * Global string defining the name for the main configuration of the module.
   *
   * @var string
   */
  public const MAIN = 'harmonize_harmony_file_discovery.settings';

  /**
   * The file extension for harmony files.
   *
   * @var string
   */
  public const MAIN__HARMONY_FILE_EXTENSION = 'file_extension';

  /**
   * The default file extension for harmony files.
   *
   * @var string
   */
  public const MAIN__HARMONY_FILE_EXTENSION__DEFAULT = '.harmony';

}
