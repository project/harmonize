<?php

namespace Drupal\harmonize_refinery\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\harmonize_refinery\PluginManager\Refinery\RefinerBase as PluginBase;

/**
 * Defines a Refiner item annotation object.
 *
 * Plugin Namespace: Plugin\harmonize\Refiner.
 *
 * @see \Drupal\harmonize_refinery\PluginManager\Refinery\Refinery
 * @see plugin_api
 *
 * @package Drupal\harmonize\Annotation
 *
 * @Annotation
 */
final class Refiner extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  protected string $id;

  /**
   * The target harmonizer signature of Refiner.
   *
   * @var string
   */
  protected string $target;

  /**
   * The themes to refine harmonizers in.
   *
   * @var array
   */
  protected array $themes;

  /**
   * The weight of this refiner.
   *
   * @var int
   */
  protected int $weight = 0;

  /**
   * Get the target of this Refiner.
   *
   * @see RefinerInterface::getTarget()
   */
  public function getTarget() : string {
    return $this->definition[PluginBase::TARGET];
  }

  /**
   * Get the themes this Refiner is applied in.
   *
   * @see RefinerInterface::getThemes()
   *
   * @noinspection PhpUnused
   */
  public function getThemes() : array {
    return $this->definition[PluginBase::THEMES];
  }

  /**
   * Get this Refiner's weight.
   *
   * @see RefinerInterface::getWeight()
   */
  public function getWeight() : int {
    return $this->definition[PluginBase::WEIGHT] ?? 0;
  }

}
