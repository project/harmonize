<?php

namespace Drupal\harmonize_refinery\EventSubscriber;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\harmonize\Constants\HarmonizationEvents;
use Drupal\harmonize\Constants\HarmonizeConfig;
use Drupal\harmonize\Event\HarmonizationEvent;
use Drupal\harmonize\Service\Harmonize;
use Drupal\harmonize_refinery\PluginManager\Refinery\RefinerInterface;
use Drupal\harmonize_refinery\PluginManager\Refinery\Refinery;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes events to apply Plugin Alterations of harmonization data.
 *
 * @property \Drupal\harmonize_refinery\PluginManager\Refinery\Refinery $refinery
 *
 * @package Drupal\custom_events\EventSubscriber
 *
 * @noinspection PhpUnused
 */
class RefineryEventSubscriber implements EventSubscriberInterface {
  use StringTranslationTrait;

  /**
   * Use DI to inject the Refinery Plugin Manager.
   *
   * @var \Drupal\harmonize_refinery\PluginManager\Refinery\Refinery
   */
  private Refinery $refinery;

  /**
   * A flag that determines whether we found a refiner.
   *
   * @var bool
   */
  private bool $loadedRefiner = FALSE;

  /**
   * RefineryEventSubscriber constructor.
   *
   * @param \Drupal\harmonize_refinery\PluginManager\Refinery\Refinery $refinery
   *   Refinery Plugin Manager injected through DI.
   */
  public function __construct(Refinery $refinery) {
    $this->refinery = $refinery;
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
   */
  public static function getSubscribedEvents() : array {
    return [
      HarmonizationEvents::CORE => ['onHarmonize', 200],
    ];
  }

  /**
   * React to a Harmonizer event.
   *
   * @param \Drupal\harmonize\Event\HarmonizationEvent $event
   *   Event that occurred, containing the harmonizer that fired the event.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *
   * @noinspection PhpUnused
   */
  public function onHarmonize(HarmonizationEvent $event) : void {
    // Reset our tracker.
    $this->loadedRefiner = FALSE;

    // If 'FCFS' is set, we only want to get the first refiner we find.
    if ($this->isFcfs()) {
      $event->harmonizer->trace($this->t('[Refinery Event] <strong>FCFS</strong> Algorithm Detected...Only a single Refiner will be loaded'));
    }

    // Get loading suggestions from the harmonizer.
    $suggestions = $event->harmonizer->getCachedSuggestions();

    // We reverse the array here.
    // FCFS should load the most specific suggestion first.
    // Granted, devs must make sure suggestions are organized properly.
    $suggestions = array_reverse($suggestions);

    // With each part of the signature, search for Refiners matching the part.
    foreach ($suggestions as $suggestion) {
      // The signature part should match the target attribute of the Refiner.
      $refinerTarget = $suggestion;

      // Get all plugin definitions with the specified target signature.
      $refiners = $this->refinery->getRefiners($refinerTarget);

      // If there are no refiners, we can stop here.
      if (empty($refiners)) {
        $event->harmonizer->trace($this->t("[Refinery Event] No refiners found for <strong><code>[@part]</code></strong>...Continuing", ['@part' => $refinerTarget]));
        continue;
      }

      // At this point we know a refiner was found.
      $this->loadedRefiner = TRUE;

      // Sort the refiners by weight.
      uasort($refiners, 'Drupal\harmonize_refinery\EventSubscriber\RefineryEventSubscriber::sortRefiners');

      $event->harmonizer->trace($this->t("[Refinery Event] <strong>Found @count Refiner(s)</strong> for <strong><code>[@part]</code></strong>...Continuing", [
        '@count' => count($refiners),
        '@part' => $refinerTarget,
      ]));

      // If 'FCFS', we only want one refiner.
      if ($this->isFcfs()) {
        $event->harmonizer->trace($this->t("[Refinery Event] Due to <strong>FCFS</strong>, only the <strong><code>[@refinerId]</code></strong> Refiner will be used...Proceeding", [
          '@refinerId' => reset($refiners)->getId(),
        ]));
        $refiners = [reset($refiners)];
      }

      // Perform data alterations for all refiners.
      foreach ($refiners as $refiner) {
        $consignment = $event->harmonizer->getData();
        if ($refiner !== NULL) {
          $harmony = [];
          $event->harmonizer->trace($this->t("[Refinery Event] Altering data with Refiner <strong><code>[@refinerId]</code></strong>", [
            '@refinerId' => $refiner->getId(),
          ]));
          $refiner->refine($consignment, $harmony, $event->harmonizer->getObject());
          // If the harmony object is empty, that means it wasn't reformatted.
          // We'll return the original consignment.
          if (!empty($harmony)) {
            $event->harmonizer->setData($harmony);
            $event->harmonizer->trace($this->t("[Refinery Event] <strong><code>\$harmony</code></strong> variable was set...<strong><code>\$consignment</code></strong> data has been overwritten"), TRUE);
          }
          else {
            $event->harmonizer->setData($consignment);
            $event->harmonizer->trace($this->t("[Refinery Event] Data modified and returned with alterations from Refiner"), TRUE);
          }
        }
      }

      // If 'FCFS', we break at this point.
      if ($this->isFcfs()) {
        break;
      }
    }

    // If no files were loaded, we can stop here.
    if (!$this->loadedRefiner) {
      $event->harmonizer->trace($this->t("[Refinery Event] No refiners were found. Data will be returned in its current state"), TRUE);
    }
  }

  /**
   * Helper function to tell us if we're in 'FCFS' mode.
   *
   * @return bool
   *   Returns TRUE if we're in FCFS mode, returns FALSE otherwise.
   */
  private function isFcfs() : bool {
    return Harmonize::$config[HarmonizeConfig::MAIN__LOADING_ALGORITHM] === 'fcfs';
  }

  /**
   * Sort refiners according to their weight.
   *
   * @param \Drupal\harmonize_refinery\PluginManager\Refinery\RefinerInterface $a
   *   First refiner plugin.
   * @param \Drupal\harmonize_refinery\PluginManager\Refinery\RefinerInterface $b
   *   Second refiner plugin.
   *
   * @return int
   *   Returns a value less than, equal to or greater than zero depending on if
   *   the first argument is considered to be respectively less than, equal to
   *   or greater than the second.
   */
  private static function sortRefiners(RefinerInterface $a, RefinerInterface $b) : int {
    return $a->getWeight() - $b->getWeight();
  }

}
